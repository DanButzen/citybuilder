﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTB.CityBuilder.BL
{
    public class Block
    {
        public int Row { get; set; }
        public int Col { get; set; }
        public string Building { get; set; }
        public int BuildingLevel { get; set; }
        public int NextToRoad { get; set; }
        public int NextToWell { get; set; }
        public int NextToHouse { get; set; }
        public int NextToTavern { get; set; }
        public int NextToFarm { get; set; }
    }
}
