﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTB.CityBuilder.BL
{
    public class Map
    {
        private string[,] obMap;

        public string[,] MyMap
        {
            get { return obMap; }
            set { obMap = value; }
        }

    }
}
