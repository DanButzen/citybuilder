﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTB.CityBuilder.BL
{
    public class Player
    {
        public int Money { get; set; }
        public int Population { get; set; }
        public int Workers { get; set; }
        public int Food { get; set; }
        public int income { get; set; }
        public int foodIncome { get; set; }
        public int foodUse { get; set; }
    }
}
