﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DTB.CityBuilder.UI
{
    public partial class BuildNew : Form
    {
        int Row;
        int Col;
        public BuildNew(int row, int col)
        {
            
            InitializeComponent();
            Row = row;
            Col = col;
        }

        

        private void btnBuildRoad_Click(object sender, EventArgs e)
        {
            if (frmGame.player.Money > frmGame.roadCost)
            {
                /** possible future addition
                if (Row - 1 >= 0 && frmGame.map[Row - 1, Col].Building != "R"
                    && Col + 1 < frmGame.mapSize && frmGame.map[Row, Col + 1].Building != "R"
                    && Col - 1 >= 0 && frmGame.map[Row, Col - 1].Building != "R"
                    && Row + 1 < frmGame.mapSize && frmGame.map[Row + 1, Col].Building != "R")
                {
                    MessageBox.Show("Cannot build road where not adjacent to another road");
                }
                /**/
                RemoveOld();
                frmGame.map[Row, Col].Building = "R";
                frmGame.player.Money -= frmGame.roadCost;
                AddRoad();
                this.Close();
            }
            else
            {
                MessageBox.Show("Not enough funds!");
            }
            
        }

        private void btnBuildHouse_Click(object sender, EventArgs e)
        {
            if (frmGame.player.Money > frmGame.houseCost)
            {
                RemoveOld();
                frmGame.map[Row, Col].Building = "H";
                frmGame.player.Money -= frmGame.houseCost;
                AddHouse();
                this.Close();
            }
            else
            {
                MessageBox.Show("Not enough funds!");
            }
            
        }

        private void btnBuildWell_Click(object sender, EventArgs e)
        {
            if (frmGame.player.Money > frmGame.wellCost)
            {
                RemoveOld();
                frmGame.map[Row, Col].Building = "W";
                frmGame.player.Money -= frmGame.wellCost;
                AddWell();
                this.Close();
            }
            else
            {
                MessageBox.Show("Not enough funds!");
            }
            
        }

        private void btnBuildTavern_Click(object sender, EventArgs e)
        {
            if (frmGame.player.Money > frmGame.tavernCost && frmGame.player.Workers >= 4)
            {
                RemoveOld();
                frmGame.map[Row, Col].Building = "T";
                frmGame.player.Money -= frmGame.tavernCost;
                AddTavern();
                this.Close();
            }
            else
            {
                MessageBox.Show("Not enough funds and/or workers!");
            }
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void RemoveOld()
        {
            if(frmGame.map[Row, Col].Building == "H")
            {
                RemoveHouse();
                frmGame.map[Row, Col].Building = String.Empty;
            }
            if(frmGame.map[Row, Col].Building == "W")
            {
                RemoveWell();
                frmGame.map[Row, Col].Building = String.Empty;
            }
            if(frmGame.map[Row, Col].Building == "T")
            {
                RemoveTavern();
                frmGame.map[Row, Col].Building = String.Empty;
            }
            if(frmGame.map[Row, Col].Building == "F")
            {
                RemoveFarm();
                frmGame.map[Row, Col].Building = String.Empty;
            }
            if (frmGame.map[Row, Col].Building == "R")
            {
                RemoveRoad();
                frmGame.map[Row, Col].Building = String.Empty;
            }
        }
        public void RemoveOld(int passedRow, int passedCol)
        {
            if (frmGame.map[passedRow, passedCol].Building == "H")
            {
                RemoveHouse(passedRow, passedCol);
                frmGame.map[passedRow, passedCol].Building = String.Empty;
            }
            if (frmGame.map[passedRow, passedCol].Building == "W")
            {
                RemoveWell(passedRow, passedCol);
                frmGame.map[passedRow, passedCol].Building = String.Empty;
            }
            if (frmGame.map[passedRow, passedCol].Building == "T")
            {
                RemoveTavern(passedRow, passedCol);
                frmGame.map[passedRow, passedCol].Building = String.Empty;
            }
            if (frmGame.map[passedRow, passedCol].Building == "F")
            {
                RemoveFarm(passedRow, passedCol);
                frmGame.map[passedRow, passedCol].Building = String.Empty;
            }
            if (frmGame.map[passedRow, passedCol].Building == "R")
            {
                RemoveRoad(passedRow, passedCol);
                frmGame.map[passedRow, passedCol].Building = String.Empty;
            }
        }

        public void RemoveRoad()
        {
            if (Row - 1 >= 0 )
            {
                frmGame.map[Row - 1, Col].NextToRoad -= 1;
            }
            if ( Col + 1 < frmGame.mapSize)
            {
                frmGame.map[Row , Col + 1].NextToRoad -= 1;
            }
            if ( Col - 1 >= 0)
            {
                frmGame.map[Row , Col - 1].NextToRoad -= 1;
            }
            if (Row + 1 < frmGame.mapSize)
            {
                frmGame.map[Row + 1, Col ].NextToRoad -= 1;
            }
        }
        public void RemoveRoad(int overRow, int overCol)
        {
            if (overRow - 1 >= 0)
            {
                frmGame.map[overRow - 1, overCol].NextToRoad -= 1;
            }
            if (overCol + 1 < frmGame.mapSize)
            {
                frmGame.map[overRow, overCol + 1].NextToRoad -= 1;
            }
            if (overCol - 1 >= 0)
            {
                frmGame.map[overRow, overCol - 1].NextToRoad -= 1;
            }
            if (overRow + 1 < frmGame.mapSize)
            {
                frmGame.map[overRow + 1, overCol].NextToRoad -= 1;
            }
        }
        private void AddRoad()
        {
            if (Row - 1 >= 0)
            {
                frmGame.map[Row - 1, Col ].NextToRoad += 1;
            }
            if (Col + 1 < frmGame.mapSize)
            {
                frmGame.map[Row , Col + 1].NextToRoad += 1;
            }
            if (Col - 1 >= 0)
            {
                frmGame.map[Row , Col - 1].NextToRoad += 1;
            }
            if (Row + 1 < frmGame.mapSize)
            {
                frmGame.map[Row + 1, Col].NextToRoad += 1;
            }
        }

        public void RemoveWell()
        {

            for (int i = Row - 3; i < Row + 4; i++)
            {
                for (int j = Col - 3; j < Col + 4; j++)
                {
                    if (i < frmGame.mapSize && i > -1 && j < frmGame.mapSize && j > -1)
                    {
                        frmGame.map[i, j].NextToWell -= 1;
                    }
                }
            }
        }
        public void RemoveWell(int overRow, int overCol)
        {

            for (int i = overRow - 3; i < overRow + 4; i++)
            {
                for (int j = overCol - 3; j < overCol + 4; j++)
                {
                    if (i < frmGame.mapSize && i > -1 && j < frmGame.mapSize && j > -1)
                    {
                        frmGame.map[i, j].NextToWell -= 1;
                    }
                }
            }
        }
        private void AddWell()
        {
            for (int i = Row - 3; i < Row + 4; i++)
            {
                for (int j = Col - 3; j < Col + 4; j++)
                {
                    if (i < frmGame.mapSize && i > -1 && j < frmGame.mapSize && j > -1)
                    {
                        frmGame.map[i, j].NextToWell += 1;
                    }
                }
            }
        }


        public void RemoveTavern()
        {
            for (int i = Row - 5; i < Row + 6; i++)
            {
                for (int j = Col - 5; j < Col + 6; j++)
                {
                    if (i < frmGame.mapSize && i > -1 && j < frmGame.mapSize && j > -1)
                    {
                        frmGame.map[i, j].NextToTavern += 1;
                    }
                }
            }
        }
        public void RemoveTavern(int overRow, int overCol)
        {
            for (int i = overRow - 5; i < overRow + 6; i++)
            {
                for (int j = overCol - 5; j < overCol + 6; j++)
                {
                    if (i < frmGame.mapSize && i > -1 && j < frmGame.mapSize && j > -1)
                    {
                        frmGame.map[i, j].NextToTavern += 1;
                    }
                }
            }
        }
        private void AddTavern()
        {
            for (int i = Row - 5; i < Row + 6; i++)
            {
                for (int j = Col - 5; j < Col + 6; j++)
                {
                    if (i < frmGame.mapSize && i > -1 && j < frmGame.mapSize && j > -1)
                    {
                        frmGame.map[i, j].NextToTavern += 1;
                    }
                }
            } 
        }

        public void RemoveHouse()
        {
            if (Row - 1 >= 0 )
            {
                frmGame.map[Row - 1, Col ].NextToHouse -= 1;
            }
            if ( Col + 1 < frmGame.mapSize)
            {
                frmGame.map[Row , Col + 1].NextToHouse -= 1;
            }
            if ( Col - 1 >= 0)
            {
                frmGame.map[Row , Col - 1].NextToHouse -= 1;
            }
            if (Row + 1 < frmGame.mapSize)
            {
                frmGame.map[Row + 1, Col ].NextToHouse -= 1;
            }
        }
        public void RemoveHouse(int overRow, int overCol)
        {
            if (overRow - 1 >= 0)
            {
                frmGame.map[overRow - 1, overCol].NextToHouse -= 1;
            }
            if (overCol + 1 < frmGame.mapSize)
            {
                frmGame.map[overRow, overCol + 1].NextToHouse -= 1;
            }
            if (overCol - 1 >= 0)
            {
                frmGame.map[overRow, overCol - 1].NextToHouse -= 1;
            }
            if (overRow + 1 < frmGame.mapSize)
            {
                frmGame.map[overRow + 1, overCol].NextToHouse -= 1;
            }
        }
        private void AddHouse()
        {
            if (Row - 1 >= 0 )
            {
                frmGame.map[Row - 1, Col].NextToHouse += 1;
            }
            if (Col + 1 < frmGame.mapSize)
            {
                frmGame.map[Row, Col + 1].NextToHouse += 1;
            }
            if ( Col - 1 >= 0)
            {
                frmGame.map[Row , Col - 1].NextToHouse += 1;
            }
            if (Row + 1 < frmGame.mapSize)
            {
                frmGame.map[Row + 1, Col].NextToHouse += 1;
            }
        }

        public void RemoveFarm()
        {
            if (Row - 1 >= 0 )
            {
                frmGame.map[Row - 1, Col].NextToFarm -= 1;
            }
            if ( Col + 1 < frmGame.mapSize)
            {
                frmGame.map[Row , Col + 1].NextToFarm -= 1;
            }
            if ( Col - 1 >= 0)
            {
                frmGame.map[Row , Col - 1].NextToFarm -= 1;
            }
            if (Row + 1 < frmGame.mapSize)
            {
                frmGame.map[Row + 1, Col ].NextToFarm -= 1;
            }
        }
        public void RemoveFarm(int overRow, int overCol)
        {
            if (overRow - 1 >= 0)
            {
                frmGame.map[overRow - 1, overCol].NextToFarm -= 1;
            }
            if (overCol + 1 < frmGame.mapSize)
            {
                frmGame.map[overRow, overCol + 1].NextToFarm -= 1;
            }
            if (overCol - 1 >= 0)
            {
                frmGame.map[overRow, overCol - 1].NextToFarm -= 1;
            }
            if (overRow + 1 < frmGame.mapSize)
            {
                frmGame.map[Row + 1, overCol].NextToFarm -= 1;
            }
        }
        private void AddFarm()
        {
            if (Row - 1 >= 0 )
            {
                frmGame.map[Row - 1, Col ].NextToFarm += 1;
            }
            if (Col + 1 < frmGame.mapSize)
            {
                frmGame.map[Row , Col + 1].NextToFarm += 1;
            }
            if ( Col - 1 >= 0)
            {
                frmGame.map[Row , Col - 1].NextToFarm += 1;
            }
            if (Row + 1 < frmGame.mapSize)
            {
                frmGame.map[Row + 1, Col ].NextToFarm += 1;
            }
        }

        private void btnBuildFarm_Click(object sender, EventArgs e)
        {
            if (frmGame.player.Money > frmGame.farmCost && frmGame.player.Workers >= 2)
            {
                RemoveOld();
                frmGame.map[Row, Col].Building = "F";
                frmGame.player.Money -= frmGame.farmCost;
                AddFarm();
                this.Close();
            }
            else
            {
                MessageBox.Show("Not enough funds and/or workers!");
            }
        }
    }
}
