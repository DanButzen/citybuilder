﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTB.CityBuilder.BL;


namespace DTB.CityBuilder.UI
{
    public partial class frmGame : Form
    {
        public frmGame()
        {
            InitializeComponent();
        }

        static public int mapSize = 16;
        static public Block[,] map = new Block[mapSize, mapSize];
        static public int roadCost = 10;
        static public int wellCost = 20;
        static public int houseCost = 10;
        static public int tavernCost = 100;
        static public int farmCost = 20;
        static public Player player = new Player();
        static public int population = 0;
        static public int day = 1;

        

        private void frmGame_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < mapSize; i++)
            {
                for (int j = 0; j < mapSize; j++)
                {
                    map[i, j] = new Block();
                    map[i, j].Row = j;
                    map[i, j].Col = i;
                    map[i, j].Building = String.Empty;
                }
            }
            
            player.Food = 20;
            player.Money = 500;
            player.Population = 0;
            player.income = 0;
            player.foodIncome = 0;
            player.foodUse = 0;
            player.Workers = 0;
            updateResources();

        }

      

        private void map_click(object sender, EventArgs e)
        {
            Label clickedLabel = (Label)sender;
            int col = clickedLabel.Left/30 - 1;
            int row = clickedLabel.Top/30 - 1;
            if (map[row, col].Building == String.Empty)
            {
                BuildNew buildNew = new BuildNew(row, col);
                buildNew.ShowDialog();
            }
            else
            {
                MessageBoxButtons messageBoxButtons = MessageBoxButtons.YesNo;
                DialogResult dialogResult = MessageBox.Show("Do you wish to remove the current building?", "Remove Building", messageBoxButtons);
                if (dialogResult == DialogResult.Yes)
                {
                    BuildNew buildNew = new BuildNew(row, col);
                    buildNew.RemoveOld(row, col);
                    map[row, col].Building = String.Empty;
                }
            }

            altUpdateMap();
            updateResources();
        }

        private void getRoad(object sender, int row, int col) //checks all neighbors for roads and places correct image into selected block
        {
            Label clickedLabel = (Label)sender;
            if (row - 1 < 0 && col - 1 < 0) // up left corner
            {
                if (map[row + 1, col].Building == "R") //down
                {
                    if (map[row, col + 1].Building == "R")//right
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRUD);
                        clickedLabel.Image = bmp;
                    }
                    else
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLUD);
                        clickedLabel.Image = bmp;
                    }
                }
                else if (map[row, col + 1].Building == "R")
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRU);
                    clickedLabel.Image = bmp;
                }
                else
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLU);
                    clickedLabel.Image = bmp;
                }
            }
            else if (row + 1 > mapSize - 1 && col - 1 < 0)// bottom left corner
            {
                if (map[row - 1, col].Building == "R") //up
                {
                    if (map[row, col + 1].Building == "R")//right
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRUD);
                        clickedLabel.Image = bmp;
                    }
                    else
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLUD);
                        clickedLabel.Image = bmp;
                    }
                }
                else if (map[row, col + 1].Building == "R") //right
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRD);
                    clickedLabel.Image = bmp;
                }
                else
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLD);
                    clickedLabel.Image = bmp;
                }
            }
            else if (row + 1 > mapSize - 1 && col + 1 > mapSize - 1)// bottom right corner
            {
                if (map[row - 1, col].Building == "R") //up
                {
                    if (map[row, col - 1].Building == "R")//left
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRUD);
                        clickedLabel.Image = bmp;
                    }
                    else
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRUD);
                        clickedLabel.Image = bmp;
                    }
                }
                else if (map[row, col - 1].Building == "R") //left
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRD);
                    clickedLabel.Image = bmp;
                }
                else
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRD);
                    clickedLabel.Image = bmp;
                }
            }
            else if (row - 1 < 0 && col + 1 > mapSize - 1)// up right corner
            {
                if (map[row + 1, col].Building == "R") //down
                {
                    if (map[row, col - 1].Building == "R")//left
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRUD);
                        clickedLabel.Image = bmp;
                    }
                    else
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRUD);
                        clickedLabel.Image = bmp;
                    }
                }
                else if (map[row, col - 1].Building == "R") //left
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRU);
                    clickedLabel.Image = bmp;
                }
                else
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRU);
                    clickedLabel.Image = bmp;
                }
            }
            else if (row - 1 < 0) //top
            {
                if (map[row, col-1].Building == "R") //left
                {
                    if (map[row, col + 1].Building == "R") //right
                    {
                        if (map[row +1, col].Building == "R") //down
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRUD);
                            clickedLabel.Image = bmp;
                        }
                        else
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRU);
                            clickedLabel.Image = bmp;
                        }
                    }
                    else if (map[row + 1, col].Building == "R")//down
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLUD);
                        clickedLabel.Image = bmp;
                    }
                    else
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLU);
                        clickedLabel.Image = bmp;
                    }
                }
                else if (map[row, col + 1].Building == "R")//right
                {
                    if (map[row + 1, col].Building == "R") //down
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRUD);
                        clickedLabel.Image = bmp;
                    }
                    else
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRU);
                        clickedLabel.Image = bmp;
                    }
                }
                else if (map[row + 1, col].Building == "R") //down
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadUD);
                    clickedLabel.Image = bmp;
                }
                else
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadU);
                    clickedLabel.Image = bmp;
                }
            }


            else if (row + 1 > mapSize - 1) //bottom
            {
                if (map[row, col - 1].Building == "R") //left
                {
                    if (map[row, col + 1].Building == "R") //right
                    {
                        if (map[row - 1, col].Building == "R") //up
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRUD);
                            clickedLabel.Image = bmp;
                        }
                        else
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRD);
                            clickedLabel.Image = bmp;
                        }
                    }
                    else if (map[row - 1, col].Building == "R")//up
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLUD);
                        clickedLabel.Image = bmp;
                    }
                    else
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLD);
                        clickedLabel.Image = bmp;
                    }
                }
                else if (map[row, col + 1].Building == "R")//right
                {
                    if (map[row - 1, col].Building == "R") //up
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRUD);
                        clickedLabel.Image = bmp;
                    }
                    else
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRD);
                        clickedLabel.Image = bmp;
                    }
                }
                else if (map[row - 1, col].Building == "R") //up
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadUD);
                    clickedLabel.Image = bmp;
                }
                else
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadD);
                    clickedLabel.Image = bmp;
                }
            }

            else if (col - 1 < 0) //left
            {
                if (map[row - 1, col].Building == "R") //up
                {
                    if (map[row, col + 1].Building == "R") //right
                    {
                        if (map[row + 1, col].Building == "R") //down
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRUD);
                            clickedLabel.Image = bmp;
                        }
                        else
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRU);
                            clickedLabel.Image = bmp;
                        }
                    }
                    else if (map[row + 1, col].Building == "R")//down
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLUD);
                        clickedLabel.Image = bmp;
                    }
                    else
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLU);
                        clickedLabel.Image = bmp;
                    }
                }
                else if (map[row, col + 1].Building == "R")//right
                {
                    if (map[row + 1, col].Building == "R") //down
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRD);
                        clickedLabel.Image = bmp;
                    }
                    else
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLR);
                        clickedLabel.Image = bmp;
                    }
                }
                else if (map[row + 1, col].Building == "R") //down
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLD);
                    clickedLabel.Image = bmp;
                }
                else
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadL);
                    clickedLabel.Image = bmp;
                }
            }
            else if (col + 1 > mapSize - 1) //right
            {
                if (map[row - 1, col].Building == "R") //up
                {
                    if (map[row, col - 1].Building == "R") //left
                    {
                        if (map[row + 1, col].Building == "R") //down
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRUD);
                            clickedLabel.Image = bmp;
                        }
                        else
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRU);
                            clickedLabel.Image = bmp;
                        }
                    }
                    else if (map[row + 1, col].Building == "R")//down
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRUD);
                        clickedLabel.Image = bmp;
                    }
                    else
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRU);
                        clickedLabel.Image = bmp;
                    }
                }
                else if (map[row, col - 1].Building == "R") //left
                {
                    if (map[row + 1, col].Building == "R") //down
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRD);
                        clickedLabel.Image = bmp;
                    }
                    else
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLR);
                        clickedLabel.Image = bmp;
                    }
                }
                else if (map[row + 1, col].Building == "R") //down
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRD);
                    clickedLabel.Image = bmp;
                }
                else
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadR);
                    clickedLabel.Image = bmp;
                }
            }
            else //no constraints
            {
                if (map[row, col+1].Building == "R") //right
                {
                    if (map[row - 1, col].Building == "R") //up
                    {
                        if (map[row, col - 1].Building == "R") //left
                        {
                            if (map[row + 1, col].Building == "R") //down
                            {
                                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRUD);
                                clickedLabel.Image = bmp;
                            }
                            else
                            {
                                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRU);
                                clickedLabel.Image = bmp;
                            }
                        }
                        else if (map[row + 1, col].Building == "R")//down
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRUD);
                            clickedLabel.Image = bmp;
                        }
                        else
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRU);
                            clickedLabel.Image = bmp;
                        }
                    }
                    else if (map[row, col - 1].Building == "R") //left
                    {
                        if (map[row + 1, col].Building == "R") //down
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRD);
                            clickedLabel.Image = bmp;
                        }
                        else
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLR);
                            clickedLabel.Image = bmp;
                        }
                    }
                    else if (map[row + 1, col].Building == "R") //down
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRD);
                        clickedLabel.Image = bmp;
                    }
                    else
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadR);
                        clickedLabel.Image = bmp;
                    }
                }
                else if (map[row, col-1].Building == "R") //Left
                {
                    if (map[row - 1, col].Building == "R") //up
                    {
                        if (map[row, col + 1].Building == "R") //right
                        {
                            if (map[row + 1, col].Building == "R") //down
                            {
                                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRUD);
                                clickedLabel.Image = bmp;
                            }
                            else
                            {
                                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRU);
                                clickedLabel.Image = bmp;
                            }
                        }
                        else if (map[row + 1, col].Building == "R")//down
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLUD);
                            clickedLabel.Image = bmp;
                        }
                        else
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLU);
                            clickedLabel.Image = bmp;
                        }
                    }
                    else if (map[row, col + 1].Building == "R")//right
                    {
                        if (map[row + 1, col].Building == "R") //down
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRD);
                            clickedLabel.Image = bmp;
                        }
                        else
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLR);
                            clickedLabel.Image = bmp;
                        }
                    }
                    else if (map[row + 1, col].Building == "R") //down
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLD);
                        clickedLabel.Image = bmp;
                    }
                    else
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadL);
                        clickedLabel.Image = bmp;
                    }
                }
                else if (map[row+1, col].Building == "R") //down
                {
                    if (map[row, col - 1].Building == "R") //left
                    {
                        if (map[row, col + 1].Building == "R") //right
                        {
                            if (map[row - 1, col].Building == "R") //up
                            {
                                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRUD);
                                clickedLabel.Image = bmp;
                            }
                            else
                            {
                                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRD);
                                clickedLabel.Image = bmp;
                            }
                        }
                        else if (map[row - 1, col].Building == "R")//up
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLUD);
                            clickedLabel.Image = bmp;
                        }
                        else
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLD);
                            clickedLabel.Image = bmp;
                        }
                    }
                    else if (map[row, col + 1].Building == "R")//right
                    {
                        if (map[row - 1, col].Building == "R") //up
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRUD);
                            clickedLabel.Image = bmp;
                        }
                        else
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRD);
                            clickedLabel.Image = bmp;
                        }
                    }
                    else if (map[row - 1, col].Building == "R") //up
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadUD);
                        clickedLabel.Image = bmp;
                    }
                    else
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadD);
                        clickedLabel.Image = bmp;
                    }
                }
                else if (map[row-1, col].Building == "R") //up
                {
                    if (map[row, col - 1].Building == "R") //left
                    {
                        if (map[row, col + 1].Building == "R") //right
                        {
                            if (map[row + 1, col].Building == "R") //down
                            {
                                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRUD);
                                clickedLabel.Image = bmp;
                            }
                            else
                            {
                                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLRU);
                                clickedLabel.Image = bmp;
                            }
                        }
                        else if (map[row + 1, col].Building == "R")//down
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLUD);
                            clickedLabel.Image = bmp;
                        }
                        else
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadLU);
                            clickedLabel.Image = bmp;
                        }
                    }
                    else if (map[row, col + 1].Building == "R")//right
                    {
                        if (map[row + 1, col].Building == "R") //down
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRUD);
                            clickedLabel.Image = bmp;
                        }
                        else
                        {
                            var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadRU);
                            clickedLabel.Image = bmp;
                        }
                    }
                    else if (map[row + 1, col].Building == "R") //down
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadUD);
                        clickedLabel.Image = bmp;
                    }
                    else
                    {
                        var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.RoadU);
                        clickedLabel.Image = bmp;
                    }
                }
                else
                {
                    var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.Road0);
                    clickedLabel.Image = bmp;
                }
            }
        }
        
      

        private void btnNextDay_Click(object sender, EventArgs e)
        {
            day++;
            lblDay.Text = day.ToString();


            player.Money += player.income;
            player.Food += player.foodIncome;
            if(player.Food < 0)
            {
                player.Money -= player.Food * 2;
                player.Food = 0;
            }
            updateResources();
        }

      

        private void updateResources()
        {
            lblFoodIncome.Text = player.foodIncome.ToString();
            lblIncome.Text = player.income.ToString();
            lblFood.Text = player.Food.ToString();
            lblMoney.Text = player.Money.ToString();
            lblPopulation.Text = player.Population.ToString();
            lblWorkers.Text = player.Workers.ToString();
        }

        private void altUpdateMap() //updates every sqaure on the map based on neighbors
        {
            player.income = 0;
            player.foodIncome = 0;
            player.Population = 0;
            player.Workers = 0;

            altUpdateSquare(lbl1_1, 0, 0);
            altUpdateSquare(lbl1_2, 0, 1);
            altUpdateSquare(lbl1_3, 0, 2);
            altUpdateSquare(lbl1_4, 0, 3);
            altUpdateSquare(lbl1_5, 0, 4);
            altUpdateSquare(lbl1_6, 0, 5);
            altUpdateSquare(lbl1_7, 0, 6);
            altUpdateSquare(lbl1_8, 0, 7);
            altUpdateSquare(lbl1_9, 0, 8);
            altUpdateSquare(lbl1_10, 0, 9);
            altUpdateSquare(lbl1_11, 0, 10);
            altUpdateSquare(lbl1_12, 0, 11);
            altUpdateSquare(lbl1_13, 0, 12);
            altUpdateSquare(lbl1_14, 0, 13);
            altUpdateSquare(lbl1_15, 0, 14);
            altUpdateSquare(lbl1_16, 0, 15);

            altUpdateSquare(lbl2_1, 1, 0);
            altUpdateSquare(lbl2_2, 1, 1);
            altUpdateSquare(lbl2_3, 1, 2);
            altUpdateSquare(lbl2_4, 1, 3);
            altUpdateSquare(lbl2_5, 1, 4);
            altUpdateSquare(lbl2_6, 1, 5);
            altUpdateSquare(lbl2_7, 1, 6);
            altUpdateSquare(lbl2_8, 1, 7);
            altUpdateSquare(lbl2_9, 1, 8);
            altUpdateSquare(lbl2_10, 1, 9);
            altUpdateSquare(lbl2_11, 1, 10);
            altUpdateSquare(lbl2_12, 1, 11);
            altUpdateSquare(lbl2_13, 1, 12);
            altUpdateSquare(lbl2_14, 1, 13);
            altUpdateSquare(lbl2_15, 1, 14);
            altUpdateSquare(lbl2_16, 1, 15);

            altUpdateSquare(lbl3_1, 2, 0);
            altUpdateSquare(lbl3_2, 2, 1);
            altUpdateSquare(lbl3_3, 2, 2);
            altUpdateSquare(lbl3_4, 2, 3);
            altUpdateSquare(lbl3_5, 2, 4);
            altUpdateSquare(lbl3_6, 2, 5);
            altUpdateSquare(lbl3_7, 2, 6);
            altUpdateSquare(lbl3_8, 2, 7);
            altUpdateSquare(lbl3_9, 2, 8);
            altUpdateSquare(lbl3_10, 2, 9);
            altUpdateSquare(lbl3_11, 2, 10);
            altUpdateSquare(lbl3_12, 2, 11);
            altUpdateSquare(lbl3_13, 2, 12);
            altUpdateSquare(lbl3_14, 2, 13);
            altUpdateSquare(lbl3_15, 2, 14);
            altUpdateSquare(lbl3_16, 2, 15);

            altUpdateSquare(lbl4_1, 3, 0);
            altUpdateSquare(lbl4_2, 3, 1);
            altUpdateSquare(lbl4_3, 3, 2);
            altUpdateSquare(lbl4_4, 3, 3);
            altUpdateSquare(lbl4_5, 3, 4);
            altUpdateSquare(lbl4_6, 3, 5);
            altUpdateSquare(lbl4_7, 3, 6);
            altUpdateSquare(lbl4_8, 3, 7);
            altUpdateSquare(lbl4_9, 3, 8);
            altUpdateSquare(lbl4_10, 3, 9);
            altUpdateSquare(lbl4_11, 3, 10);
            altUpdateSquare(lbl4_12, 3, 11);
            altUpdateSquare(lbl4_13, 3, 12);
            altUpdateSquare(lbl4_14, 3, 13);
            altUpdateSquare(lbl4_15, 3, 14);
            altUpdateSquare(lbl4_16, 3, 15);

            altUpdateSquare(lbl5_1, 4, 0);
            altUpdateSquare(lbl5_2, 4, 1);
            altUpdateSquare(lbl5_3, 4, 2);
            altUpdateSquare(lbl5_4, 4, 3);
            altUpdateSquare(lbl5_5, 4, 4);
            altUpdateSquare(lbl5_6, 4, 5);
            altUpdateSquare(lbl5_7, 4, 6);
            altUpdateSquare(lbl5_8, 4, 7);
            altUpdateSquare(lbl5_9, 4, 8);
            altUpdateSquare(lbl5_10, 4, 9);
            altUpdateSquare(lbl5_11, 4, 10);
            altUpdateSquare(lbl5_12, 4, 11);
            altUpdateSquare(lbl5_13, 4, 12);
            altUpdateSquare(lbl5_14, 4, 13);
            altUpdateSquare(lbl5_15, 4, 14);
            altUpdateSquare(lbl5_16, 4, 15);

            altUpdateSquare(lbl6_1, 5, 0);
            altUpdateSquare(lbl6_2, 5, 1);
            altUpdateSquare(lbl6_3, 5, 2);
            altUpdateSquare(lbl6_4, 5, 3);
            altUpdateSquare(lbl6_5, 5, 4);
            altUpdateSquare(lbl6_6, 5, 5);
            altUpdateSquare(lbl6_7, 5, 6);
            altUpdateSquare(lbl6_8, 5, 7);
            altUpdateSquare(lbl6_9, 5, 8);
            altUpdateSquare(lbl6_10, 5, 9);
            altUpdateSquare(lbl6_11, 5, 10);
            altUpdateSquare(lbl6_12, 5, 11);
            altUpdateSquare(lbl6_13, 5, 12);
            altUpdateSquare(lbl6_14, 5, 13);
            altUpdateSquare(lbl6_15, 5, 14);
            altUpdateSquare(lbl6_16, 5, 15);

            altUpdateSquare(lbl7_1, 6, 0);
            altUpdateSquare(lbl7_2, 6, 1);
            altUpdateSquare(lbl7_3, 6, 2);
            altUpdateSquare(lbl7_4, 6, 3);
            altUpdateSquare(lbl7_5, 6, 4);
            altUpdateSquare(lbl7_6, 6, 5);
            altUpdateSquare(lbl7_7, 6, 6);
            altUpdateSquare(lbl7_8, 6, 7);
            altUpdateSquare(lbl7_9, 6, 8);
            altUpdateSquare(lbl7_10, 6, 9);
            altUpdateSquare(lbl7_11, 6, 10);
            altUpdateSquare(lbl7_12, 6, 11);
            altUpdateSquare(lbl7_13, 6, 12);
            altUpdateSquare(lbl7_14, 6, 13);
            altUpdateSquare(lbl7_15, 6, 14);
            altUpdateSquare(lbl7_16, 6, 15);

            altUpdateSquare(lbl8_1, 7, 0);
            altUpdateSquare(lbl8_2, 7, 1);
            altUpdateSquare(lbl8_3, 7, 2);
            altUpdateSquare(lbl8_4, 7, 3);
            altUpdateSquare(lbl8_5, 7, 4);
            altUpdateSquare(lbl8_6, 7, 5);
            altUpdateSquare(lbl8_7, 7, 6);
            altUpdateSquare(lbl8_8, 7, 7);
            altUpdateSquare(lbl8_9, 7, 8);
            altUpdateSquare(lbl8_10, 7, 9);
            altUpdateSquare(lbl8_11, 7, 10);
            altUpdateSquare(lbl8_12, 7, 11);
            altUpdateSquare(lbl8_13, 7, 12);
            altUpdateSquare(lbl8_14, 7, 13);
            altUpdateSquare(lbl8_15, 7, 14);
            altUpdateSquare(lbl8_16, 7, 15);

            altUpdateSquare(lbl9_1, 8, 0);
            altUpdateSquare(lbl9_2, 8, 1);
            altUpdateSquare(lbl9_3, 8, 2);
            altUpdateSquare(lbl9_4, 8, 3);
            altUpdateSquare(lbl9_5, 8, 4);
            altUpdateSquare(lbl9_6, 8, 5);
            altUpdateSquare(lbl9_7, 8, 6);
            altUpdateSquare(lbl9_8, 8, 7);
            altUpdateSquare(lbl9_9, 8, 8);
            altUpdateSquare(lbl9_10, 8, 9);
            altUpdateSquare(lbl9_11, 8, 10);
            altUpdateSquare(lbl9_12, 8, 11);
            altUpdateSquare(lbl9_13, 8, 12);
            altUpdateSquare(lbl9_14, 8, 13);
            altUpdateSquare(lbl9_15, 8, 14);
            altUpdateSquare(lbl9_16, 8, 15);

            altUpdateSquare(lbl10_1, 9, 0);
            altUpdateSquare(lbl10_2, 9, 1);
            altUpdateSquare(lbl10_3, 9, 2);
            altUpdateSquare(lbl10_4, 9, 3);
            altUpdateSquare(lbl10_5, 9, 4);
            altUpdateSquare(lbl10_6, 9, 5);
            altUpdateSquare(lbl10_7, 9, 6);
            altUpdateSquare(lbl10_8, 9, 7);
            altUpdateSquare(lbl10_9, 9, 8);
            altUpdateSquare(lbl10_10, 9, 9);
            altUpdateSquare(lbl10_11, 9, 10);
            altUpdateSquare(lbl10_12, 9, 11);
            altUpdateSquare(lbl10_13, 9, 12);
            altUpdateSquare(lbl10_14, 9, 13);
            altUpdateSquare(lbl10_15, 9, 14);
            altUpdateSquare(lbl10_16, 9, 15);

            altUpdateSquare(lbl11_1, 10, 0);
            altUpdateSquare(lbl11_2, 10, 1);
            altUpdateSquare(lbl11_3, 10, 2);
            altUpdateSquare(lbl11_4, 10, 3);
            altUpdateSquare(lbl11_5, 10, 4);
            altUpdateSquare(lbl11_6, 10, 5);
            altUpdateSquare(lbl11_7, 10, 6);
            altUpdateSquare(lbl11_8, 10, 7);
            altUpdateSquare(lbl11_9, 10, 8);
            altUpdateSquare(lbl11_10, 10, 9);
            altUpdateSquare(lbl11_11, 10, 10);
            altUpdateSquare(lbl11_12, 10, 11);
            altUpdateSquare(lbl11_13, 10, 12);
            altUpdateSquare(lbl11_14, 10, 13);
            altUpdateSquare(lbl11_15, 10, 14);
            altUpdateSquare(lbl11_16, 10, 15);

            altUpdateSquare(lbl12_1, 11, 0);
            altUpdateSquare(lbl12_2, 11, 1);
            altUpdateSquare(lbl12_3, 11, 2);
            altUpdateSquare(lbl12_4, 11, 3);
            altUpdateSquare(lbl12_5, 11, 4);
            altUpdateSquare(lbl12_6, 11, 5);
            altUpdateSquare(lbl12_7, 11, 6);
            altUpdateSquare(lbl12_8, 11, 7);
            altUpdateSquare(lbl12_9, 11, 8);
            altUpdateSquare(lbl12_10, 11, 9);
            altUpdateSquare(lbl12_11, 11, 10);
            altUpdateSquare(lbl12_12, 11, 11);
            altUpdateSquare(lbl12_13, 11, 12);
            altUpdateSquare(lbl12_14, 11, 13);
            altUpdateSquare(lbl12_15, 11, 14);
            altUpdateSquare(lbl12_16, 11, 15);

            altUpdateSquare(lbl13_1, 12, 0);
            altUpdateSquare(lbl13_2, 12, 1);
            altUpdateSquare(lbl13_3, 12, 2);
            altUpdateSquare(lbl13_4, 12, 3);
            altUpdateSquare(lbl13_5, 12, 4);
            altUpdateSquare(lbl13_6, 12, 5);
            altUpdateSquare(lbl13_7, 12, 6);
            altUpdateSquare(lbl13_8, 12, 7);
            altUpdateSquare(lbl13_9, 12, 8);
            altUpdateSquare(lbl13_10, 12, 9);
            altUpdateSquare(lbl13_11, 12, 10);
            altUpdateSquare(lbl13_12, 12, 11);
            altUpdateSquare(lbl13_13, 12, 12);
            altUpdateSquare(lbl13_14, 12, 13);
            altUpdateSquare(lbl13_15, 12, 14);
            altUpdateSquare(lbl13_16, 12, 15);

            altUpdateSquare(lbl14_1, 13, 0);
            altUpdateSquare(lbl14_2, 13, 1);
            altUpdateSquare(lbl14_3, 13, 2);
            altUpdateSquare(lbl14_4, 13, 3);
            altUpdateSquare(lbl14_5, 13, 4);
            altUpdateSquare(lbl14_6, 13, 5);
            altUpdateSquare(lbl14_7, 13, 6);
            altUpdateSquare(lbl14_8, 13, 7);
            altUpdateSquare(lbl14_9, 13, 8);
            altUpdateSquare(lbl14_10, 13, 9);
            altUpdateSquare(lbl14_11, 13, 10);
            altUpdateSquare(lbl14_12, 13, 11);
            altUpdateSquare(lbl14_13, 13, 12);
            altUpdateSquare(lbl14_14, 13, 13);
            altUpdateSquare(lbl14_15, 13, 14);
            altUpdateSquare(lbl14_16, 13, 15);

            altUpdateSquare(lbl15_1, 14, 0);
            altUpdateSquare(lbl15_2, 14, 1);
            altUpdateSquare(lbl15_3, 14, 2);
            altUpdateSquare(lbl15_4, 14, 3);
            altUpdateSquare(lbl15_5, 14, 4);
            altUpdateSquare(lbl15_6, 14, 5);
            altUpdateSquare(lbl15_7, 14, 6);
            altUpdateSquare(lbl15_8, 14, 7);
            altUpdateSquare(lbl15_9, 14, 8);
            altUpdateSquare(lbl15_10, 14, 9);
            altUpdateSquare(lbl15_11, 14, 10);
            altUpdateSquare(lbl15_12, 14, 11);
            altUpdateSquare(lbl15_13, 14, 12);
            altUpdateSquare(lbl15_14, 14, 13);
            altUpdateSquare(lbl15_15, 14, 14);
            altUpdateSquare(lbl15_16, 14, 15);

            altUpdateSquare(lbl16_1, 15, 0);
            altUpdateSquare(lbl16_2, 15, 1);
            altUpdateSquare(lbl16_3, 15, 2);
            altUpdateSquare(lbl16_4, 15, 3);
            altUpdateSquare(lbl16_5, 15, 4);
            altUpdateSquare(lbl16_6, 15, 5);
            altUpdateSquare(lbl16_7, 15, 6);
            altUpdateSquare(lbl16_8, 15, 7);
            altUpdateSquare(lbl16_9, 15, 8);
            altUpdateSquare(lbl16_10, 15, 9);
            altUpdateSquare(lbl16_11, 15, 10);
            altUpdateSquare(lbl16_12, 15, 11);
            altUpdateSquare(lbl16_13, 15, 12);
            altUpdateSquare(lbl16_14, 15, 13);
            altUpdateSquare(lbl16_15, 15, 14);
            altUpdateSquare(lbl16_16, 15, 15);

            updateResources();
        }
        private void altUpdateSquare(object lbl, int row, int col) //Updates individual square
        {
            Label thisLabel = (Label)lbl;
            if (map[row, col].Building == "R")
            {
                getRoad(thisLabel, row, col);
            }
            if (map[row, col].Building == "H")
            {
                getHouse(thisLabel, row, col);
            }
            if (map[row, col].Building == "F")
            {
                getFarm(thisLabel, row, col);
            }
            if (map[row, col].Building == "W")
            {
                //getWell
                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.Well);
                thisLabel.Image = bmp;
            }
            if (map[row, col].Building == "T")
            {
                //getTavern
                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.Tavern);
                thisLabel.Image = bmp;
                player.Workers -= 4;
            }
            if (map[row, col].Building == String.Empty)
            {
                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.Grass1);
                thisLabel.Image = bmp;
            }
        }

        private void getHouse(object lbl, int row, int col)
        {
            Label thisLabel = (Label)lbl;
            map[row, col].BuildingLevel = 0;
            if (map[row, col].NextToRoad > 0)
            {
                map[row, col].BuildingLevel = 1;
                if (map[row, col].NextToWell > 0)
                {
                    map[row, col].BuildingLevel = 2;
                    if (map[row, col].NextToTavern > 0)
                    {
                        map[row, col].BuildingLevel = 3;
                    }
                }
            }
            if (map[row, col].BuildingLevel == 0)
            {
                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.House0);
                thisLabel.Image = bmp;
            }
            if (map[row, col].BuildingLevel == 1)
            {
                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.House1);
                thisLabel.Image = bmp;
            }
            if (map[row, col].BuildingLevel == 2)
            {
                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.House2);
                thisLabel.Image = bmp;
            }
            if (map[row, col].BuildingLevel == 3)
            {
                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.House3);
                thisLabel.Image = bmp;
            }
            player.Workers += map[row, col].BuildingLevel;
            player.income += map[row, col].BuildingLevel * 2;
            player.Population += map[row, col].BuildingLevel;
            player.foodIncome -= map[row, col].BuildingLevel * 2;
        }

        private void getFarm(object lbl, int row, int col)
        {
            Label thisLabel = (Label)lbl;
            if (map[row, col].NextToRoad > 0)
            {
                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.Farm1);
                thisLabel.Image = bmp;
                player.foodIncome += 5;
                player.Workers -= 2;
                player.income -= 2;
            }
            else
            {
                var bmp = new Bitmap(DTB.CityBuilder.UI.Properties.Resources.House0);
                thisLabel.Image = bmp;
            }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            frmHelp help = new frmHelp();
            help.ShowDialog();
        }
    }
}
