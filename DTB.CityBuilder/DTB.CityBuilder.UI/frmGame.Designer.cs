﻿namespace DTB.CityBuilder.UI
{
    partial class frmGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGame));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.labelWhatever = new System.Windows.Forms.Label();
            this.lblMoney = new System.Windows.Forms.Label();
            this.lblPopulation = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.btnNextDay = new System.Windows.Forms.Button();
            this.grpBuild = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblTavernCost = new System.Windows.Forms.Label();
            this.lblWellCost = new System.Windows.Forms.Label();
            this.lblFarmCost = new System.Windows.Forms.Label();
            this.lblRoadCost = new System.Windows.Forms.Label();
            this.lblHouseCost = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblFood = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblWorkers = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lblFoodIncome = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lblIncome = new System.Windows.Forms.Label();
            this.lblDay = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbl16_16 = new System.Windows.Forms.Label();
            this.lbl16_15 = new System.Windows.Forms.Label();
            this.lbl16_14 = new System.Windows.Forms.Label();
            this.lbl16_13 = new System.Windows.Forms.Label();
            this.lbl16_12 = new System.Windows.Forms.Label();
            this.lbl16_11 = new System.Windows.Forms.Label();
            this.lbl16_10 = new System.Windows.Forms.Label();
            this.lbl16_9 = new System.Windows.Forms.Label();
            this.lbl16_8 = new System.Windows.Forms.Label();
            this.lbl16_7 = new System.Windows.Forms.Label();
            this.lbl16_6 = new System.Windows.Forms.Label();
            this.lbl16_5 = new System.Windows.Forms.Label();
            this.lbl16_4 = new System.Windows.Forms.Label();
            this.lbl16_3 = new System.Windows.Forms.Label();
            this.lbl16_2 = new System.Windows.Forms.Label();
            this.lbl16_1 = new System.Windows.Forms.Label();
            this.lbl1_16 = new System.Windows.Forms.Label();
            this.lbl1_15 = new System.Windows.Forms.Label();
            this.lbl1_14 = new System.Windows.Forms.Label();
            this.lbl1_13 = new System.Windows.Forms.Label();
            this.lbl1_12 = new System.Windows.Forms.Label();
            this.lbl1_11 = new System.Windows.Forms.Label();
            this.lbl1_10 = new System.Windows.Forms.Label();
            this.lbl1_9 = new System.Windows.Forms.Label();
            this.lbl1_8 = new System.Windows.Forms.Label();
            this.lbl1_7 = new System.Windows.Forms.Label();
            this.lbl1_6 = new System.Windows.Forms.Label();
            this.lbl1_5 = new System.Windows.Forms.Label();
            this.lbl1_4 = new System.Windows.Forms.Label();
            this.lbl1_3 = new System.Windows.Forms.Label();
            this.lbl1_2 = new System.Windows.Forms.Label();
            this.lbl1_1 = new System.Windows.Forms.Label();
            this.lbl15_16 = new System.Windows.Forms.Label();
            this.lbl15_15 = new System.Windows.Forms.Label();
            this.lbl15_14 = new System.Windows.Forms.Label();
            this.lbl15_13 = new System.Windows.Forms.Label();
            this.lbl15_12 = new System.Windows.Forms.Label();
            this.lbl15_11 = new System.Windows.Forms.Label();
            this.lbl15_10 = new System.Windows.Forms.Label();
            this.lbl15_9 = new System.Windows.Forms.Label();
            this.lbl15_8 = new System.Windows.Forms.Label();
            this.lbl15_7 = new System.Windows.Forms.Label();
            this.lbl15_6 = new System.Windows.Forms.Label();
            this.lbl15_5 = new System.Windows.Forms.Label();
            this.lbl15_4 = new System.Windows.Forms.Label();
            this.lbl15_3 = new System.Windows.Forms.Label();
            this.lbl15_2 = new System.Windows.Forms.Label();
            this.lbl15_1 = new System.Windows.Forms.Label();
            this.lbl14_16 = new System.Windows.Forms.Label();
            this.lbl14_15 = new System.Windows.Forms.Label();
            this.lbl14_14 = new System.Windows.Forms.Label();
            this.lbl14_13 = new System.Windows.Forms.Label();
            this.lbl14_12 = new System.Windows.Forms.Label();
            this.lbl14_11 = new System.Windows.Forms.Label();
            this.lbl14_10 = new System.Windows.Forms.Label();
            this.lbl14_9 = new System.Windows.Forms.Label();
            this.lbl14_8 = new System.Windows.Forms.Label();
            this.lbl14_7 = new System.Windows.Forms.Label();
            this.lbl14_6 = new System.Windows.Forms.Label();
            this.lbl14_5 = new System.Windows.Forms.Label();
            this.lbl14_4 = new System.Windows.Forms.Label();
            this.lbl14_3 = new System.Windows.Forms.Label();
            this.lbl14_2 = new System.Windows.Forms.Label();
            this.lbl14_1 = new System.Windows.Forms.Label();
            this.lbl13_16 = new System.Windows.Forms.Label();
            this.lbl13_15 = new System.Windows.Forms.Label();
            this.lbl13_14 = new System.Windows.Forms.Label();
            this.lbl13_13 = new System.Windows.Forms.Label();
            this.lbl13_12 = new System.Windows.Forms.Label();
            this.lbl13_11 = new System.Windows.Forms.Label();
            this.lbl13_10 = new System.Windows.Forms.Label();
            this.lbl13_9 = new System.Windows.Forms.Label();
            this.lbl13_8 = new System.Windows.Forms.Label();
            this.lbl13_7 = new System.Windows.Forms.Label();
            this.lbl13_6 = new System.Windows.Forms.Label();
            this.lbl13_5 = new System.Windows.Forms.Label();
            this.lbl13_4 = new System.Windows.Forms.Label();
            this.lbl13_3 = new System.Windows.Forms.Label();
            this.lbl13_2 = new System.Windows.Forms.Label();
            this.lbl13_1 = new System.Windows.Forms.Label();
            this.lbl12_16 = new System.Windows.Forms.Label();
            this.lbl12_15 = new System.Windows.Forms.Label();
            this.lbl12_14 = new System.Windows.Forms.Label();
            this.lbl12_13 = new System.Windows.Forms.Label();
            this.lbl12_12 = new System.Windows.Forms.Label();
            this.lbl12_11 = new System.Windows.Forms.Label();
            this.lbl12_10 = new System.Windows.Forms.Label();
            this.lbl12_9 = new System.Windows.Forms.Label();
            this.lbl12_8 = new System.Windows.Forms.Label();
            this.lbl12_7 = new System.Windows.Forms.Label();
            this.lbl12_6 = new System.Windows.Forms.Label();
            this.lbl12_5 = new System.Windows.Forms.Label();
            this.lbl12_4 = new System.Windows.Forms.Label();
            this.lbl12_3 = new System.Windows.Forms.Label();
            this.lbl12_2 = new System.Windows.Forms.Label();
            this.lbl12_1 = new System.Windows.Forms.Label();
            this.lbl11_16 = new System.Windows.Forms.Label();
            this.lbl11_15 = new System.Windows.Forms.Label();
            this.lbl11_14 = new System.Windows.Forms.Label();
            this.lbl11_13 = new System.Windows.Forms.Label();
            this.lbl11_12 = new System.Windows.Forms.Label();
            this.lbl11_11 = new System.Windows.Forms.Label();
            this.lbl11_10 = new System.Windows.Forms.Label();
            this.lbl11_9 = new System.Windows.Forms.Label();
            this.lbl11_8 = new System.Windows.Forms.Label();
            this.lbl11_7 = new System.Windows.Forms.Label();
            this.lbl11_6 = new System.Windows.Forms.Label();
            this.lbl11_5 = new System.Windows.Forms.Label();
            this.lbl11_4 = new System.Windows.Forms.Label();
            this.lbl11_3 = new System.Windows.Forms.Label();
            this.lbl11_2 = new System.Windows.Forms.Label();
            this.lbl11_1 = new System.Windows.Forms.Label();
            this.lbl10_16 = new System.Windows.Forms.Label();
            this.lbl10_15 = new System.Windows.Forms.Label();
            this.lbl10_14 = new System.Windows.Forms.Label();
            this.lbl10_13 = new System.Windows.Forms.Label();
            this.lbl10_12 = new System.Windows.Forms.Label();
            this.lbl10_11 = new System.Windows.Forms.Label();
            this.lbl10_10 = new System.Windows.Forms.Label();
            this.lbl10_9 = new System.Windows.Forms.Label();
            this.lbl10_8 = new System.Windows.Forms.Label();
            this.lbl10_7 = new System.Windows.Forms.Label();
            this.lbl10_6 = new System.Windows.Forms.Label();
            this.lbl10_5 = new System.Windows.Forms.Label();
            this.lbl10_4 = new System.Windows.Forms.Label();
            this.lbl10_3 = new System.Windows.Forms.Label();
            this.lbl10_2 = new System.Windows.Forms.Label();
            this.lbl10_1 = new System.Windows.Forms.Label();
            this.lbl9_16 = new System.Windows.Forms.Label();
            this.lbl9_15 = new System.Windows.Forms.Label();
            this.lbl9_14 = new System.Windows.Forms.Label();
            this.lbl9_13 = new System.Windows.Forms.Label();
            this.lbl9_12 = new System.Windows.Forms.Label();
            this.lbl9_11 = new System.Windows.Forms.Label();
            this.lbl9_10 = new System.Windows.Forms.Label();
            this.lbl9_9 = new System.Windows.Forms.Label();
            this.lbl9_8 = new System.Windows.Forms.Label();
            this.lbl9_7 = new System.Windows.Forms.Label();
            this.lbl9_6 = new System.Windows.Forms.Label();
            this.lbl9_5 = new System.Windows.Forms.Label();
            this.lbl9_4 = new System.Windows.Forms.Label();
            this.lbl9_3 = new System.Windows.Forms.Label();
            this.lbl9_2 = new System.Windows.Forms.Label();
            this.lbl9_1 = new System.Windows.Forms.Label();
            this.lbl8_16 = new System.Windows.Forms.Label();
            this.lbl8_15 = new System.Windows.Forms.Label();
            this.lbl8_14 = new System.Windows.Forms.Label();
            this.lbl8_13 = new System.Windows.Forms.Label();
            this.lbl8_12 = new System.Windows.Forms.Label();
            this.lbl8_11 = new System.Windows.Forms.Label();
            this.lbl8_10 = new System.Windows.Forms.Label();
            this.lbl8_9 = new System.Windows.Forms.Label();
            this.lbl8_8 = new System.Windows.Forms.Label();
            this.lbl8_7 = new System.Windows.Forms.Label();
            this.lbl8_6 = new System.Windows.Forms.Label();
            this.lbl8_5 = new System.Windows.Forms.Label();
            this.lbl8_4 = new System.Windows.Forms.Label();
            this.lbl8_3 = new System.Windows.Forms.Label();
            this.lbl8_2 = new System.Windows.Forms.Label();
            this.lbl8_1 = new System.Windows.Forms.Label();
            this.lbl7_16 = new System.Windows.Forms.Label();
            this.lbl7_15 = new System.Windows.Forms.Label();
            this.lbl7_14 = new System.Windows.Forms.Label();
            this.lbl7_13 = new System.Windows.Forms.Label();
            this.lbl7_12 = new System.Windows.Forms.Label();
            this.lbl7_11 = new System.Windows.Forms.Label();
            this.lbl7_10 = new System.Windows.Forms.Label();
            this.lbl7_9 = new System.Windows.Forms.Label();
            this.lbl7_8 = new System.Windows.Forms.Label();
            this.lbl7_7 = new System.Windows.Forms.Label();
            this.lbl7_6 = new System.Windows.Forms.Label();
            this.lbl7_5 = new System.Windows.Forms.Label();
            this.lbl7_4 = new System.Windows.Forms.Label();
            this.lbl7_3 = new System.Windows.Forms.Label();
            this.lbl7_2 = new System.Windows.Forms.Label();
            this.lbl7_1 = new System.Windows.Forms.Label();
            this.lbl2_16 = new System.Windows.Forms.Label();
            this.lbl2_15 = new System.Windows.Forms.Label();
            this.lbl2_14 = new System.Windows.Forms.Label();
            this.lbl2_13 = new System.Windows.Forms.Label();
            this.lbl2_12 = new System.Windows.Forms.Label();
            this.lbl2_11 = new System.Windows.Forms.Label();
            this.lbl2_10 = new System.Windows.Forms.Label();
            this.lbl2_9 = new System.Windows.Forms.Label();
            this.lbl2_8 = new System.Windows.Forms.Label();
            this.lbl2_7 = new System.Windows.Forms.Label();
            this.lbl2_6 = new System.Windows.Forms.Label();
            this.lbl2_5 = new System.Windows.Forms.Label();
            this.lbl2_4 = new System.Windows.Forms.Label();
            this.lbl2_3 = new System.Windows.Forms.Label();
            this.lbl2_2 = new System.Windows.Forms.Label();
            this.lbl2_1 = new System.Windows.Forms.Label();
            this.lbl3_16 = new System.Windows.Forms.Label();
            this.lbl3_15 = new System.Windows.Forms.Label();
            this.lbl3_14 = new System.Windows.Forms.Label();
            this.lbl3_13 = new System.Windows.Forms.Label();
            this.lbl3_12 = new System.Windows.Forms.Label();
            this.lbl3_11 = new System.Windows.Forms.Label();
            this.lbl3_10 = new System.Windows.Forms.Label();
            this.lbl3_9 = new System.Windows.Forms.Label();
            this.lbl3_8 = new System.Windows.Forms.Label();
            this.lbl3_7 = new System.Windows.Forms.Label();
            this.lbl3_6 = new System.Windows.Forms.Label();
            this.lbl3_5 = new System.Windows.Forms.Label();
            this.lbl3_4 = new System.Windows.Forms.Label();
            this.lbl3_3 = new System.Windows.Forms.Label();
            this.lbl3_2 = new System.Windows.Forms.Label();
            this.lbl3_1 = new System.Windows.Forms.Label();
            this.lbl6_16 = new System.Windows.Forms.Label();
            this.lbl6_15 = new System.Windows.Forms.Label();
            this.lbl6_14 = new System.Windows.Forms.Label();
            this.lbl6_13 = new System.Windows.Forms.Label();
            this.lbl6_12 = new System.Windows.Forms.Label();
            this.lbl6_11 = new System.Windows.Forms.Label();
            this.lbl6_10 = new System.Windows.Forms.Label();
            this.lbl6_9 = new System.Windows.Forms.Label();
            this.lbl6_8 = new System.Windows.Forms.Label();
            this.lbl6_7 = new System.Windows.Forms.Label();
            this.lbl6_6 = new System.Windows.Forms.Label();
            this.lbl6_5 = new System.Windows.Forms.Label();
            this.lbl6_4 = new System.Windows.Forms.Label();
            this.lbl6_3 = new System.Windows.Forms.Label();
            this.lbl6_2 = new System.Windows.Forms.Label();
            this.lbl6_1 = new System.Windows.Forms.Label();
            this.lbl5_16 = new System.Windows.Forms.Label();
            this.lbl5_15 = new System.Windows.Forms.Label();
            this.lbl5_14 = new System.Windows.Forms.Label();
            this.lbl5_13 = new System.Windows.Forms.Label();
            this.lbl5_12 = new System.Windows.Forms.Label();
            this.lbl5_11 = new System.Windows.Forms.Label();
            this.lbl5_10 = new System.Windows.Forms.Label();
            this.lbl5_9 = new System.Windows.Forms.Label();
            this.lbl5_8 = new System.Windows.Forms.Label();
            this.lbl5_7 = new System.Windows.Forms.Label();
            this.lbl5_6 = new System.Windows.Forms.Label();
            this.lbl5_5 = new System.Windows.Forms.Label();
            this.lbl5_4 = new System.Windows.Forms.Label();
            this.lbl5_3 = new System.Windows.Forms.Label();
            this.lbl5_2 = new System.Windows.Forms.Label();
            this.lbl5_1 = new System.Windows.Forms.Label();
            this.lbl4_16 = new System.Windows.Forms.Label();
            this.lbl4_15 = new System.Windows.Forms.Label();
            this.lbl4_14 = new System.Windows.Forms.Label();
            this.lbl4_13 = new System.Windows.Forms.Label();
            this.lbl4_12 = new System.Windows.Forms.Label();
            this.lbl4_11 = new System.Windows.Forms.Label();
            this.lbl4_10 = new System.Windows.Forms.Label();
            this.lbl4_9 = new System.Windows.Forms.Label();
            this.lbl4_8 = new System.Windows.Forms.Label();
            this.lbl4_7 = new System.Windows.Forms.Label();
            this.lbl4_6 = new System.Windows.Forms.Label();
            this.lbl4_5 = new System.Windows.Forms.Label();
            this.lbl4_4 = new System.Windows.Forms.Label();
            this.lbl4_3 = new System.Windows.Forms.Label();
            this.lbl4_2 = new System.Windows.Forms.Label();
            this.lbl4_1 = new System.Windows.Forms.Label();
            this.btnHelp = new System.Windows.Forms.Button();
            this.grpBuild.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 256;
            this.label1.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(78, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 257;
            this.label2.Text = "2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(108, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 258;
            this.label3.Text = "3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(138, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 259;
            this.label4.Text = "4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(168, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 260;
            this.label5.Text = "5";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(198, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 261;
            this.label11.Text = "6";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(227, 9);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(13, 13);
            this.label48.TabIndex = 262;
            this.label48.Text = "7";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(258, 9);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(13, 13);
            this.label49.TabIndex = 263;
            this.label49.Text = "8";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(288, 9);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(13, 13);
            this.label50.TabIndex = 264;
            this.label50.Text = "9";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(312, 9);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(19, 13);
            this.label51.TabIndex = 265;
            this.label51.Text = "10";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(342, 9);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(19, 13);
            this.label52.TabIndex = 266;
            this.label52.Text = "11";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(372, 9);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(19, 13);
            this.label53.TabIndex = 267;
            this.label53.Text = "12";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(402, 9);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(19, 13);
            this.label54.TabIndex = 268;
            this.label54.Text = "13";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(432, 9);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(19, 13);
            this.label55.TabIndex = 269;
            this.label55.Text = "14";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(462, 9);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(19, 13);
            this.label56.TabIndex = 270;
            this.label56.Text = "15";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(492, 9);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(19, 13);
            this.label57.TabIndex = 271;
            this.label57.Text = "16";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(12, 39);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(13, 13);
            this.label59.TabIndex = 273;
            this.label59.Text = "1";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(12, 99);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(13, 13);
            this.label60.TabIndex = 274;
            this.label60.Text = "3";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(12, 69);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(13, 13);
            this.label61.TabIndex = 275;
            this.label61.Text = "2";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(11, 129);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(13, 13);
            this.label62.TabIndex = 276;
            this.label62.Text = "4";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(12, 279);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(13, 13);
            this.label63.TabIndex = 277;
            this.label63.Text = "9";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(11, 249);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(13, 13);
            this.label64.TabIndex = 278;
            this.label64.Text = "8";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(11, 219);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(13, 13);
            this.label65.TabIndex = 279;
            this.label65.Text = "7";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(12, 189);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(13, 13);
            this.label66.TabIndex = 280;
            this.label66.Text = "6";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(12, 159);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(13, 13);
            this.label67.TabIndex = 281;
            this.label67.Text = "5";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(5, 309);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(19, 13);
            this.label68.TabIndex = 282;
            this.label68.Text = "10";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(6, 339);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(19, 13);
            this.label69.TabIndex = 283;
            this.label69.Text = "11";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(5, 369);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(19, 13);
            this.label70.TabIndex = 284;
            this.label70.Text = "12";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(5, 399);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(19, 13);
            this.label71.TabIndex = 285;
            this.label71.Text = "13";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(5, 429);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(19, 13);
            this.label72.TabIndex = 286;
            this.label72.Text = "14";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(5, 459);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(19, 13);
            this.label73.TabIndex = 287;
            this.label73.Text = "15";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(5, 489);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(19, 13);
            this.label74.TabIndex = 288;
            this.label74.Text = "16";
            // 
            // labelWhatever
            // 
            this.labelWhatever.AutoSize = true;
            this.labelWhatever.Location = new System.Drawing.Point(41, 40);
            this.labelWhatever.Name = "labelWhatever";
            this.labelWhatever.Size = new System.Drawing.Size(42, 13);
            this.labelWhatever.TabIndex = 290;
            this.labelWhatever.Text = "Money:";
            // 
            // lblMoney
            // 
            this.lblMoney.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMoney.Location = new System.Drawing.Point(89, 35);
            this.lblMoney.Name = "lblMoney";
            this.lblMoney.Size = new System.Drawing.Size(105, 23);
            this.lblMoney.TabIndex = 291;
            this.lblMoney.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPopulation
            // 
            this.lblPopulation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPopulation.Location = new System.Drawing.Point(89, 68);
            this.lblPopulation.Name = "lblPopulation";
            this.lblPopulation.Size = new System.Drawing.Size(105, 23);
            this.lblPopulation.TabIndex = 292;
            this.lblPopulation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(23, 73);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(60, 13);
            this.label76.TabIndex = 293;
            this.label76.Text = "Population:";
            // 
            // btnNextDay
            // 
            this.btnNextDay.Location = new System.Drawing.Point(210, 552);
            this.btnNextDay.Name = "btnNextDay";
            this.btnNextDay.Size = new System.Drawing.Size(110, 36);
            this.btnNextDay.TabIndex = 294;
            this.btnNextDay.Text = "End Day";
            this.btnNextDay.UseVisualStyleBackColor = true;
            this.btnNextDay.Click += new System.EventHandler(this.btnNextDay_Click);
            // 
            // grpBuild
            // 
            this.grpBuild.Controls.Add(this.label12);
            this.grpBuild.Controls.Add(this.label13);
            this.grpBuild.Controls.Add(this.label14);
            this.grpBuild.Controls.Add(this.label17);
            this.grpBuild.Controls.Add(this.label18);
            this.grpBuild.Controls.Add(this.label19);
            this.grpBuild.Controls.Add(this.lblTavernCost);
            this.grpBuild.Controls.Add(this.lblWellCost);
            this.grpBuild.Controls.Add(this.lblFarmCost);
            this.grpBuild.Controls.Add(this.lblRoadCost);
            this.grpBuild.Controls.Add(this.lblHouseCost);
            this.grpBuild.Controls.Add(this.label10);
            this.grpBuild.Controls.Add(this.label9);
            this.grpBuild.Controls.Add(this.label8);
            this.grpBuild.Controls.Add(this.label7);
            this.grpBuild.Controls.Add(this.label6);
            this.grpBuild.Location = new System.Drawing.Point(603, 24);
            this.grpBuild.Name = "grpBuild";
            this.grpBuild.Size = new System.Drawing.Size(390, 165);
            this.grpBuild.TabIndex = 295;
            this.grpBuild.TabStop = false;
            this.grpBuild.Text = "Build and Upgrade";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(257, 121);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "500";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(257, 98);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(41, 13);
            this.label13.TabIndex = 18;
            this.label13.Text = "100";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(257, 75);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 17;
            this.label14.Text = "100";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(172, 121);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 13);
            this.label17.TabIndex = 14;
            this.label17.Text = "Tavern Upgrade:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(185, 98);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(75, 13);
            this.label18.TabIndex = 13;
            this.label18.Text = "Well Upgrade:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(183, 75);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 13);
            this.label19.TabIndex = 12;
            this.label19.Text = "Farm Upgrade:";
            // 
            // lblTavernCost
            // 
            this.lblTavernCost.Location = new System.Drawing.Point(92, 121);
            this.lblTavernCost.Name = "lblTavernCost";
            this.lblTavernCost.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblTavernCost.Size = new System.Drawing.Size(41, 13);
            this.lblTavernCost.TabIndex = 9;
            this.lblTavernCost.Text = "100";
            // 
            // lblWellCost
            // 
            this.lblWellCost.Location = new System.Drawing.Point(92, 98);
            this.lblWellCost.Name = "lblWellCost";
            this.lblWellCost.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblWellCost.Size = new System.Drawing.Size(41, 13);
            this.lblWellCost.TabIndex = 8;
            this.lblWellCost.Text = "20";
            // 
            // lblFarmCost
            // 
            this.lblFarmCost.Location = new System.Drawing.Point(92, 75);
            this.lblFarmCost.Name = "lblFarmCost";
            this.lblFarmCost.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblFarmCost.Size = new System.Drawing.Size(41, 13);
            this.lblFarmCost.TabIndex = 7;
            this.lblFarmCost.Text = "20";
            // 
            // lblRoadCost
            // 
            this.lblRoadCost.Location = new System.Drawing.Point(92, 52);
            this.lblRoadCost.Name = "lblRoadCost";
            this.lblRoadCost.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblRoadCost.Size = new System.Drawing.Size(41, 13);
            this.lblRoadCost.TabIndex = 6;
            this.lblRoadCost.Text = "10";
            // 
            // lblHouseCost
            // 
            this.lblHouseCost.Location = new System.Drawing.Point(92, 29);
            this.lblHouseCost.Name = "lblHouseCost";
            this.lblHouseCost.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblHouseCost.Size = new System.Drawing.Size(41, 13);
            this.lblHouseCost.TabIndex = 5;
            this.lblHouseCost.Text = "10";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 121);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Tavern Cost:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Well Cost:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Farm Cost:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Road Cost:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "House Cost:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(49, 140);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 13);
            this.label15.TabIndex = 297;
            this.label15.Text = "Food:";
            // 
            // lblFood
            // 
            this.lblFood.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFood.Location = new System.Drawing.Point(89, 135);
            this.lblFood.Name = "lblFood";
            this.lblFood.Size = new System.Drawing.Size(105, 23);
            this.lblFood.TabIndex = 296;
            this.lblFood.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblWorkers);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.lblFoodIncome);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.lblIncome);
            this.groupBox1.Controls.Add(this.lblMoney);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.labelWhatever);
            this.groupBox1.Controls.Add(this.lblFood);
            this.groupBox1.Controls.Add(this.lblPopulation);
            this.groupBox1.Controls.Add(this.label76);
            this.groupBox1.Location = new System.Drawing.Point(603, 234);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(249, 277);
            this.groupBox1.TabIndex = 298;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Resources";
            // 
            // lblWorkers
            // 
            this.lblWorkers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblWorkers.Location = new System.Drawing.Point(89, 100);
            this.lblWorkers.Name = "lblWorkers";
            this.lblWorkers.Size = new System.Drawing.Size(105, 23);
            this.lblWorkers.TabIndex = 303;
            this.lblWorkers.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(30, 105);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(50, 13);
            this.label22.TabIndex = 302;
            this.label22.Text = "Workers:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(11, 199);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(72, 13);
            this.label21.TabIndex = 301;
            this.label21.Text = "Food Income:";
            // 
            // lblFoodIncome
            // 
            this.lblFoodIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFoodIncome.Location = new System.Drawing.Point(89, 194);
            this.lblFoodIncome.Name = "lblFoodIncome";
            this.lblFoodIncome.Size = new System.Drawing.Size(105, 23);
            this.lblFoodIncome.TabIndex = 300;
            this.lblFoodIncome.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(38, 169);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(45, 13);
            this.label20.TabIndex = 299;
            this.label20.Text = "Income:";
            // 
            // lblIncome
            // 
            this.lblIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIncome.Location = new System.Drawing.Point(89, 164);
            this.lblIncome.Name = "lblIncome";
            this.lblIncome.Size = new System.Drawing.Size(105, 23);
            this.lblIncome.TabIndex = 298;
            this.lblIncome.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDay
            // 
            this.lblDay.AutoSize = true;
            this.lblDay.Location = new System.Drawing.Point(402, 564);
            this.lblDay.Name = "lblDay";
            this.lblDay.Size = new System.Drawing.Size(13, 13);
            this.lblDay.TabIndex = 299;
            this.lblDay.Text = "1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(360, 564);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 13);
            this.label16.TabIndex = 300;
            this.label16.Text = "Day:";
            // 
            // lbl16_16
            // 
            this.lbl16_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16_16.Image = ((System.Drawing.Image)(resources.GetObject("lbl16_16.Image")));
            this.lbl16_16.Location = new System.Drawing.Point(480, 480);
            this.lbl16_16.Name = "lbl16_16";
            this.lbl16_16.Size = new System.Drawing.Size(31, 31);
            this.lbl16_16.TabIndex = 255;
            this.lbl16_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl16_16.Click += new System.EventHandler(this.map_click);
            // 
            // lbl16_15
            // 
            this.lbl16_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16_15.Image = ((System.Drawing.Image)(resources.GetObject("lbl16_15.Image")));
            this.lbl16_15.Location = new System.Drawing.Point(450, 480);
            this.lbl16_15.Name = "lbl16_15";
            this.lbl16_15.Size = new System.Drawing.Size(31, 31);
            this.lbl16_15.TabIndex = 254;
            this.lbl16_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl16_15.Click += new System.EventHandler(this.map_click);
            // 
            // lbl16_14
            // 
            this.lbl16_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16_14.Image = ((System.Drawing.Image)(resources.GetObject("lbl16_14.Image")));
            this.lbl16_14.Location = new System.Drawing.Point(420, 480);
            this.lbl16_14.Name = "lbl16_14";
            this.lbl16_14.Size = new System.Drawing.Size(31, 31);
            this.lbl16_14.TabIndex = 253;
            this.lbl16_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl16_14.Click += new System.EventHandler(this.map_click);
            // 
            // lbl16_13
            // 
            this.lbl16_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16_13.Image = ((System.Drawing.Image)(resources.GetObject("lbl16_13.Image")));
            this.lbl16_13.Location = new System.Drawing.Point(390, 480);
            this.lbl16_13.Name = "lbl16_13";
            this.lbl16_13.Size = new System.Drawing.Size(31, 31);
            this.lbl16_13.TabIndex = 252;
            this.lbl16_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl16_13.Click += new System.EventHandler(this.map_click);
            // 
            // lbl16_12
            // 
            this.lbl16_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16_12.Image = ((System.Drawing.Image)(resources.GetObject("lbl16_12.Image")));
            this.lbl16_12.Location = new System.Drawing.Point(360, 480);
            this.lbl16_12.Name = "lbl16_12";
            this.lbl16_12.Size = new System.Drawing.Size(31, 31);
            this.lbl16_12.TabIndex = 251;
            this.lbl16_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl16_12.Click += new System.EventHandler(this.map_click);
            // 
            // lbl16_11
            // 
            this.lbl16_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16_11.Image = ((System.Drawing.Image)(resources.GetObject("lbl16_11.Image")));
            this.lbl16_11.Location = new System.Drawing.Point(330, 480);
            this.lbl16_11.Name = "lbl16_11";
            this.lbl16_11.Size = new System.Drawing.Size(31, 31);
            this.lbl16_11.TabIndex = 250;
            this.lbl16_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl16_11.Click += new System.EventHandler(this.map_click);
            // 
            // lbl16_10
            // 
            this.lbl16_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16_10.Image = ((System.Drawing.Image)(resources.GetObject("lbl16_10.Image")));
            this.lbl16_10.Location = new System.Drawing.Point(300, 480);
            this.lbl16_10.Name = "lbl16_10";
            this.lbl16_10.Size = new System.Drawing.Size(31, 31);
            this.lbl16_10.TabIndex = 249;
            this.lbl16_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl16_10.Click += new System.EventHandler(this.map_click);
            // 
            // lbl16_9
            // 
            this.lbl16_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16_9.Image = ((System.Drawing.Image)(resources.GetObject("lbl16_9.Image")));
            this.lbl16_9.Location = new System.Drawing.Point(270, 480);
            this.lbl16_9.Name = "lbl16_9";
            this.lbl16_9.Size = new System.Drawing.Size(31, 31);
            this.lbl16_9.TabIndex = 248;
            this.lbl16_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl16_9.Click += new System.EventHandler(this.map_click);
            // 
            // lbl16_8
            // 
            this.lbl16_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16_8.Image = ((System.Drawing.Image)(resources.GetObject("lbl16_8.Image")));
            this.lbl16_8.Location = new System.Drawing.Point(240, 480);
            this.lbl16_8.Name = "lbl16_8";
            this.lbl16_8.Size = new System.Drawing.Size(31, 31);
            this.lbl16_8.TabIndex = 247;
            this.lbl16_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl16_8.Click += new System.EventHandler(this.map_click);
            // 
            // lbl16_7
            // 
            this.lbl16_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16_7.Image = ((System.Drawing.Image)(resources.GetObject("lbl16_7.Image")));
            this.lbl16_7.Location = new System.Drawing.Point(210, 480);
            this.lbl16_7.Name = "lbl16_7";
            this.lbl16_7.Size = new System.Drawing.Size(31, 31);
            this.lbl16_7.TabIndex = 246;
            this.lbl16_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl16_7.Click += new System.EventHandler(this.map_click);
            // 
            // lbl16_6
            // 
            this.lbl16_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16_6.Image = ((System.Drawing.Image)(resources.GetObject("lbl16_6.Image")));
            this.lbl16_6.Location = new System.Drawing.Point(180, 480);
            this.lbl16_6.Name = "lbl16_6";
            this.lbl16_6.Size = new System.Drawing.Size(31, 31);
            this.lbl16_6.TabIndex = 245;
            this.lbl16_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl16_6.Click += new System.EventHandler(this.map_click);
            // 
            // lbl16_5
            // 
            this.lbl16_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16_5.Image = ((System.Drawing.Image)(resources.GetObject("lbl16_5.Image")));
            this.lbl16_5.Location = new System.Drawing.Point(150, 480);
            this.lbl16_5.Name = "lbl16_5";
            this.lbl16_5.Size = new System.Drawing.Size(31, 31);
            this.lbl16_5.TabIndex = 244;
            this.lbl16_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl16_5.Click += new System.EventHandler(this.map_click);
            // 
            // lbl16_4
            // 
            this.lbl16_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16_4.Image = ((System.Drawing.Image)(resources.GetObject("lbl16_4.Image")));
            this.lbl16_4.Location = new System.Drawing.Point(120, 480);
            this.lbl16_4.Name = "lbl16_4";
            this.lbl16_4.Size = new System.Drawing.Size(31, 31);
            this.lbl16_4.TabIndex = 243;
            this.lbl16_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl16_4.Click += new System.EventHandler(this.map_click);
            // 
            // lbl16_3
            // 
            this.lbl16_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16_3.Image = ((System.Drawing.Image)(resources.GetObject("lbl16_3.Image")));
            this.lbl16_3.Location = new System.Drawing.Point(90, 480);
            this.lbl16_3.Name = "lbl16_3";
            this.lbl16_3.Size = new System.Drawing.Size(31, 31);
            this.lbl16_3.TabIndex = 242;
            this.lbl16_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl16_3.Click += new System.EventHandler(this.map_click);
            // 
            // lbl16_2
            // 
            this.lbl16_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16_2.Image = ((System.Drawing.Image)(resources.GetObject("lbl16_2.Image")));
            this.lbl16_2.Location = new System.Drawing.Point(60, 480);
            this.lbl16_2.Name = "lbl16_2";
            this.lbl16_2.Size = new System.Drawing.Size(31, 31);
            this.lbl16_2.TabIndex = 241;
            this.lbl16_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl16_2.Click += new System.EventHandler(this.map_click);
            // 
            // lbl16_1
            // 
            this.lbl16_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16_1.Image = ((System.Drawing.Image)(resources.GetObject("lbl16_1.Image")));
            this.lbl16_1.Location = new System.Drawing.Point(30, 480);
            this.lbl16_1.Name = "lbl16_1";
            this.lbl16_1.Size = new System.Drawing.Size(31, 31);
            this.lbl16_1.TabIndex = 240;
            this.lbl16_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl16_1.Click += new System.EventHandler(this.map_click);
            // 
            // lbl1_16
            // 
            this.lbl1_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl1_16.Image = ((System.Drawing.Image)(resources.GetObject("lbl1_16.Image")));
            this.lbl1_16.Location = new System.Drawing.Point(480, 30);
            this.lbl1_16.Name = "lbl1_16";
            this.lbl1_16.Size = new System.Drawing.Size(31, 31);
            this.lbl1_16.TabIndex = 239;
            this.lbl1_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl1_16.Click += new System.EventHandler(this.map_click);
            // 
            // lbl1_15
            // 
            this.lbl1_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl1_15.Image = ((System.Drawing.Image)(resources.GetObject("lbl1_15.Image")));
            this.lbl1_15.Location = new System.Drawing.Point(450, 30);
            this.lbl1_15.Name = "lbl1_15";
            this.lbl1_15.Size = new System.Drawing.Size(31, 31);
            this.lbl1_15.TabIndex = 238;
            this.lbl1_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl1_15.Click += new System.EventHandler(this.map_click);
            // 
            // lbl1_14
            // 
            this.lbl1_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl1_14.Image = ((System.Drawing.Image)(resources.GetObject("lbl1_14.Image")));
            this.lbl1_14.Location = new System.Drawing.Point(420, 30);
            this.lbl1_14.Name = "lbl1_14";
            this.lbl1_14.Size = new System.Drawing.Size(31, 31);
            this.lbl1_14.TabIndex = 237;
            this.lbl1_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl1_14.Click += new System.EventHandler(this.map_click);
            // 
            // lbl1_13
            // 
            this.lbl1_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl1_13.Image = ((System.Drawing.Image)(resources.GetObject("lbl1_13.Image")));
            this.lbl1_13.Location = new System.Drawing.Point(390, 30);
            this.lbl1_13.Name = "lbl1_13";
            this.lbl1_13.Size = new System.Drawing.Size(31, 31);
            this.lbl1_13.TabIndex = 236;
            this.lbl1_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl1_13.Click += new System.EventHandler(this.map_click);
            // 
            // lbl1_12
            // 
            this.lbl1_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl1_12.Image = ((System.Drawing.Image)(resources.GetObject("lbl1_12.Image")));
            this.lbl1_12.Location = new System.Drawing.Point(360, 30);
            this.lbl1_12.Name = "lbl1_12";
            this.lbl1_12.Size = new System.Drawing.Size(31, 31);
            this.lbl1_12.TabIndex = 235;
            this.lbl1_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl1_12.Click += new System.EventHandler(this.map_click);
            // 
            // lbl1_11
            // 
            this.lbl1_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl1_11.Image = ((System.Drawing.Image)(resources.GetObject("lbl1_11.Image")));
            this.lbl1_11.Location = new System.Drawing.Point(330, 30);
            this.lbl1_11.Name = "lbl1_11";
            this.lbl1_11.Size = new System.Drawing.Size(31, 31);
            this.lbl1_11.TabIndex = 234;
            this.lbl1_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl1_11.Click += new System.EventHandler(this.map_click);
            // 
            // lbl1_10
            // 
            this.lbl1_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl1_10.Image = ((System.Drawing.Image)(resources.GetObject("lbl1_10.Image")));
            this.lbl1_10.Location = new System.Drawing.Point(300, 30);
            this.lbl1_10.Name = "lbl1_10";
            this.lbl1_10.Size = new System.Drawing.Size(31, 31);
            this.lbl1_10.TabIndex = 233;
            this.lbl1_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl1_10.Click += new System.EventHandler(this.map_click);
            // 
            // lbl1_9
            // 
            this.lbl1_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl1_9.Image = ((System.Drawing.Image)(resources.GetObject("lbl1_9.Image")));
            this.lbl1_9.Location = new System.Drawing.Point(270, 30);
            this.lbl1_9.Name = "lbl1_9";
            this.lbl1_9.Size = new System.Drawing.Size(31, 31);
            this.lbl1_9.TabIndex = 232;
            this.lbl1_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl1_9.Click += new System.EventHandler(this.map_click);
            // 
            // lbl1_8
            // 
            this.lbl1_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl1_8.Image = ((System.Drawing.Image)(resources.GetObject("lbl1_8.Image")));
            this.lbl1_8.Location = new System.Drawing.Point(240, 30);
            this.lbl1_8.Name = "lbl1_8";
            this.lbl1_8.Size = new System.Drawing.Size(31, 31);
            this.lbl1_8.TabIndex = 231;
            this.lbl1_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl1_8.Click += new System.EventHandler(this.map_click);
            // 
            // lbl1_7
            // 
            this.lbl1_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl1_7.Image = ((System.Drawing.Image)(resources.GetObject("lbl1_7.Image")));
            this.lbl1_7.Location = new System.Drawing.Point(210, 30);
            this.lbl1_7.Name = "lbl1_7";
            this.lbl1_7.Size = new System.Drawing.Size(31, 31);
            this.lbl1_7.TabIndex = 230;
            this.lbl1_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl1_7.Click += new System.EventHandler(this.map_click);
            // 
            // lbl1_6
            // 
            this.lbl1_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl1_6.Image = ((System.Drawing.Image)(resources.GetObject("lbl1_6.Image")));
            this.lbl1_6.Location = new System.Drawing.Point(180, 30);
            this.lbl1_6.Name = "lbl1_6";
            this.lbl1_6.Size = new System.Drawing.Size(31, 31);
            this.lbl1_6.TabIndex = 229;
            this.lbl1_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl1_6.Click += new System.EventHandler(this.map_click);
            // 
            // lbl1_5
            // 
            this.lbl1_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl1_5.Image = ((System.Drawing.Image)(resources.GetObject("lbl1_5.Image")));
            this.lbl1_5.Location = new System.Drawing.Point(150, 30);
            this.lbl1_5.Name = "lbl1_5";
            this.lbl1_5.Size = new System.Drawing.Size(31, 31);
            this.lbl1_5.TabIndex = 228;
            this.lbl1_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl1_5.Click += new System.EventHandler(this.map_click);
            // 
            // lbl1_4
            // 
            this.lbl1_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl1_4.Image = ((System.Drawing.Image)(resources.GetObject("lbl1_4.Image")));
            this.lbl1_4.Location = new System.Drawing.Point(120, 30);
            this.lbl1_4.Name = "lbl1_4";
            this.lbl1_4.Size = new System.Drawing.Size(31, 31);
            this.lbl1_4.TabIndex = 227;
            this.lbl1_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl1_4.Click += new System.EventHandler(this.map_click);
            // 
            // lbl1_3
            // 
            this.lbl1_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl1_3.Image = ((System.Drawing.Image)(resources.GetObject("lbl1_3.Image")));
            this.lbl1_3.Location = new System.Drawing.Point(90, 30);
            this.lbl1_3.Name = "lbl1_3";
            this.lbl1_3.Size = new System.Drawing.Size(31, 31);
            this.lbl1_3.TabIndex = 226;
            this.lbl1_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl1_3.Click += new System.EventHandler(this.map_click);
            // 
            // lbl1_2
            // 
            this.lbl1_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl1_2.Image = ((System.Drawing.Image)(resources.GetObject("lbl1_2.Image")));
            this.lbl1_2.Location = new System.Drawing.Point(60, 30);
            this.lbl1_2.Name = "lbl1_2";
            this.lbl1_2.Size = new System.Drawing.Size(31, 31);
            this.lbl1_2.TabIndex = 225;
            this.lbl1_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl1_2.Click += new System.EventHandler(this.map_click);
            // 
            // lbl1_1
            // 
            this.lbl1_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl1_1.Image = ((System.Drawing.Image)(resources.GetObject("lbl1_1.Image")));
            this.lbl1_1.Location = new System.Drawing.Point(30, 30);
            this.lbl1_1.Name = "lbl1_1";
            this.lbl1_1.Size = new System.Drawing.Size(31, 31);
            this.lbl1_1.TabIndex = 224;
            this.lbl1_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl1_1.Click += new System.EventHandler(this.map_click);
            // 
            // lbl15_16
            // 
            this.lbl15_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15_16.Image = ((System.Drawing.Image)(resources.GetObject("lbl15_16.Image")));
            this.lbl15_16.Location = new System.Drawing.Point(480, 450);
            this.lbl15_16.Name = "lbl15_16";
            this.lbl15_16.Size = new System.Drawing.Size(31, 31);
            this.lbl15_16.TabIndex = 223;
            this.lbl15_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl15_16.Click += new System.EventHandler(this.map_click);
            // 
            // lbl15_15
            // 
            this.lbl15_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15_15.Image = ((System.Drawing.Image)(resources.GetObject("lbl15_15.Image")));
            this.lbl15_15.Location = new System.Drawing.Point(450, 450);
            this.lbl15_15.Name = "lbl15_15";
            this.lbl15_15.Size = new System.Drawing.Size(31, 31);
            this.lbl15_15.TabIndex = 222;
            this.lbl15_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl15_15.Click += new System.EventHandler(this.map_click);
            // 
            // lbl15_14
            // 
            this.lbl15_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15_14.Image = ((System.Drawing.Image)(resources.GetObject("lbl15_14.Image")));
            this.lbl15_14.Location = new System.Drawing.Point(420, 450);
            this.lbl15_14.Name = "lbl15_14";
            this.lbl15_14.Size = new System.Drawing.Size(31, 31);
            this.lbl15_14.TabIndex = 221;
            this.lbl15_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl15_14.Click += new System.EventHandler(this.map_click);
            // 
            // lbl15_13
            // 
            this.lbl15_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15_13.Image = ((System.Drawing.Image)(resources.GetObject("lbl15_13.Image")));
            this.lbl15_13.Location = new System.Drawing.Point(390, 450);
            this.lbl15_13.Name = "lbl15_13";
            this.lbl15_13.Size = new System.Drawing.Size(31, 31);
            this.lbl15_13.TabIndex = 220;
            this.lbl15_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl15_13.Click += new System.EventHandler(this.map_click);
            // 
            // lbl15_12
            // 
            this.lbl15_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15_12.Image = ((System.Drawing.Image)(resources.GetObject("lbl15_12.Image")));
            this.lbl15_12.Location = new System.Drawing.Point(360, 450);
            this.lbl15_12.Name = "lbl15_12";
            this.lbl15_12.Size = new System.Drawing.Size(31, 31);
            this.lbl15_12.TabIndex = 219;
            this.lbl15_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl15_12.Click += new System.EventHandler(this.map_click);
            // 
            // lbl15_11
            // 
            this.lbl15_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15_11.Image = ((System.Drawing.Image)(resources.GetObject("lbl15_11.Image")));
            this.lbl15_11.Location = new System.Drawing.Point(330, 450);
            this.lbl15_11.Name = "lbl15_11";
            this.lbl15_11.Size = new System.Drawing.Size(31, 31);
            this.lbl15_11.TabIndex = 218;
            this.lbl15_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl15_11.Click += new System.EventHandler(this.map_click);
            // 
            // lbl15_10
            // 
            this.lbl15_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15_10.Image = ((System.Drawing.Image)(resources.GetObject("lbl15_10.Image")));
            this.lbl15_10.Location = new System.Drawing.Point(300, 450);
            this.lbl15_10.Name = "lbl15_10";
            this.lbl15_10.Size = new System.Drawing.Size(31, 31);
            this.lbl15_10.TabIndex = 217;
            this.lbl15_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl15_10.Click += new System.EventHandler(this.map_click);
            // 
            // lbl15_9
            // 
            this.lbl15_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15_9.Image = ((System.Drawing.Image)(resources.GetObject("lbl15_9.Image")));
            this.lbl15_9.Location = new System.Drawing.Point(270, 450);
            this.lbl15_9.Name = "lbl15_9";
            this.lbl15_9.Size = new System.Drawing.Size(31, 31);
            this.lbl15_9.TabIndex = 216;
            this.lbl15_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl15_9.Click += new System.EventHandler(this.map_click);
            // 
            // lbl15_8
            // 
            this.lbl15_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15_8.Image = ((System.Drawing.Image)(resources.GetObject("lbl15_8.Image")));
            this.lbl15_8.Location = new System.Drawing.Point(240, 450);
            this.lbl15_8.Name = "lbl15_8";
            this.lbl15_8.Size = new System.Drawing.Size(31, 31);
            this.lbl15_8.TabIndex = 215;
            this.lbl15_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl15_8.Click += new System.EventHandler(this.map_click);
            // 
            // lbl15_7
            // 
            this.lbl15_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15_7.Image = ((System.Drawing.Image)(resources.GetObject("lbl15_7.Image")));
            this.lbl15_7.Location = new System.Drawing.Point(210, 450);
            this.lbl15_7.Name = "lbl15_7";
            this.lbl15_7.Size = new System.Drawing.Size(31, 31);
            this.lbl15_7.TabIndex = 214;
            this.lbl15_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl15_7.Click += new System.EventHandler(this.map_click);
            // 
            // lbl15_6
            // 
            this.lbl15_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15_6.Image = ((System.Drawing.Image)(resources.GetObject("lbl15_6.Image")));
            this.lbl15_6.Location = new System.Drawing.Point(180, 450);
            this.lbl15_6.Name = "lbl15_6";
            this.lbl15_6.Size = new System.Drawing.Size(31, 31);
            this.lbl15_6.TabIndex = 213;
            this.lbl15_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl15_6.Click += new System.EventHandler(this.map_click);
            // 
            // lbl15_5
            // 
            this.lbl15_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15_5.Image = ((System.Drawing.Image)(resources.GetObject("lbl15_5.Image")));
            this.lbl15_5.Location = new System.Drawing.Point(150, 450);
            this.lbl15_5.Name = "lbl15_5";
            this.lbl15_5.Size = new System.Drawing.Size(31, 31);
            this.lbl15_5.TabIndex = 212;
            this.lbl15_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl15_5.Click += new System.EventHandler(this.map_click);
            // 
            // lbl15_4
            // 
            this.lbl15_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15_4.Image = ((System.Drawing.Image)(resources.GetObject("lbl15_4.Image")));
            this.lbl15_4.Location = new System.Drawing.Point(120, 450);
            this.lbl15_4.Name = "lbl15_4";
            this.lbl15_4.Size = new System.Drawing.Size(31, 31);
            this.lbl15_4.TabIndex = 211;
            this.lbl15_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl15_4.Click += new System.EventHandler(this.map_click);
            // 
            // lbl15_3
            // 
            this.lbl15_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15_3.Image = ((System.Drawing.Image)(resources.GetObject("lbl15_3.Image")));
            this.lbl15_3.Location = new System.Drawing.Point(90, 450);
            this.lbl15_3.Name = "lbl15_3";
            this.lbl15_3.Size = new System.Drawing.Size(31, 31);
            this.lbl15_3.TabIndex = 210;
            this.lbl15_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl15_3.Click += new System.EventHandler(this.map_click);
            // 
            // lbl15_2
            // 
            this.lbl15_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15_2.Image = ((System.Drawing.Image)(resources.GetObject("lbl15_2.Image")));
            this.lbl15_2.Location = new System.Drawing.Point(60, 450);
            this.lbl15_2.Name = "lbl15_2";
            this.lbl15_2.Size = new System.Drawing.Size(31, 31);
            this.lbl15_2.TabIndex = 209;
            this.lbl15_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl15_2.Click += new System.EventHandler(this.map_click);
            // 
            // lbl15_1
            // 
            this.lbl15_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15_1.Image = ((System.Drawing.Image)(resources.GetObject("lbl15_1.Image")));
            this.lbl15_1.Location = new System.Drawing.Point(30, 450);
            this.lbl15_1.Name = "lbl15_1";
            this.lbl15_1.Size = new System.Drawing.Size(31, 31);
            this.lbl15_1.TabIndex = 208;
            this.lbl15_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl15_1.Click += new System.EventHandler(this.map_click);
            // 
            // lbl14_16
            // 
            this.lbl14_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14_16.Image = ((System.Drawing.Image)(resources.GetObject("lbl14_16.Image")));
            this.lbl14_16.Location = new System.Drawing.Point(480, 420);
            this.lbl14_16.Name = "lbl14_16";
            this.lbl14_16.Size = new System.Drawing.Size(31, 31);
            this.lbl14_16.TabIndex = 207;
            this.lbl14_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl14_16.Click += new System.EventHandler(this.map_click);
            // 
            // lbl14_15
            // 
            this.lbl14_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14_15.Image = ((System.Drawing.Image)(resources.GetObject("lbl14_15.Image")));
            this.lbl14_15.Location = new System.Drawing.Point(450, 420);
            this.lbl14_15.Name = "lbl14_15";
            this.lbl14_15.Size = new System.Drawing.Size(31, 31);
            this.lbl14_15.TabIndex = 206;
            this.lbl14_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl14_15.Click += new System.EventHandler(this.map_click);
            // 
            // lbl14_14
            // 
            this.lbl14_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14_14.Image = ((System.Drawing.Image)(resources.GetObject("lbl14_14.Image")));
            this.lbl14_14.Location = new System.Drawing.Point(420, 420);
            this.lbl14_14.Name = "lbl14_14";
            this.lbl14_14.Size = new System.Drawing.Size(31, 31);
            this.lbl14_14.TabIndex = 205;
            this.lbl14_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl14_14.Click += new System.EventHandler(this.map_click);
            // 
            // lbl14_13
            // 
            this.lbl14_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14_13.Image = ((System.Drawing.Image)(resources.GetObject("lbl14_13.Image")));
            this.lbl14_13.Location = new System.Drawing.Point(390, 420);
            this.lbl14_13.Name = "lbl14_13";
            this.lbl14_13.Size = new System.Drawing.Size(31, 31);
            this.lbl14_13.TabIndex = 204;
            this.lbl14_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl14_13.Click += new System.EventHandler(this.map_click);
            // 
            // lbl14_12
            // 
            this.lbl14_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14_12.Image = ((System.Drawing.Image)(resources.GetObject("lbl14_12.Image")));
            this.lbl14_12.Location = new System.Drawing.Point(360, 420);
            this.lbl14_12.Name = "lbl14_12";
            this.lbl14_12.Size = new System.Drawing.Size(31, 31);
            this.lbl14_12.TabIndex = 203;
            this.lbl14_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl14_12.Click += new System.EventHandler(this.map_click);
            // 
            // lbl14_11
            // 
            this.lbl14_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14_11.Image = ((System.Drawing.Image)(resources.GetObject("lbl14_11.Image")));
            this.lbl14_11.Location = new System.Drawing.Point(330, 420);
            this.lbl14_11.Name = "lbl14_11";
            this.lbl14_11.Size = new System.Drawing.Size(31, 31);
            this.lbl14_11.TabIndex = 202;
            this.lbl14_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl14_11.Click += new System.EventHandler(this.map_click);
            // 
            // lbl14_10
            // 
            this.lbl14_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14_10.Image = ((System.Drawing.Image)(resources.GetObject("lbl14_10.Image")));
            this.lbl14_10.Location = new System.Drawing.Point(300, 420);
            this.lbl14_10.Name = "lbl14_10";
            this.lbl14_10.Size = new System.Drawing.Size(31, 31);
            this.lbl14_10.TabIndex = 201;
            this.lbl14_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl14_10.Click += new System.EventHandler(this.map_click);
            // 
            // lbl14_9
            // 
            this.lbl14_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14_9.Image = ((System.Drawing.Image)(resources.GetObject("lbl14_9.Image")));
            this.lbl14_9.Location = new System.Drawing.Point(270, 420);
            this.lbl14_9.Name = "lbl14_9";
            this.lbl14_9.Size = new System.Drawing.Size(31, 31);
            this.lbl14_9.TabIndex = 200;
            this.lbl14_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl14_9.Click += new System.EventHandler(this.map_click);
            // 
            // lbl14_8
            // 
            this.lbl14_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14_8.Image = ((System.Drawing.Image)(resources.GetObject("lbl14_8.Image")));
            this.lbl14_8.Location = new System.Drawing.Point(240, 420);
            this.lbl14_8.Name = "lbl14_8";
            this.lbl14_8.Size = new System.Drawing.Size(31, 31);
            this.lbl14_8.TabIndex = 199;
            this.lbl14_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl14_8.Click += new System.EventHandler(this.map_click);
            // 
            // lbl14_7
            // 
            this.lbl14_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14_7.Image = ((System.Drawing.Image)(resources.GetObject("lbl14_7.Image")));
            this.lbl14_7.Location = new System.Drawing.Point(210, 420);
            this.lbl14_7.Name = "lbl14_7";
            this.lbl14_7.Size = new System.Drawing.Size(31, 31);
            this.lbl14_7.TabIndex = 198;
            this.lbl14_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl14_7.Click += new System.EventHandler(this.map_click);
            // 
            // lbl14_6
            // 
            this.lbl14_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14_6.Image = ((System.Drawing.Image)(resources.GetObject("lbl14_6.Image")));
            this.lbl14_6.Location = new System.Drawing.Point(180, 420);
            this.lbl14_6.Name = "lbl14_6";
            this.lbl14_6.Size = new System.Drawing.Size(31, 31);
            this.lbl14_6.TabIndex = 197;
            this.lbl14_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl14_6.Click += new System.EventHandler(this.map_click);
            // 
            // lbl14_5
            // 
            this.lbl14_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14_5.Image = ((System.Drawing.Image)(resources.GetObject("lbl14_5.Image")));
            this.lbl14_5.Location = new System.Drawing.Point(150, 420);
            this.lbl14_5.Name = "lbl14_5";
            this.lbl14_5.Size = new System.Drawing.Size(31, 31);
            this.lbl14_5.TabIndex = 196;
            this.lbl14_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl14_5.Click += new System.EventHandler(this.map_click);
            // 
            // lbl14_4
            // 
            this.lbl14_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14_4.Image = ((System.Drawing.Image)(resources.GetObject("lbl14_4.Image")));
            this.lbl14_4.Location = new System.Drawing.Point(120, 420);
            this.lbl14_4.Name = "lbl14_4";
            this.lbl14_4.Size = new System.Drawing.Size(31, 31);
            this.lbl14_4.TabIndex = 195;
            this.lbl14_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl14_4.Click += new System.EventHandler(this.map_click);
            // 
            // lbl14_3
            // 
            this.lbl14_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14_3.Image = ((System.Drawing.Image)(resources.GetObject("lbl14_3.Image")));
            this.lbl14_3.Location = new System.Drawing.Point(90, 420);
            this.lbl14_3.Name = "lbl14_3";
            this.lbl14_3.Size = new System.Drawing.Size(31, 31);
            this.lbl14_3.TabIndex = 194;
            this.lbl14_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl14_3.Click += new System.EventHandler(this.map_click);
            // 
            // lbl14_2
            // 
            this.lbl14_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14_2.Image = ((System.Drawing.Image)(resources.GetObject("lbl14_2.Image")));
            this.lbl14_2.Location = new System.Drawing.Point(60, 420);
            this.lbl14_2.Name = "lbl14_2";
            this.lbl14_2.Size = new System.Drawing.Size(31, 31);
            this.lbl14_2.TabIndex = 193;
            this.lbl14_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl14_2.Click += new System.EventHandler(this.map_click);
            // 
            // lbl14_1
            // 
            this.lbl14_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14_1.Image = ((System.Drawing.Image)(resources.GetObject("lbl14_1.Image")));
            this.lbl14_1.Location = new System.Drawing.Point(30, 420);
            this.lbl14_1.Name = "lbl14_1";
            this.lbl14_1.Size = new System.Drawing.Size(31, 31);
            this.lbl14_1.TabIndex = 192;
            this.lbl14_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl14_1.Click += new System.EventHandler(this.map_click);
            // 
            // lbl13_16
            // 
            this.lbl13_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13_16.Image = ((System.Drawing.Image)(resources.GetObject("lbl13_16.Image")));
            this.lbl13_16.Location = new System.Drawing.Point(480, 390);
            this.lbl13_16.Name = "lbl13_16";
            this.lbl13_16.Size = new System.Drawing.Size(31, 31);
            this.lbl13_16.TabIndex = 191;
            this.lbl13_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl13_16.Click += new System.EventHandler(this.map_click);
            // 
            // lbl13_15
            // 
            this.lbl13_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13_15.Image = ((System.Drawing.Image)(resources.GetObject("lbl13_15.Image")));
            this.lbl13_15.Location = new System.Drawing.Point(450, 390);
            this.lbl13_15.Name = "lbl13_15";
            this.lbl13_15.Size = new System.Drawing.Size(31, 31);
            this.lbl13_15.TabIndex = 190;
            this.lbl13_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl13_15.Click += new System.EventHandler(this.map_click);
            // 
            // lbl13_14
            // 
            this.lbl13_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13_14.Image = ((System.Drawing.Image)(resources.GetObject("lbl13_14.Image")));
            this.lbl13_14.Location = new System.Drawing.Point(420, 390);
            this.lbl13_14.Name = "lbl13_14";
            this.lbl13_14.Size = new System.Drawing.Size(31, 31);
            this.lbl13_14.TabIndex = 189;
            this.lbl13_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl13_14.Click += new System.EventHandler(this.map_click);
            // 
            // lbl13_13
            // 
            this.lbl13_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13_13.Image = ((System.Drawing.Image)(resources.GetObject("lbl13_13.Image")));
            this.lbl13_13.Location = new System.Drawing.Point(390, 390);
            this.lbl13_13.Name = "lbl13_13";
            this.lbl13_13.Size = new System.Drawing.Size(31, 31);
            this.lbl13_13.TabIndex = 188;
            this.lbl13_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl13_13.Click += new System.EventHandler(this.map_click);
            // 
            // lbl13_12
            // 
            this.lbl13_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13_12.Image = ((System.Drawing.Image)(resources.GetObject("lbl13_12.Image")));
            this.lbl13_12.Location = new System.Drawing.Point(360, 390);
            this.lbl13_12.Name = "lbl13_12";
            this.lbl13_12.Size = new System.Drawing.Size(31, 31);
            this.lbl13_12.TabIndex = 187;
            this.lbl13_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl13_12.Click += new System.EventHandler(this.map_click);
            // 
            // lbl13_11
            // 
            this.lbl13_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13_11.Image = ((System.Drawing.Image)(resources.GetObject("lbl13_11.Image")));
            this.lbl13_11.Location = new System.Drawing.Point(330, 390);
            this.lbl13_11.Name = "lbl13_11";
            this.lbl13_11.Size = new System.Drawing.Size(31, 31);
            this.lbl13_11.TabIndex = 186;
            this.lbl13_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl13_11.Click += new System.EventHandler(this.map_click);
            // 
            // lbl13_10
            // 
            this.lbl13_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13_10.Image = ((System.Drawing.Image)(resources.GetObject("lbl13_10.Image")));
            this.lbl13_10.Location = new System.Drawing.Point(300, 390);
            this.lbl13_10.Name = "lbl13_10";
            this.lbl13_10.Size = new System.Drawing.Size(31, 31);
            this.lbl13_10.TabIndex = 185;
            this.lbl13_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl13_10.Click += new System.EventHandler(this.map_click);
            // 
            // lbl13_9
            // 
            this.lbl13_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13_9.Image = ((System.Drawing.Image)(resources.GetObject("lbl13_9.Image")));
            this.lbl13_9.Location = new System.Drawing.Point(270, 390);
            this.lbl13_9.Name = "lbl13_9";
            this.lbl13_9.Size = new System.Drawing.Size(31, 31);
            this.lbl13_9.TabIndex = 184;
            this.lbl13_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl13_9.Click += new System.EventHandler(this.map_click);
            // 
            // lbl13_8
            // 
            this.lbl13_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13_8.Image = ((System.Drawing.Image)(resources.GetObject("lbl13_8.Image")));
            this.lbl13_8.Location = new System.Drawing.Point(240, 390);
            this.lbl13_8.Name = "lbl13_8";
            this.lbl13_8.Size = new System.Drawing.Size(31, 31);
            this.lbl13_8.TabIndex = 183;
            this.lbl13_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl13_8.Click += new System.EventHandler(this.map_click);
            // 
            // lbl13_7
            // 
            this.lbl13_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13_7.Image = ((System.Drawing.Image)(resources.GetObject("lbl13_7.Image")));
            this.lbl13_7.Location = new System.Drawing.Point(210, 390);
            this.lbl13_7.Name = "lbl13_7";
            this.lbl13_7.Size = new System.Drawing.Size(31, 31);
            this.lbl13_7.TabIndex = 182;
            this.lbl13_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl13_7.Click += new System.EventHandler(this.map_click);
            // 
            // lbl13_6
            // 
            this.lbl13_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13_6.Image = ((System.Drawing.Image)(resources.GetObject("lbl13_6.Image")));
            this.lbl13_6.Location = new System.Drawing.Point(180, 390);
            this.lbl13_6.Name = "lbl13_6";
            this.lbl13_6.Size = new System.Drawing.Size(31, 31);
            this.lbl13_6.TabIndex = 181;
            this.lbl13_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl13_6.Click += new System.EventHandler(this.map_click);
            // 
            // lbl13_5
            // 
            this.lbl13_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13_5.Image = ((System.Drawing.Image)(resources.GetObject("lbl13_5.Image")));
            this.lbl13_5.Location = new System.Drawing.Point(150, 390);
            this.lbl13_5.Name = "lbl13_5";
            this.lbl13_5.Size = new System.Drawing.Size(31, 31);
            this.lbl13_5.TabIndex = 180;
            this.lbl13_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl13_5.Click += new System.EventHandler(this.map_click);
            // 
            // lbl13_4
            // 
            this.lbl13_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13_4.Image = ((System.Drawing.Image)(resources.GetObject("lbl13_4.Image")));
            this.lbl13_4.Location = new System.Drawing.Point(120, 390);
            this.lbl13_4.Name = "lbl13_4";
            this.lbl13_4.Size = new System.Drawing.Size(31, 31);
            this.lbl13_4.TabIndex = 179;
            this.lbl13_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl13_4.Click += new System.EventHandler(this.map_click);
            // 
            // lbl13_3
            // 
            this.lbl13_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13_3.Image = ((System.Drawing.Image)(resources.GetObject("lbl13_3.Image")));
            this.lbl13_3.Location = new System.Drawing.Point(90, 390);
            this.lbl13_3.Name = "lbl13_3";
            this.lbl13_3.Size = new System.Drawing.Size(31, 31);
            this.lbl13_3.TabIndex = 178;
            this.lbl13_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl13_3.Click += new System.EventHandler(this.map_click);
            // 
            // lbl13_2
            // 
            this.lbl13_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13_2.Image = ((System.Drawing.Image)(resources.GetObject("lbl13_2.Image")));
            this.lbl13_2.Location = new System.Drawing.Point(60, 390);
            this.lbl13_2.Name = "lbl13_2";
            this.lbl13_2.Size = new System.Drawing.Size(31, 31);
            this.lbl13_2.TabIndex = 177;
            this.lbl13_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl13_2.Click += new System.EventHandler(this.map_click);
            // 
            // lbl13_1
            // 
            this.lbl13_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13_1.Image = ((System.Drawing.Image)(resources.GetObject("lbl13_1.Image")));
            this.lbl13_1.Location = new System.Drawing.Point(30, 390);
            this.lbl13_1.Name = "lbl13_1";
            this.lbl13_1.Size = new System.Drawing.Size(31, 31);
            this.lbl13_1.TabIndex = 176;
            this.lbl13_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl13_1.Click += new System.EventHandler(this.map_click);
            // 
            // lbl12_16
            // 
            this.lbl12_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12_16.Image = ((System.Drawing.Image)(resources.GetObject("lbl12_16.Image")));
            this.lbl12_16.Location = new System.Drawing.Point(480, 360);
            this.lbl12_16.Name = "lbl12_16";
            this.lbl12_16.Size = new System.Drawing.Size(31, 31);
            this.lbl12_16.TabIndex = 175;
            this.lbl12_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl12_16.Click += new System.EventHandler(this.map_click);
            // 
            // lbl12_15
            // 
            this.lbl12_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12_15.Image = ((System.Drawing.Image)(resources.GetObject("lbl12_15.Image")));
            this.lbl12_15.Location = new System.Drawing.Point(450, 360);
            this.lbl12_15.Name = "lbl12_15";
            this.lbl12_15.Size = new System.Drawing.Size(31, 31);
            this.lbl12_15.TabIndex = 174;
            this.lbl12_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl12_15.Click += new System.EventHandler(this.map_click);
            // 
            // lbl12_14
            // 
            this.lbl12_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12_14.Image = ((System.Drawing.Image)(resources.GetObject("lbl12_14.Image")));
            this.lbl12_14.Location = new System.Drawing.Point(420, 360);
            this.lbl12_14.Name = "lbl12_14";
            this.lbl12_14.Size = new System.Drawing.Size(31, 31);
            this.lbl12_14.TabIndex = 173;
            this.lbl12_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl12_14.Click += new System.EventHandler(this.map_click);
            // 
            // lbl12_13
            // 
            this.lbl12_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12_13.Image = ((System.Drawing.Image)(resources.GetObject("lbl12_13.Image")));
            this.lbl12_13.Location = new System.Drawing.Point(390, 360);
            this.lbl12_13.Name = "lbl12_13";
            this.lbl12_13.Size = new System.Drawing.Size(31, 31);
            this.lbl12_13.TabIndex = 172;
            this.lbl12_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl12_13.Click += new System.EventHandler(this.map_click);
            // 
            // lbl12_12
            // 
            this.lbl12_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12_12.Image = ((System.Drawing.Image)(resources.GetObject("lbl12_12.Image")));
            this.lbl12_12.Location = new System.Drawing.Point(360, 360);
            this.lbl12_12.Name = "lbl12_12";
            this.lbl12_12.Size = new System.Drawing.Size(31, 31);
            this.lbl12_12.TabIndex = 171;
            this.lbl12_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl12_12.Click += new System.EventHandler(this.map_click);
            // 
            // lbl12_11
            // 
            this.lbl12_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12_11.Image = ((System.Drawing.Image)(resources.GetObject("lbl12_11.Image")));
            this.lbl12_11.Location = new System.Drawing.Point(330, 360);
            this.lbl12_11.Name = "lbl12_11";
            this.lbl12_11.Size = new System.Drawing.Size(31, 31);
            this.lbl12_11.TabIndex = 170;
            this.lbl12_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl12_11.Click += new System.EventHandler(this.map_click);
            // 
            // lbl12_10
            // 
            this.lbl12_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12_10.Image = ((System.Drawing.Image)(resources.GetObject("lbl12_10.Image")));
            this.lbl12_10.Location = new System.Drawing.Point(300, 360);
            this.lbl12_10.Name = "lbl12_10";
            this.lbl12_10.Size = new System.Drawing.Size(31, 31);
            this.lbl12_10.TabIndex = 169;
            this.lbl12_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl12_10.Click += new System.EventHandler(this.map_click);
            // 
            // lbl12_9
            // 
            this.lbl12_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12_9.Image = ((System.Drawing.Image)(resources.GetObject("lbl12_9.Image")));
            this.lbl12_9.Location = new System.Drawing.Point(270, 360);
            this.lbl12_9.Name = "lbl12_9";
            this.lbl12_9.Size = new System.Drawing.Size(31, 31);
            this.lbl12_9.TabIndex = 168;
            this.lbl12_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl12_9.Click += new System.EventHandler(this.map_click);
            // 
            // lbl12_8
            // 
            this.lbl12_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12_8.Image = ((System.Drawing.Image)(resources.GetObject("lbl12_8.Image")));
            this.lbl12_8.Location = new System.Drawing.Point(240, 360);
            this.lbl12_8.Name = "lbl12_8";
            this.lbl12_8.Size = new System.Drawing.Size(31, 31);
            this.lbl12_8.TabIndex = 167;
            this.lbl12_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl12_8.Click += new System.EventHandler(this.map_click);
            // 
            // lbl12_7
            // 
            this.lbl12_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12_7.Image = ((System.Drawing.Image)(resources.GetObject("lbl12_7.Image")));
            this.lbl12_7.Location = new System.Drawing.Point(210, 360);
            this.lbl12_7.Name = "lbl12_7";
            this.lbl12_7.Size = new System.Drawing.Size(31, 31);
            this.lbl12_7.TabIndex = 166;
            this.lbl12_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl12_7.Click += new System.EventHandler(this.map_click);
            // 
            // lbl12_6
            // 
            this.lbl12_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12_6.Image = ((System.Drawing.Image)(resources.GetObject("lbl12_6.Image")));
            this.lbl12_6.Location = new System.Drawing.Point(180, 360);
            this.lbl12_6.Name = "lbl12_6";
            this.lbl12_6.Size = new System.Drawing.Size(31, 31);
            this.lbl12_6.TabIndex = 165;
            this.lbl12_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl12_6.Click += new System.EventHandler(this.map_click);
            // 
            // lbl12_5
            // 
            this.lbl12_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12_5.Image = ((System.Drawing.Image)(resources.GetObject("lbl12_5.Image")));
            this.lbl12_5.Location = new System.Drawing.Point(150, 360);
            this.lbl12_5.Name = "lbl12_5";
            this.lbl12_5.Size = new System.Drawing.Size(31, 31);
            this.lbl12_5.TabIndex = 164;
            this.lbl12_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl12_5.Click += new System.EventHandler(this.map_click);
            // 
            // lbl12_4
            // 
            this.lbl12_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12_4.Image = ((System.Drawing.Image)(resources.GetObject("lbl12_4.Image")));
            this.lbl12_4.Location = new System.Drawing.Point(120, 360);
            this.lbl12_4.Name = "lbl12_4";
            this.lbl12_4.Size = new System.Drawing.Size(31, 31);
            this.lbl12_4.TabIndex = 163;
            this.lbl12_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl12_4.Click += new System.EventHandler(this.map_click);
            // 
            // lbl12_3
            // 
            this.lbl12_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12_3.Image = ((System.Drawing.Image)(resources.GetObject("lbl12_3.Image")));
            this.lbl12_3.Location = new System.Drawing.Point(90, 360);
            this.lbl12_3.Name = "lbl12_3";
            this.lbl12_3.Size = new System.Drawing.Size(31, 31);
            this.lbl12_3.TabIndex = 162;
            this.lbl12_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl12_3.Click += new System.EventHandler(this.map_click);
            // 
            // lbl12_2
            // 
            this.lbl12_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12_2.Image = ((System.Drawing.Image)(resources.GetObject("lbl12_2.Image")));
            this.lbl12_2.Location = new System.Drawing.Point(60, 360);
            this.lbl12_2.Name = "lbl12_2";
            this.lbl12_2.Size = new System.Drawing.Size(31, 31);
            this.lbl12_2.TabIndex = 161;
            this.lbl12_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl12_2.Click += new System.EventHandler(this.map_click);
            // 
            // lbl12_1
            // 
            this.lbl12_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12_1.Image = ((System.Drawing.Image)(resources.GetObject("lbl12_1.Image")));
            this.lbl12_1.Location = new System.Drawing.Point(30, 360);
            this.lbl12_1.Name = "lbl12_1";
            this.lbl12_1.Size = new System.Drawing.Size(31, 31);
            this.lbl12_1.TabIndex = 160;
            this.lbl12_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl12_1.Click += new System.EventHandler(this.map_click);
            // 
            // lbl11_16
            // 
            this.lbl11_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11_16.Image = ((System.Drawing.Image)(resources.GetObject("lbl11_16.Image")));
            this.lbl11_16.Location = new System.Drawing.Point(480, 330);
            this.lbl11_16.Name = "lbl11_16";
            this.lbl11_16.Size = new System.Drawing.Size(31, 31);
            this.lbl11_16.TabIndex = 159;
            this.lbl11_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl11_16.Click += new System.EventHandler(this.map_click);
            // 
            // lbl11_15
            // 
            this.lbl11_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11_15.Image = ((System.Drawing.Image)(resources.GetObject("lbl11_15.Image")));
            this.lbl11_15.Location = new System.Drawing.Point(450, 330);
            this.lbl11_15.Name = "lbl11_15";
            this.lbl11_15.Size = new System.Drawing.Size(31, 31);
            this.lbl11_15.TabIndex = 158;
            this.lbl11_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl11_15.Click += new System.EventHandler(this.map_click);
            // 
            // lbl11_14
            // 
            this.lbl11_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11_14.Image = ((System.Drawing.Image)(resources.GetObject("lbl11_14.Image")));
            this.lbl11_14.Location = new System.Drawing.Point(420, 330);
            this.lbl11_14.Name = "lbl11_14";
            this.lbl11_14.Size = new System.Drawing.Size(31, 31);
            this.lbl11_14.TabIndex = 157;
            this.lbl11_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl11_14.Click += new System.EventHandler(this.map_click);
            // 
            // lbl11_13
            // 
            this.lbl11_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11_13.Image = ((System.Drawing.Image)(resources.GetObject("lbl11_13.Image")));
            this.lbl11_13.Location = new System.Drawing.Point(390, 330);
            this.lbl11_13.Name = "lbl11_13";
            this.lbl11_13.Size = new System.Drawing.Size(31, 31);
            this.lbl11_13.TabIndex = 156;
            this.lbl11_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl11_13.Click += new System.EventHandler(this.map_click);
            // 
            // lbl11_12
            // 
            this.lbl11_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11_12.Image = ((System.Drawing.Image)(resources.GetObject("lbl11_12.Image")));
            this.lbl11_12.Location = new System.Drawing.Point(360, 330);
            this.lbl11_12.Name = "lbl11_12";
            this.lbl11_12.Size = new System.Drawing.Size(31, 31);
            this.lbl11_12.TabIndex = 155;
            this.lbl11_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl11_12.Click += new System.EventHandler(this.map_click);
            // 
            // lbl11_11
            // 
            this.lbl11_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11_11.Image = ((System.Drawing.Image)(resources.GetObject("lbl11_11.Image")));
            this.lbl11_11.Location = new System.Drawing.Point(330, 330);
            this.lbl11_11.Name = "lbl11_11";
            this.lbl11_11.Size = new System.Drawing.Size(31, 31);
            this.lbl11_11.TabIndex = 154;
            this.lbl11_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl11_11.Click += new System.EventHandler(this.map_click);
            // 
            // lbl11_10
            // 
            this.lbl11_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11_10.Image = ((System.Drawing.Image)(resources.GetObject("lbl11_10.Image")));
            this.lbl11_10.Location = new System.Drawing.Point(300, 330);
            this.lbl11_10.Name = "lbl11_10";
            this.lbl11_10.Size = new System.Drawing.Size(31, 31);
            this.lbl11_10.TabIndex = 153;
            this.lbl11_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl11_10.Click += new System.EventHandler(this.map_click);
            // 
            // lbl11_9
            // 
            this.lbl11_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11_9.Image = ((System.Drawing.Image)(resources.GetObject("lbl11_9.Image")));
            this.lbl11_9.Location = new System.Drawing.Point(270, 330);
            this.lbl11_9.Name = "lbl11_9";
            this.lbl11_9.Size = new System.Drawing.Size(31, 31);
            this.lbl11_9.TabIndex = 152;
            this.lbl11_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl11_9.Click += new System.EventHandler(this.map_click);
            // 
            // lbl11_8
            // 
            this.lbl11_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11_8.Image = ((System.Drawing.Image)(resources.GetObject("lbl11_8.Image")));
            this.lbl11_8.Location = new System.Drawing.Point(240, 330);
            this.lbl11_8.Name = "lbl11_8";
            this.lbl11_8.Size = new System.Drawing.Size(31, 31);
            this.lbl11_8.TabIndex = 151;
            this.lbl11_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl11_8.Click += new System.EventHandler(this.map_click);
            // 
            // lbl11_7
            // 
            this.lbl11_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11_7.Image = ((System.Drawing.Image)(resources.GetObject("lbl11_7.Image")));
            this.lbl11_7.Location = new System.Drawing.Point(210, 330);
            this.lbl11_7.Name = "lbl11_7";
            this.lbl11_7.Size = new System.Drawing.Size(31, 31);
            this.lbl11_7.TabIndex = 150;
            this.lbl11_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl11_7.Click += new System.EventHandler(this.map_click);
            // 
            // lbl11_6
            // 
            this.lbl11_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11_6.Image = ((System.Drawing.Image)(resources.GetObject("lbl11_6.Image")));
            this.lbl11_6.Location = new System.Drawing.Point(180, 330);
            this.lbl11_6.Name = "lbl11_6";
            this.lbl11_6.Size = new System.Drawing.Size(31, 31);
            this.lbl11_6.TabIndex = 149;
            this.lbl11_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl11_6.Click += new System.EventHandler(this.map_click);
            // 
            // lbl11_5
            // 
            this.lbl11_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11_5.Image = ((System.Drawing.Image)(resources.GetObject("lbl11_5.Image")));
            this.lbl11_5.Location = new System.Drawing.Point(150, 330);
            this.lbl11_5.Name = "lbl11_5";
            this.lbl11_5.Size = new System.Drawing.Size(31, 31);
            this.lbl11_5.TabIndex = 148;
            this.lbl11_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl11_5.Click += new System.EventHandler(this.map_click);
            // 
            // lbl11_4
            // 
            this.lbl11_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11_4.Image = ((System.Drawing.Image)(resources.GetObject("lbl11_4.Image")));
            this.lbl11_4.Location = new System.Drawing.Point(120, 330);
            this.lbl11_4.Name = "lbl11_4";
            this.lbl11_4.Size = new System.Drawing.Size(31, 31);
            this.lbl11_4.TabIndex = 147;
            this.lbl11_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl11_4.Click += new System.EventHandler(this.map_click);
            // 
            // lbl11_3
            // 
            this.lbl11_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11_3.Image = ((System.Drawing.Image)(resources.GetObject("lbl11_3.Image")));
            this.lbl11_3.Location = new System.Drawing.Point(90, 330);
            this.lbl11_3.Name = "lbl11_3";
            this.lbl11_3.Size = new System.Drawing.Size(31, 31);
            this.lbl11_3.TabIndex = 146;
            this.lbl11_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl11_3.Click += new System.EventHandler(this.map_click);
            // 
            // lbl11_2
            // 
            this.lbl11_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11_2.Image = ((System.Drawing.Image)(resources.GetObject("lbl11_2.Image")));
            this.lbl11_2.Location = new System.Drawing.Point(60, 330);
            this.lbl11_2.Name = "lbl11_2";
            this.lbl11_2.Size = new System.Drawing.Size(31, 31);
            this.lbl11_2.TabIndex = 145;
            this.lbl11_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl11_2.Click += new System.EventHandler(this.map_click);
            // 
            // lbl11_1
            // 
            this.lbl11_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11_1.Image = ((System.Drawing.Image)(resources.GetObject("lbl11_1.Image")));
            this.lbl11_1.Location = new System.Drawing.Point(30, 330);
            this.lbl11_1.Name = "lbl11_1";
            this.lbl11_1.Size = new System.Drawing.Size(31, 31);
            this.lbl11_1.TabIndex = 144;
            this.lbl11_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl11_1.Click += new System.EventHandler(this.map_click);
            // 
            // lbl10_16
            // 
            this.lbl10_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10_16.Image = ((System.Drawing.Image)(resources.GetObject("lbl10_16.Image")));
            this.lbl10_16.Location = new System.Drawing.Point(480, 300);
            this.lbl10_16.Name = "lbl10_16";
            this.lbl10_16.Size = new System.Drawing.Size(31, 31);
            this.lbl10_16.TabIndex = 143;
            this.lbl10_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl10_16.Click += new System.EventHandler(this.map_click);
            // 
            // lbl10_15
            // 
            this.lbl10_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10_15.Image = ((System.Drawing.Image)(resources.GetObject("lbl10_15.Image")));
            this.lbl10_15.Location = new System.Drawing.Point(450, 300);
            this.lbl10_15.Name = "lbl10_15";
            this.lbl10_15.Size = new System.Drawing.Size(31, 31);
            this.lbl10_15.TabIndex = 142;
            this.lbl10_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl10_15.Click += new System.EventHandler(this.map_click);
            // 
            // lbl10_14
            // 
            this.lbl10_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10_14.Image = ((System.Drawing.Image)(resources.GetObject("lbl10_14.Image")));
            this.lbl10_14.Location = new System.Drawing.Point(420, 300);
            this.lbl10_14.Name = "lbl10_14";
            this.lbl10_14.Size = new System.Drawing.Size(31, 31);
            this.lbl10_14.TabIndex = 141;
            this.lbl10_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl10_14.Click += new System.EventHandler(this.map_click);
            // 
            // lbl10_13
            // 
            this.lbl10_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10_13.Image = ((System.Drawing.Image)(resources.GetObject("lbl10_13.Image")));
            this.lbl10_13.Location = new System.Drawing.Point(390, 300);
            this.lbl10_13.Name = "lbl10_13";
            this.lbl10_13.Size = new System.Drawing.Size(31, 31);
            this.lbl10_13.TabIndex = 140;
            this.lbl10_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl10_13.Click += new System.EventHandler(this.map_click);
            // 
            // lbl10_12
            // 
            this.lbl10_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10_12.Image = ((System.Drawing.Image)(resources.GetObject("lbl10_12.Image")));
            this.lbl10_12.Location = new System.Drawing.Point(360, 300);
            this.lbl10_12.Name = "lbl10_12";
            this.lbl10_12.Size = new System.Drawing.Size(31, 31);
            this.lbl10_12.TabIndex = 139;
            this.lbl10_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl10_12.Click += new System.EventHandler(this.map_click);
            // 
            // lbl10_11
            // 
            this.lbl10_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10_11.Image = ((System.Drawing.Image)(resources.GetObject("lbl10_11.Image")));
            this.lbl10_11.Location = new System.Drawing.Point(330, 300);
            this.lbl10_11.Name = "lbl10_11";
            this.lbl10_11.Size = new System.Drawing.Size(31, 31);
            this.lbl10_11.TabIndex = 138;
            this.lbl10_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl10_11.Click += new System.EventHandler(this.map_click);
            // 
            // lbl10_10
            // 
            this.lbl10_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10_10.Image = ((System.Drawing.Image)(resources.GetObject("lbl10_10.Image")));
            this.lbl10_10.Location = new System.Drawing.Point(300, 300);
            this.lbl10_10.Name = "lbl10_10";
            this.lbl10_10.Size = new System.Drawing.Size(31, 31);
            this.lbl10_10.TabIndex = 137;
            this.lbl10_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl10_10.Click += new System.EventHandler(this.map_click);
            // 
            // lbl10_9
            // 
            this.lbl10_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10_9.Image = ((System.Drawing.Image)(resources.GetObject("lbl10_9.Image")));
            this.lbl10_9.Location = new System.Drawing.Point(270, 300);
            this.lbl10_9.Name = "lbl10_9";
            this.lbl10_9.Size = new System.Drawing.Size(31, 31);
            this.lbl10_9.TabIndex = 136;
            this.lbl10_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl10_9.Click += new System.EventHandler(this.map_click);
            // 
            // lbl10_8
            // 
            this.lbl10_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10_8.Image = ((System.Drawing.Image)(resources.GetObject("lbl10_8.Image")));
            this.lbl10_8.Location = new System.Drawing.Point(240, 300);
            this.lbl10_8.Name = "lbl10_8";
            this.lbl10_8.Size = new System.Drawing.Size(31, 31);
            this.lbl10_8.TabIndex = 135;
            this.lbl10_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl10_8.Click += new System.EventHandler(this.map_click);
            // 
            // lbl10_7
            // 
            this.lbl10_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10_7.Image = ((System.Drawing.Image)(resources.GetObject("lbl10_7.Image")));
            this.lbl10_7.Location = new System.Drawing.Point(210, 300);
            this.lbl10_7.Name = "lbl10_7";
            this.lbl10_7.Size = new System.Drawing.Size(31, 31);
            this.lbl10_7.TabIndex = 134;
            this.lbl10_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl10_7.Click += new System.EventHandler(this.map_click);
            // 
            // lbl10_6
            // 
            this.lbl10_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10_6.Image = ((System.Drawing.Image)(resources.GetObject("lbl10_6.Image")));
            this.lbl10_6.Location = new System.Drawing.Point(180, 300);
            this.lbl10_6.Name = "lbl10_6";
            this.lbl10_6.Size = new System.Drawing.Size(31, 31);
            this.lbl10_6.TabIndex = 133;
            this.lbl10_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl10_6.Click += new System.EventHandler(this.map_click);
            // 
            // lbl10_5
            // 
            this.lbl10_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10_5.Image = ((System.Drawing.Image)(resources.GetObject("lbl10_5.Image")));
            this.lbl10_5.Location = new System.Drawing.Point(150, 300);
            this.lbl10_5.Name = "lbl10_5";
            this.lbl10_5.Size = new System.Drawing.Size(31, 31);
            this.lbl10_5.TabIndex = 132;
            this.lbl10_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl10_5.Click += new System.EventHandler(this.map_click);
            // 
            // lbl10_4
            // 
            this.lbl10_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10_4.Image = ((System.Drawing.Image)(resources.GetObject("lbl10_4.Image")));
            this.lbl10_4.Location = new System.Drawing.Point(120, 300);
            this.lbl10_4.Name = "lbl10_4";
            this.lbl10_4.Size = new System.Drawing.Size(31, 31);
            this.lbl10_4.TabIndex = 131;
            this.lbl10_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl10_4.Click += new System.EventHandler(this.map_click);
            // 
            // lbl10_3
            // 
            this.lbl10_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10_3.Image = ((System.Drawing.Image)(resources.GetObject("lbl10_3.Image")));
            this.lbl10_3.Location = new System.Drawing.Point(90, 300);
            this.lbl10_3.Name = "lbl10_3";
            this.lbl10_3.Size = new System.Drawing.Size(31, 31);
            this.lbl10_3.TabIndex = 130;
            this.lbl10_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl10_3.Click += new System.EventHandler(this.map_click);
            // 
            // lbl10_2
            // 
            this.lbl10_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10_2.Image = ((System.Drawing.Image)(resources.GetObject("lbl10_2.Image")));
            this.lbl10_2.Location = new System.Drawing.Point(60, 300);
            this.lbl10_2.Name = "lbl10_2";
            this.lbl10_2.Size = new System.Drawing.Size(31, 31);
            this.lbl10_2.TabIndex = 129;
            this.lbl10_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl10_2.Click += new System.EventHandler(this.map_click);
            // 
            // lbl10_1
            // 
            this.lbl10_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10_1.Image = ((System.Drawing.Image)(resources.GetObject("lbl10_1.Image")));
            this.lbl10_1.Location = new System.Drawing.Point(30, 300);
            this.lbl10_1.Name = "lbl10_1";
            this.lbl10_1.Size = new System.Drawing.Size(31, 31);
            this.lbl10_1.TabIndex = 128;
            this.lbl10_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl10_1.Click += new System.EventHandler(this.map_click);
            // 
            // lbl9_16
            // 
            this.lbl9_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl9_16.Image = ((System.Drawing.Image)(resources.GetObject("lbl9_16.Image")));
            this.lbl9_16.Location = new System.Drawing.Point(480, 270);
            this.lbl9_16.Name = "lbl9_16";
            this.lbl9_16.Size = new System.Drawing.Size(31, 31);
            this.lbl9_16.TabIndex = 127;
            this.lbl9_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl9_16.Click += new System.EventHandler(this.map_click);
            // 
            // lbl9_15
            // 
            this.lbl9_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl9_15.Image = ((System.Drawing.Image)(resources.GetObject("lbl9_15.Image")));
            this.lbl9_15.Location = new System.Drawing.Point(450, 270);
            this.lbl9_15.Name = "lbl9_15";
            this.lbl9_15.Size = new System.Drawing.Size(31, 31);
            this.lbl9_15.TabIndex = 126;
            this.lbl9_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl9_15.Click += new System.EventHandler(this.map_click);
            // 
            // lbl9_14
            // 
            this.lbl9_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl9_14.Image = ((System.Drawing.Image)(resources.GetObject("lbl9_14.Image")));
            this.lbl9_14.Location = new System.Drawing.Point(420, 270);
            this.lbl9_14.Name = "lbl9_14";
            this.lbl9_14.Size = new System.Drawing.Size(31, 31);
            this.lbl9_14.TabIndex = 125;
            this.lbl9_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl9_14.Click += new System.EventHandler(this.map_click);
            // 
            // lbl9_13
            // 
            this.lbl9_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl9_13.Image = ((System.Drawing.Image)(resources.GetObject("lbl9_13.Image")));
            this.lbl9_13.Location = new System.Drawing.Point(390, 270);
            this.lbl9_13.Name = "lbl9_13";
            this.lbl9_13.Size = new System.Drawing.Size(31, 31);
            this.lbl9_13.TabIndex = 124;
            this.lbl9_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl9_13.Click += new System.EventHandler(this.map_click);
            // 
            // lbl9_12
            // 
            this.lbl9_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl9_12.Image = ((System.Drawing.Image)(resources.GetObject("lbl9_12.Image")));
            this.lbl9_12.Location = new System.Drawing.Point(360, 270);
            this.lbl9_12.Name = "lbl9_12";
            this.lbl9_12.Size = new System.Drawing.Size(31, 31);
            this.lbl9_12.TabIndex = 123;
            this.lbl9_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl9_12.Click += new System.EventHandler(this.map_click);
            // 
            // lbl9_11
            // 
            this.lbl9_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl9_11.Image = ((System.Drawing.Image)(resources.GetObject("lbl9_11.Image")));
            this.lbl9_11.Location = new System.Drawing.Point(330, 270);
            this.lbl9_11.Name = "lbl9_11";
            this.lbl9_11.Size = new System.Drawing.Size(31, 31);
            this.lbl9_11.TabIndex = 122;
            this.lbl9_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl9_11.Click += new System.EventHandler(this.map_click);
            // 
            // lbl9_10
            // 
            this.lbl9_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl9_10.Image = ((System.Drawing.Image)(resources.GetObject("lbl9_10.Image")));
            this.lbl9_10.Location = new System.Drawing.Point(300, 270);
            this.lbl9_10.Name = "lbl9_10";
            this.lbl9_10.Size = new System.Drawing.Size(31, 31);
            this.lbl9_10.TabIndex = 121;
            this.lbl9_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl9_10.Click += new System.EventHandler(this.map_click);
            // 
            // lbl9_9
            // 
            this.lbl9_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl9_9.Image = ((System.Drawing.Image)(resources.GetObject("lbl9_9.Image")));
            this.lbl9_9.Location = new System.Drawing.Point(270, 270);
            this.lbl9_9.Name = "lbl9_9";
            this.lbl9_9.Size = new System.Drawing.Size(31, 31);
            this.lbl9_9.TabIndex = 120;
            this.lbl9_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl9_9.Click += new System.EventHandler(this.map_click);
            // 
            // lbl9_8
            // 
            this.lbl9_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl9_8.Image = ((System.Drawing.Image)(resources.GetObject("lbl9_8.Image")));
            this.lbl9_8.Location = new System.Drawing.Point(240, 270);
            this.lbl9_8.Name = "lbl9_8";
            this.lbl9_8.Size = new System.Drawing.Size(31, 31);
            this.lbl9_8.TabIndex = 119;
            this.lbl9_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl9_8.Click += new System.EventHandler(this.map_click);
            // 
            // lbl9_7
            // 
            this.lbl9_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl9_7.Image = ((System.Drawing.Image)(resources.GetObject("lbl9_7.Image")));
            this.lbl9_7.Location = new System.Drawing.Point(210, 270);
            this.lbl9_7.Name = "lbl9_7";
            this.lbl9_7.Size = new System.Drawing.Size(31, 31);
            this.lbl9_7.TabIndex = 118;
            this.lbl9_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl9_7.Click += new System.EventHandler(this.map_click);
            // 
            // lbl9_6
            // 
            this.lbl9_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl9_6.Image = ((System.Drawing.Image)(resources.GetObject("lbl9_6.Image")));
            this.lbl9_6.Location = new System.Drawing.Point(180, 270);
            this.lbl9_6.Name = "lbl9_6";
            this.lbl9_6.Size = new System.Drawing.Size(31, 31);
            this.lbl9_6.TabIndex = 117;
            this.lbl9_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl9_6.Click += new System.EventHandler(this.map_click);
            // 
            // lbl9_5
            // 
            this.lbl9_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl9_5.Image = ((System.Drawing.Image)(resources.GetObject("lbl9_5.Image")));
            this.lbl9_5.Location = new System.Drawing.Point(150, 270);
            this.lbl9_5.Name = "lbl9_5";
            this.lbl9_5.Size = new System.Drawing.Size(31, 31);
            this.lbl9_5.TabIndex = 116;
            this.lbl9_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl9_5.Click += new System.EventHandler(this.map_click);
            // 
            // lbl9_4
            // 
            this.lbl9_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl9_4.Image = ((System.Drawing.Image)(resources.GetObject("lbl9_4.Image")));
            this.lbl9_4.Location = new System.Drawing.Point(120, 270);
            this.lbl9_4.Name = "lbl9_4";
            this.lbl9_4.Size = new System.Drawing.Size(31, 31);
            this.lbl9_4.TabIndex = 115;
            this.lbl9_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl9_4.Click += new System.EventHandler(this.map_click);
            // 
            // lbl9_3
            // 
            this.lbl9_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl9_3.Image = ((System.Drawing.Image)(resources.GetObject("lbl9_3.Image")));
            this.lbl9_3.Location = new System.Drawing.Point(90, 270);
            this.lbl9_3.Name = "lbl9_3";
            this.lbl9_3.Size = new System.Drawing.Size(31, 31);
            this.lbl9_3.TabIndex = 114;
            this.lbl9_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl9_3.Click += new System.EventHandler(this.map_click);
            // 
            // lbl9_2
            // 
            this.lbl9_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl9_2.Image = ((System.Drawing.Image)(resources.GetObject("lbl9_2.Image")));
            this.lbl9_2.Location = new System.Drawing.Point(60, 270);
            this.lbl9_2.Name = "lbl9_2";
            this.lbl9_2.Size = new System.Drawing.Size(31, 31);
            this.lbl9_2.TabIndex = 113;
            this.lbl9_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl9_2.Click += new System.EventHandler(this.map_click);
            // 
            // lbl9_1
            // 
            this.lbl9_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl9_1.Image = ((System.Drawing.Image)(resources.GetObject("lbl9_1.Image")));
            this.lbl9_1.Location = new System.Drawing.Point(30, 270);
            this.lbl9_1.Name = "lbl9_1";
            this.lbl9_1.Size = new System.Drawing.Size(31, 31);
            this.lbl9_1.TabIndex = 112;
            this.lbl9_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl9_1.Click += new System.EventHandler(this.map_click);
            // 
            // lbl8_16
            // 
            this.lbl8_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl8_16.Image = ((System.Drawing.Image)(resources.GetObject("lbl8_16.Image")));
            this.lbl8_16.Location = new System.Drawing.Point(480, 240);
            this.lbl8_16.Name = "lbl8_16";
            this.lbl8_16.Size = new System.Drawing.Size(31, 31);
            this.lbl8_16.TabIndex = 111;
            this.lbl8_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl8_16.Click += new System.EventHandler(this.map_click);
            // 
            // lbl8_15
            // 
            this.lbl8_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl8_15.Image = ((System.Drawing.Image)(resources.GetObject("lbl8_15.Image")));
            this.lbl8_15.Location = new System.Drawing.Point(450, 240);
            this.lbl8_15.Name = "lbl8_15";
            this.lbl8_15.Size = new System.Drawing.Size(31, 31);
            this.lbl8_15.TabIndex = 110;
            this.lbl8_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl8_15.Click += new System.EventHandler(this.map_click);
            // 
            // lbl8_14
            // 
            this.lbl8_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl8_14.Image = ((System.Drawing.Image)(resources.GetObject("lbl8_14.Image")));
            this.lbl8_14.Location = new System.Drawing.Point(420, 240);
            this.lbl8_14.Name = "lbl8_14";
            this.lbl8_14.Size = new System.Drawing.Size(31, 31);
            this.lbl8_14.TabIndex = 109;
            this.lbl8_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl8_14.Click += new System.EventHandler(this.map_click);
            // 
            // lbl8_13
            // 
            this.lbl8_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl8_13.Image = ((System.Drawing.Image)(resources.GetObject("lbl8_13.Image")));
            this.lbl8_13.Location = new System.Drawing.Point(390, 240);
            this.lbl8_13.Name = "lbl8_13";
            this.lbl8_13.Size = new System.Drawing.Size(31, 31);
            this.lbl8_13.TabIndex = 108;
            this.lbl8_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl8_13.Click += new System.EventHandler(this.map_click);
            // 
            // lbl8_12
            // 
            this.lbl8_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl8_12.Image = ((System.Drawing.Image)(resources.GetObject("lbl8_12.Image")));
            this.lbl8_12.Location = new System.Drawing.Point(360, 240);
            this.lbl8_12.Name = "lbl8_12";
            this.lbl8_12.Size = new System.Drawing.Size(31, 31);
            this.lbl8_12.TabIndex = 107;
            this.lbl8_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl8_12.Click += new System.EventHandler(this.map_click);
            // 
            // lbl8_11
            // 
            this.lbl8_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl8_11.Image = ((System.Drawing.Image)(resources.GetObject("lbl8_11.Image")));
            this.lbl8_11.Location = new System.Drawing.Point(330, 240);
            this.lbl8_11.Name = "lbl8_11";
            this.lbl8_11.Size = new System.Drawing.Size(31, 31);
            this.lbl8_11.TabIndex = 106;
            this.lbl8_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl8_11.Click += new System.EventHandler(this.map_click);
            // 
            // lbl8_10
            // 
            this.lbl8_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl8_10.Image = ((System.Drawing.Image)(resources.GetObject("lbl8_10.Image")));
            this.lbl8_10.Location = new System.Drawing.Point(300, 240);
            this.lbl8_10.Name = "lbl8_10";
            this.lbl8_10.Size = new System.Drawing.Size(31, 31);
            this.lbl8_10.TabIndex = 105;
            this.lbl8_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl8_10.Click += new System.EventHandler(this.map_click);
            // 
            // lbl8_9
            // 
            this.lbl8_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl8_9.Image = ((System.Drawing.Image)(resources.GetObject("lbl8_9.Image")));
            this.lbl8_9.Location = new System.Drawing.Point(270, 240);
            this.lbl8_9.Name = "lbl8_9";
            this.lbl8_9.Size = new System.Drawing.Size(31, 31);
            this.lbl8_9.TabIndex = 104;
            this.lbl8_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl8_9.Click += new System.EventHandler(this.map_click);
            // 
            // lbl8_8
            // 
            this.lbl8_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl8_8.Image = ((System.Drawing.Image)(resources.GetObject("lbl8_8.Image")));
            this.lbl8_8.Location = new System.Drawing.Point(240, 240);
            this.lbl8_8.Name = "lbl8_8";
            this.lbl8_8.Size = new System.Drawing.Size(31, 31);
            this.lbl8_8.TabIndex = 103;
            this.lbl8_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl8_8.Click += new System.EventHandler(this.map_click);
            // 
            // lbl8_7
            // 
            this.lbl8_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl8_7.Image = ((System.Drawing.Image)(resources.GetObject("lbl8_7.Image")));
            this.lbl8_7.Location = new System.Drawing.Point(210, 240);
            this.lbl8_7.Name = "lbl8_7";
            this.lbl8_7.Size = new System.Drawing.Size(31, 31);
            this.lbl8_7.TabIndex = 102;
            this.lbl8_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl8_7.Click += new System.EventHandler(this.map_click);
            // 
            // lbl8_6
            // 
            this.lbl8_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl8_6.Image = ((System.Drawing.Image)(resources.GetObject("lbl8_6.Image")));
            this.lbl8_6.Location = new System.Drawing.Point(180, 240);
            this.lbl8_6.Name = "lbl8_6";
            this.lbl8_6.Size = new System.Drawing.Size(31, 31);
            this.lbl8_6.TabIndex = 101;
            this.lbl8_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl8_6.Click += new System.EventHandler(this.map_click);
            // 
            // lbl8_5
            // 
            this.lbl8_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl8_5.Image = ((System.Drawing.Image)(resources.GetObject("lbl8_5.Image")));
            this.lbl8_5.Location = new System.Drawing.Point(150, 240);
            this.lbl8_5.Name = "lbl8_5";
            this.lbl8_5.Size = new System.Drawing.Size(31, 31);
            this.lbl8_5.TabIndex = 100;
            this.lbl8_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl8_5.Click += new System.EventHandler(this.map_click);
            // 
            // lbl8_4
            // 
            this.lbl8_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl8_4.Image = ((System.Drawing.Image)(resources.GetObject("lbl8_4.Image")));
            this.lbl8_4.Location = new System.Drawing.Point(120, 240);
            this.lbl8_4.Name = "lbl8_4";
            this.lbl8_4.Size = new System.Drawing.Size(31, 31);
            this.lbl8_4.TabIndex = 99;
            this.lbl8_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl8_4.Click += new System.EventHandler(this.map_click);
            // 
            // lbl8_3
            // 
            this.lbl8_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl8_3.Image = ((System.Drawing.Image)(resources.GetObject("lbl8_3.Image")));
            this.lbl8_3.Location = new System.Drawing.Point(90, 240);
            this.lbl8_3.Name = "lbl8_3";
            this.lbl8_3.Size = new System.Drawing.Size(31, 31);
            this.lbl8_3.TabIndex = 98;
            this.lbl8_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl8_3.Click += new System.EventHandler(this.map_click);
            // 
            // lbl8_2
            // 
            this.lbl8_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl8_2.Image = ((System.Drawing.Image)(resources.GetObject("lbl8_2.Image")));
            this.lbl8_2.Location = new System.Drawing.Point(60, 240);
            this.lbl8_2.Name = "lbl8_2";
            this.lbl8_2.Size = new System.Drawing.Size(31, 31);
            this.lbl8_2.TabIndex = 97;
            this.lbl8_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl8_2.Click += new System.EventHandler(this.map_click);
            // 
            // lbl8_1
            // 
            this.lbl8_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl8_1.Image = ((System.Drawing.Image)(resources.GetObject("lbl8_1.Image")));
            this.lbl8_1.Location = new System.Drawing.Point(30, 240);
            this.lbl8_1.Name = "lbl8_1";
            this.lbl8_1.Size = new System.Drawing.Size(31, 31);
            this.lbl8_1.TabIndex = 96;
            this.lbl8_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl8_1.Click += new System.EventHandler(this.map_click);
            // 
            // lbl7_16
            // 
            this.lbl7_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl7_16.Image = ((System.Drawing.Image)(resources.GetObject("lbl7_16.Image")));
            this.lbl7_16.Location = new System.Drawing.Point(480, 210);
            this.lbl7_16.Name = "lbl7_16";
            this.lbl7_16.Size = new System.Drawing.Size(31, 31);
            this.lbl7_16.TabIndex = 95;
            this.lbl7_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl7_16.Click += new System.EventHandler(this.map_click);
            // 
            // lbl7_15
            // 
            this.lbl7_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl7_15.Image = ((System.Drawing.Image)(resources.GetObject("lbl7_15.Image")));
            this.lbl7_15.Location = new System.Drawing.Point(450, 210);
            this.lbl7_15.Name = "lbl7_15";
            this.lbl7_15.Size = new System.Drawing.Size(31, 31);
            this.lbl7_15.TabIndex = 94;
            this.lbl7_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl7_15.Click += new System.EventHandler(this.map_click);
            // 
            // lbl7_14
            // 
            this.lbl7_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl7_14.Image = ((System.Drawing.Image)(resources.GetObject("lbl7_14.Image")));
            this.lbl7_14.Location = new System.Drawing.Point(420, 210);
            this.lbl7_14.Name = "lbl7_14";
            this.lbl7_14.Size = new System.Drawing.Size(31, 31);
            this.lbl7_14.TabIndex = 93;
            this.lbl7_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl7_14.Click += new System.EventHandler(this.map_click);
            // 
            // lbl7_13
            // 
            this.lbl7_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl7_13.Image = ((System.Drawing.Image)(resources.GetObject("lbl7_13.Image")));
            this.lbl7_13.Location = new System.Drawing.Point(390, 210);
            this.lbl7_13.Name = "lbl7_13";
            this.lbl7_13.Size = new System.Drawing.Size(31, 31);
            this.lbl7_13.TabIndex = 92;
            this.lbl7_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl7_13.Click += new System.EventHandler(this.map_click);
            // 
            // lbl7_12
            // 
            this.lbl7_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl7_12.Image = ((System.Drawing.Image)(resources.GetObject("lbl7_12.Image")));
            this.lbl7_12.Location = new System.Drawing.Point(360, 210);
            this.lbl7_12.Name = "lbl7_12";
            this.lbl7_12.Size = new System.Drawing.Size(31, 31);
            this.lbl7_12.TabIndex = 91;
            this.lbl7_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl7_12.Click += new System.EventHandler(this.map_click);
            // 
            // lbl7_11
            // 
            this.lbl7_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl7_11.Image = ((System.Drawing.Image)(resources.GetObject("lbl7_11.Image")));
            this.lbl7_11.Location = new System.Drawing.Point(330, 210);
            this.lbl7_11.Name = "lbl7_11";
            this.lbl7_11.Size = new System.Drawing.Size(31, 31);
            this.lbl7_11.TabIndex = 90;
            this.lbl7_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl7_11.Click += new System.EventHandler(this.map_click);
            // 
            // lbl7_10
            // 
            this.lbl7_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl7_10.Image = ((System.Drawing.Image)(resources.GetObject("lbl7_10.Image")));
            this.lbl7_10.Location = new System.Drawing.Point(300, 210);
            this.lbl7_10.Name = "lbl7_10";
            this.lbl7_10.Size = new System.Drawing.Size(31, 31);
            this.lbl7_10.TabIndex = 89;
            this.lbl7_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl7_10.Click += new System.EventHandler(this.map_click);
            // 
            // lbl7_9
            // 
            this.lbl7_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl7_9.Image = ((System.Drawing.Image)(resources.GetObject("lbl7_9.Image")));
            this.lbl7_9.Location = new System.Drawing.Point(270, 210);
            this.lbl7_9.Name = "lbl7_9";
            this.lbl7_9.Size = new System.Drawing.Size(31, 31);
            this.lbl7_9.TabIndex = 88;
            this.lbl7_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl7_9.Click += new System.EventHandler(this.map_click);
            // 
            // lbl7_8
            // 
            this.lbl7_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl7_8.Image = ((System.Drawing.Image)(resources.GetObject("lbl7_8.Image")));
            this.lbl7_8.Location = new System.Drawing.Point(240, 210);
            this.lbl7_8.Name = "lbl7_8";
            this.lbl7_8.Size = new System.Drawing.Size(31, 31);
            this.lbl7_8.TabIndex = 87;
            this.lbl7_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl7_8.Click += new System.EventHandler(this.map_click);
            // 
            // lbl7_7
            // 
            this.lbl7_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl7_7.Image = ((System.Drawing.Image)(resources.GetObject("lbl7_7.Image")));
            this.lbl7_7.Location = new System.Drawing.Point(210, 210);
            this.lbl7_7.Name = "lbl7_7";
            this.lbl7_7.Size = new System.Drawing.Size(31, 31);
            this.lbl7_7.TabIndex = 86;
            this.lbl7_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl7_7.Click += new System.EventHandler(this.map_click);
            // 
            // lbl7_6
            // 
            this.lbl7_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl7_6.Image = ((System.Drawing.Image)(resources.GetObject("lbl7_6.Image")));
            this.lbl7_6.Location = new System.Drawing.Point(180, 210);
            this.lbl7_6.Name = "lbl7_6";
            this.lbl7_6.Size = new System.Drawing.Size(31, 31);
            this.lbl7_6.TabIndex = 85;
            this.lbl7_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl7_6.Click += new System.EventHandler(this.map_click);
            // 
            // lbl7_5
            // 
            this.lbl7_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl7_5.Image = ((System.Drawing.Image)(resources.GetObject("lbl7_5.Image")));
            this.lbl7_5.Location = new System.Drawing.Point(150, 210);
            this.lbl7_5.Name = "lbl7_5";
            this.lbl7_5.Size = new System.Drawing.Size(31, 31);
            this.lbl7_5.TabIndex = 84;
            this.lbl7_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl7_5.Click += new System.EventHandler(this.map_click);
            // 
            // lbl7_4
            // 
            this.lbl7_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl7_4.Image = ((System.Drawing.Image)(resources.GetObject("lbl7_4.Image")));
            this.lbl7_4.Location = new System.Drawing.Point(120, 210);
            this.lbl7_4.Name = "lbl7_4";
            this.lbl7_4.Size = new System.Drawing.Size(31, 31);
            this.lbl7_4.TabIndex = 83;
            this.lbl7_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl7_4.Click += new System.EventHandler(this.map_click);
            // 
            // lbl7_3
            // 
            this.lbl7_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl7_3.Image = ((System.Drawing.Image)(resources.GetObject("lbl7_3.Image")));
            this.lbl7_3.Location = new System.Drawing.Point(90, 210);
            this.lbl7_3.Name = "lbl7_3";
            this.lbl7_3.Size = new System.Drawing.Size(31, 31);
            this.lbl7_3.TabIndex = 82;
            this.lbl7_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl7_3.Click += new System.EventHandler(this.map_click);
            // 
            // lbl7_2
            // 
            this.lbl7_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl7_2.Image = ((System.Drawing.Image)(resources.GetObject("lbl7_2.Image")));
            this.lbl7_2.Location = new System.Drawing.Point(60, 210);
            this.lbl7_2.Name = "lbl7_2";
            this.lbl7_2.Size = new System.Drawing.Size(31, 31);
            this.lbl7_2.TabIndex = 81;
            this.lbl7_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl7_2.Click += new System.EventHandler(this.map_click);
            // 
            // lbl7_1
            // 
            this.lbl7_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl7_1.Image = ((System.Drawing.Image)(resources.GetObject("lbl7_1.Image")));
            this.lbl7_1.Location = new System.Drawing.Point(30, 210);
            this.lbl7_1.Name = "lbl7_1";
            this.lbl7_1.Size = new System.Drawing.Size(31, 31);
            this.lbl7_1.TabIndex = 80;
            this.lbl7_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl7_1.Click += new System.EventHandler(this.map_click);
            // 
            // lbl2_16
            // 
            this.lbl2_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl2_16.Image = ((System.Drawing.Image)(resources.GetObject("lbl2_16.Image")));
            this.lbl2_16.Location = new System.Drawing.Point(480, 60);
            this.lbl2_16.Name = "lbl2_16";
            this.lbl2_16.Size = new System.Drawing.Size(31, 31);
            this.lbl2_16.TabIndex = 79;
            this.lbl2_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl2_16.Click += new System.EventHandler(this.map_click);
            // 
            // lbl2_15
            // 
            this.lbl2_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl2_15.Image = ((System.Drawing.Image)(resources.GetObject("lbl2_15.Image")));
            this.lbl2_15.Location = new System.Drawing.Point(450, 60);
            this.lbl2_15.Name = "lbl2_15";
            this.lbl2_15.Size = new System.Drawing.Size(31, 31);
            this.lbl2_15.TabIndex = 78;
            this.lbl2_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl2_15.Click += new System.EventHandler(this.map_click);
            // 
            // lbl2_14
            // 
            this.lbl2_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl2_14.Image = ((System.Drawing.Image)(resources.GetObject("lbl2_14.Image")));
            this.lbl2_14.Location = new System.Drawing.Point(420, 60);
            this.lbl2_14.Name = "lbl2_14";
            this.lbl2_14.Size = new System.Drawing.Size(31, 31);
            this.lbl2_14.TabIndex = 77;
            this.lbl2_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl2_14.Click += new System.EventHandler(this.map_click);
            // 
            // lbl2_13
            // 
            this.lbl2_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl2_13.Image = ((System.Drawing.Image)(resources.GetObject("lbl2_13.Image")));
            this.lbl2_13.Location = new System.Drawing.Point(390, 60);
            this.lbl2_13.Name = "lbl2_13";
            this.lbl2_13.Size = new System.Drawing.Size(31, 31);
            this.lbl2_13.TabIndex = 76;
            this.lbl2_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl2_13.Click += new System.EventHandler(this.map_click);
            // 
            // lbl2_12
            // 
            this.lbl2_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl2_12.Image = ((System.Drawing.Image)(resources.GetObject("lbl2_12.Image")));
            this.lbl2_12.Location = new System.Drawing.Point(360, 60);
            this.lbl2_12.Name = "lbl2_12";
            this.lbl2_12.Size = new System.Drawing.Size(31, 31);
            this.lbl2_12.TabIndex = 75;
            this.lbl2_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl2_12.Click += new System.EventHandler(this.map_click);
            // 
            // lbl2_11
            // 
            this.lbl2_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl2_11.Image = ((System.Drawing.Image)(resources.GetObject("lbl2_11.Image")));
            this.lbl2_11.Location = new System.Drawing.Point(330, 60);
            this.lbl2_11.Name = "lbl2_11";
            this.lbl2_11.Size = new System.Drawing.Size(31, 31);
            this.lbl2_11.TabIndex = 74;
            this.lbl2_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl2_11.Click += new System.EventHandler(this.map_click);
            // 
            // lbl2_10
            // 
            this.lbl2_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl2_10.Image = ((System.Drawing.Image)(resources.GetObject("lbl2_10.Image")));
            this.lbl2_10.Location = new System.Drawing.Point(300, 60);
            this.lbl2_10.Name = "lbl2_10";
            this.lbl2_10.Size = new System.Drawing.Size(31, 31);
            this.lbl2_10.TabIndex = 73;
            this.lbl2_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl2_10.Click += new System.EventHandler(this.map_click);
            // 
            // lbl2_9
            // 
            this.lbl2_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl2_9.Image = ((System.Drawing.Image)(resources.GetObject("lbl2_9.Image")));
            this.lbl2_9.Location = new System.Drawing.Point(270, 60);
            this.lbl2_9.Name = "lbl2_9";
            this.lbl2_9.Size = new System.Drawing.Size(31, 31);
            this.lbl2_9.TabIndex = 72;
            this.lbl2_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl2_9.Click += new System.EventHandler(this.map_click);
            // 
            // lbl2_8
            // 
            this.lbl2_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl2_8.Image = ((System.Drawing.Image)(resources.GetObject("lbl2_8.Image")));
            this.lbl2_8.Location = new System.Drawing.Point(240, 60);
            this.lbl2_8.Name = "lbl2_8";
            this.lbl2_8.Size = new System.Drawing.Size(31, 31);
            this.lbl2_8.TabIndex = 71;
            this.lbl2_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl2_8.Click += new System.EventHandler(this.map_click);
            // 
            // lbl2_7
            // 
            this.lbl2_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl2_7.Image = ((System.Drawing.Image)(resources.GetObject("lbl2_7.Image")));
            this.lbl2_7.Location = new System.Drawing.Point(210, 60);
            this.lbl2_7.Name = "lbl2_7";
            this.lbl2_7.Size = new System.Drawing.Size(31, 31);
            this.lbl2_7.TabIndex = 70;
            this.lbl2_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl2_7.Click += new System.EventHandler(this.map_click);
            // 
            // lbl2_6
            // 
            this.lbl2_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl2_6.Image = ((System.Drawing.Image)(resources.GetObject("lbl2_6.Image")));
            this.lbl2_6.Location = new System.Drawing.Point(180, 60);
            this.lbl2_6.Name = "lbl2_6";
            this.lbl2_6.Size = new System.Drawing.Size(31, 31);
            this.lbl2_6.TabIndex = 69;
            this.lbl2_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl2_6.Click += new System.EventHandler(this.map_click);
            // 
            // lbl2_5
            // 
            this.lbl2_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl2_5.Image = ((System.Drawing.Image)(resources.GetObject("lbl2_5.Image")));
            this.lbl2_5.Location = new System.Drawing.Point(150, 60);
            this.lbl2_5.Name = "lbl2_5";
            this.lbl2_5.Size = new System.Drawing.Size(31, 31);
            this.lbl2_5.TabIndex = 68;
            this.lbl2_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl2_5.Click += new System.EventHandler(this.map_click);
            // 
            // lbl2_4
            // 
            this.lbl2_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl2_4.Image = ((System.Drawing.Image)(resources.GetObject("lbl2_4.Image")));
            this.lbl2_4.Location = new System.Drawing.Point(120, 60);
            this.lbl2_4.Name = "lbl2_4";
            this.lbl2_4.Size = new System.Drawing.Size(31, 31);
            this.lbl2_4.TabIndex = 67;
            this.lbl2_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl2_4.Click += new System.EventHandler(this.map_click);
            // 
            // lbl2_3
            // 
            this.lbl2_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl2_3.Image = ((System.Drawing.Image)(resources.GetObject("lbl2_3.Image")));
            this.lbl2_3.Location = new System.Drawing.Point(90, 60);
            this.lbl2_3.Name = "lbl2_3";
            this.lbl2_3.Size = new System.Drawing.Size(31, 31);
            this.lbl2_3.TabIndex = 66;
            this.lbl2_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl2_3.Click += new System.EventHandler(this.map_click);
            // 
            // lbl2_2
            // 
            this.lbl2_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl2_2.Image = ((System.Drawing.Image)(resources.GetObject("lbl2_2.Image")));
            this.lbl2_2.Location = new System.Drawing.Point(60, 60);
            this.lbl2_2.Name = "lbl2_2";
            this.lbl2_2.Size = new System.Drawing.Size(31, 31);
            this.lbl2_2.TabIndex = 65;
            this.lbl2_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl2_2.Click += new System.EventHandler(this.map_click);
            // 
            // lbl2_1
            // 
            this.lbl2_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl2_1.Image = ((System.Drawing.Image)(resources.GetObject("lbl2_1.Image")));
            this.lbl2_1.Location = new System.Drawing.Point(30, 60);
            this.lbl2_1.Name = "lbl2_1";
            this.lbl2_1.Size = new System.Drawing.Size(31, 31);
            this.lbl2_1.TabIndex = 64;
            this.lbl2_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl2_1.Click += new System.EventHandler(this.map_click);
            // 
            // lbl3_16
            // 
            this.lbl3_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl3_16.Image = ((System.Drawing.Image)(resources.GetObject("lbl3_16.Image")));
            this.lbl3_16.Location = new System.Drawing.Point(480, 90);
            this.lbl3_16.Name = "lbl3_16";
            this.lbl3_16.Size = new System.Drawing.Size(31, 31);
            this.lbl3_16.TabIndex = 63;
            this.lbl3_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl3_16.Click += new System.EventHandler(this.map_click);
            // 
            // lbl3_15
            // 
            this.lbl3_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl3_15.Image = ((System.Drawing.Image)(resources.GetObject("lbl3_15.Image")));
            this.lbl3_15.Location = new System.Drawing.Point(450, 90);
            this.lbl3_15.Name = "lbl3_15";
            this.lbl3_15.Size = new System.Drawing.Size(31, 31);
            this.lbl3_15.TabIndex = 62;
            this.lbl3_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl3_15.Click += new System.EventHandler(this.map_click);
            // 
            // lbl3_14
            // 
            this.lbl3_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl3_14.Image = ((System.Drawing.Image)(resources.GetObject("lbl3_14.Image")));
            this.lbl3_14.Location = new System.Drawing.Point(420, 90);
            this.lbl3_14.Name = "lbl3_14";
            this.lbl3_14.Size = new System.Drawing.Size(31, 31);
            this.lbl3_14.TabIndex = 61;
            this.lbl3_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl3_14.Click += new System.EventHandler(this.map_click);
            // 
            // lbl3_13
            // 
            this.lbl3_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl3_13.Image = ((System.Drawing.Image)(resources.GetObject("lbl3_13.Image")));
            this.lbl3_13.Location = new System.Drawing.Point(390, 90);
            this.lbl3_13.Name = "lbl3_13";
            this.lbl3_13.Size = new System.Drawing.Size(31, 31);
            this.lbl3_13.TabIndex = 60;
            this.lbl3_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl3_13.Click += new System.EventHandler(this.map_click);
            // 
            // lbl3_12
            // 
            this.lbl3_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl3_12.Image = ((System.Drawing.Image)(resources.GetObject("lbl3_12.Image")));
            this.lbl3_12.Location = new System.Drawing.Point(360, 90);
            this.lbl3_12.Name = "lbl3_12";
            this.lbl3_12.Size = new System.Drawing.Size(31, 31);
            this.lbl3_12.TabIndex = 59;
            this.lbl3_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl3_12.Click += new System.EventHandler(this.map_click);
            // 
            // lbl3_11
            // 
            this.lbl3_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl3_11.Image = ((System.Drawing.Image)(resources.GetObject("lbl3_11.Image")));
            this.lbl3_11.Location = new System.Drawing.Point(330, 90);
            this.lbl3_11.Name = "lbl3_11";
            this.lbl3_11.Size = new System.Drawing.Size(31, 31);
            this.lbl3_11.TabIndex = 58;
            this.lbl3_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl3_11.Click += new System.EventHandler(this.map_click);
            // 
            // lbl3_10
            // 
            this.lbl3_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl3_10.Image = ((System.Drawing.Image)(resources.GetObject("lbl3_10.Image")));
            this.lbl3_10.Location = new System.Drawing.Point(300, 90);
            this.lbl3_10.Name = "lbl3_10";
            this.lbl3_10.Size = new System.Drawing.Size(31, 31);
            this.lbl3_10.TabIndex = 57;
            this.lbl3_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl3_10.Click += new System.EventHandler(this.map_click);
            // 
            // lbl3_9
            // 
            this.lbl3_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl3_9.Image = ((System.Drawing.Image)(resources.GetObject("lbl3_9.Image")));
            this.lbl3_9.Location = new System.Drawing.Point(270, 90);
            this.lbl3_9.Name = "lbl3_9";
            this.lbl3_9.Size = new System.Drawing.Size(31, 31);
            this.lbl3_9.TabIndex = 56;
            this.lbl3_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl3_9.Click += new System.EventHandler(this.map_click);
            // 
            // lbl3_8
            // 
            this.lbl3_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl3_8.Image = ((System.Drawing.Image)(resources.GetObject("lbl3_8.Image")));
            this.lbl3_8.Location = new System.Drawing.Point(240, 90);
            this.lbl3_8.Name = "lbl3_8";
            this.lbl3_8.Size = new System.Drawing.Size(31, 31);
            this.lbl3_8.TabIndex = 55;
            this.lbl3_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl3_8.Click += new System.EventHandler(this.map_click);
            // 
            // lbl3_7
            // 
            this.lbl3_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl3_7.Image = ((System.Drawing.Image)(resources.GetObject("lbl3_7.Image")));
            this.lbl3_7.Location = new System.Drawing.Point(210, 90);
            this.lbl3_7.Name = "lbl3_7";
            this.lbl3_7.Size = new System.Drawing.Size(31, 31);
            this.lbl3_7.TabIndex = 54;
            this.lbl3_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl3_7.Click += new System.EventHandler(this.map_click);
            // 
            // lbl3_6
            // 
            this.lbl3_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl3_6.Image = ((System.Drawing.Image)(resources.GetObject("lbl3_6.Image")));
            this.lbl3_6.Location = new System.Drawing.Point(180, 90);
            this.lbl3_6.Name = "lbl3_6";
            this.lbl3_6.Size = new System.Drawing.Size(31, 31);
            this.lbl3_6.TabIndex = 53;
            this.lbl3_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl3_6.Click += new System.EventHandler(this.map_click);
            // 
            // lbl3_5
            // 
            this.lbl3_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl3_5.Image = ((System.Drawing.Image)(resources.GetObject("lbl3_5.Image")));
            this.lbl3_5.Location = new System.Drawing.Point(150, 90);
            this.lbl3_5.Name = "lbl3_5";
            this.lbl3_5.Size = new System.Drawing.Size(31, 31);
            this.lbl3_5.TabIndex = 52;
            this.lbl3_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl3_5.Click += new System.EventHandler(this.map_click);
            // 
            // lbl3_4
            // 
            this.lbl3_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl3_4.Image = ((System.Drawing.Image)(resources.GetObject("lbl3_4.Image")));
            this.lbl3_4.Location = new System.Drawing.Point(120, 90);
            this.lbl3_4.Name = "lbl3_4";
            this.lbl3_4.Size = new System.Drawing.Size(31, 31);
            this.lbl3_4.TabIndex = 51;
            this.lbl3_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl3_4.Click += new System.EventHandler(this.map_click);
            // 
            // lbl3_3
            // 
            this.lbl3_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl3_3.Image = ((System.Drawing.Image)(resources.GetObject("lbl3_3.Image")));
            this.lbl3_3.Location = new System.Drawing.Point(90, 90);
            this.lbl3_3.Name = "lbl3_3";
            this.lbl3_3.Size = new System.Drawing.Size(31, 31);
            this.lbl3_3.TabIndex = 50;
            this.lbl3_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl3_3.Click += new System.EventHandler(this.map_click);
            // 
            // lbl3_2
            // 
            this.lbl3_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl3_2.Image = ((System.Drawing.Image)(resources.GetObject("lbl3_2.Image")));
            this.lbl3_2.Location = new System.Drawing.Point(60, 90);
            this.lbl3_2.Name = "lbl3_2";
            this.lbl3_2.Size = new System.Drawing.Size(31, 31);
            this.lbl3_2.TabIndex = 49;
            this.lbl3_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl3_2.Click += new System.EventHandler(this.map_click);
            // 
            // lbl3_1
            // 
            this.lbl3_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl3_1.Image = ((System.Drawing.Image)(resources.GetObject("lbl3_1.Image")));
            this.lbl3_1.Location = new System.Drawing.Point(30, 90);
            this.lbl3_1.Name = "lbl3_1";
            this.lbl3_1.Size = new System.Drawing.Size(31, 31);
            this.lbl3_1.TabIndex = 48;
            this.lbl3_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl3_1.Click += new System.EventHandler(this.map_click);
            // 
            // lbl6_16
            // 
            this.lbl6_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl6_16.Image = ((System.Drawing.Image)(resources.GetObject("lbl6_16.Image")));
            this.lbl6_16.Location = new System.Drawing.Point(480, 180);
            this.lbl6_16.Name = "lbl6_16";
            this.lbl6_16.Size = new System.Drawing.Size(31, 31);
            this.lbl6_16.TabIndex = 47;
            this.lbl6_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl6_16.Click += new System.EventHandler(this.map_click);
            // 
            // lbl6_15
            // 
            this.lbl6_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl6_15.Image = ((System.Drawing.Image)(resources.GetObject("lbl6_15.Image")));
            this.lbl6_15.Location = new System.Drawing.Point(450, 180);
            this.lbl6_15.Name = "lbl6_15";
            this.lbl6_15.Size = new System.Drawing.Size(31, 31);
            this.lbl6_15.TabIndex = 46;
            this.lbl6_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl6_15.Click += new System.EventHandler(this.map_click);
            // 
            // lbl6_14
            // 
            this.lbl6_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl6_14.Image = ((System.Drawing.Image)(resources.GetObject("lbl6_14.Image")));
            this.lbl6_14.Location = new System.Drawing.Point(420, 180);
            this.lbl6_14.Name = "lbl6_14";
            this.lbl6_14.Size = new System.Drawing.Size(31, 31);
            this.lbl6_14.TabIndex = 45;
            this.lbl6_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl6_14.Click += new System.EventHandler(this.map_click);
            // 
            // lbl6_13
            // 
            this.lbl6_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl6_13.Image = ((System.Drawing.Image)(resources.GetObject("lbl6_13.Image")));
            this.lbl6_13.Location = new System.Drawing.Point(390, 180);
            this.lbl6_13.Name = "lbl6_13";
            this.lbl6_13.Size = new System.Drawing.Size(31, 31);
            this.lbl6_13.TabIndex = 44;
            this.lbl6_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl6_13.Click += new System.EventHandler(this.map_click);
            // 
            // lbl6_12
            // 
            this.lbl6_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl6_12.Image = ((System.Drawing.Image)(resources.GetObject("lbl6_12.Image")));
            this.lbl6_12.Location = new System.Drawing.Point(360, 180);
            this.lbl6_12.Name = "lbl6_12";
            this.lbl6_12.Size = new System.Drawing.Size(31, 31);
            this.lbl6_12.TabIndex = 43;
            this.lbl6_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl6_12.Click += new System.EventHandler(this.map_click);
            // 
            // lbl6_11
            // 
            this.lbl6_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl6_11.Image = ((System.Drawing.Image)(resources.GetObject("lbl6_11.Image")));
            this.lbl6_11.Location = new System.Drawing.Point(330, 180);
            this.lbl6_11.Name = "lbl6_11";
            this.lbl6_11.Size = new System.Drawing.Size(31, 31);
            this.lbl6_11.TabIndex = 42;
            this.lbl6_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl6_11.Click += new System.EventHandler(this.map_click);
            // 
            // lbl6_10
            // 
            this.lbl6_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl6_10.Image = ((System.Drawing.Image)(resources.GetObject("lbl6_10.Image")));
            this.lbl6_10.Location = new System.Drawing.Point(300, 180);
            this.lbl6_10.Name = "lbl6_10";
            this.lbl6_10.Size = new System.Drawing.Size(31, 31);
            this.lbl6_10.TabIndex = 41;
            this.lbl6_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl6_10.Click += new System.EventHandler(this.map_click);
            // 
            // lbl6_9
            // 
            this.lbl6_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl6_9.Image = ((System.Drawing.Image)(resources.GetObject("lbl6_9.Image")));
            this.lbl6_9.Location = new System.Drawing.Point(270, 180);
            this.lbl6_9.Name = "lbl6_9";
            this.lbl6_9.Size = new System.Drawing.Size(31, 31);
            this.lbl6_9.TabIndex = 40;
            this.lbl6_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl6_9.Click += new System.EventHandler(this.map_click);
            // 
            // lbl6_8
            // 
            this.lbl6_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl6_8.Image = ((System.Drawing.Image)(resources.GetObject("lbl6_8.Image")));
            this.lbl6_8.Location = new System.Drawing.Point(240, 180);
            this.lbl6_8.Name = "lbl6_8";
            this.lbl6_8.Size = new System.Drawing.Size(31, 31);
            this.lbl6_8.TabIndex = 39;
            this.lbl6_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl6_8.Click += new System.EventHandler(this.map_click);
            // 
            // lbl6_7
            // 
            this.lbl6_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl6_7.Image = ((System.Drawing.Image)(resources.GetObject("lbl6_7.Image")));
            this.lbl6_7.Location = new System.Drawing.Point(210, 180);
            this.lbl6_7.Name = "lbl6_7";
            this.lbl6_7.Size = new System.Drawing.Size(31, 31);
            this.lbl6_7.TabIndex = 38;
            this.lbl6_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl6_7.Click += new System.EventHandler(this.map_click);
            // 
            // lbl6_6
            // 
            this.lbl6_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl6_6.Image = ((System.Drawing.Image)(resources.GetObject("lbl6_6.Image")));
            this.lbl6_6.Location = new System.Drawing.Point(180, 180);
            this.lbl6_6.Name = "lbl6_6";
            this.lbl6_6.Size = new System.Drawing.Size(31, 31);
            this.lbl6_6.TabIndex = 37;
            this.lbl6_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl6_6.Click += new System.EventHandler(this.map_click);
            // 
            // lbl6_5
            // 
            this.lbl6_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl6_5.Image = ((System.Drawing.Image)(resources.GetObject("lbl6_5.Image")));
            this.lbl6_5.Location = new System.Drawing.Point(150, 180);
            this.lbl6_5.Name = "lbl6_5";
            this.lbl6_5.Size = new System.Drawing.Size(31, 31);
            this.lbl6_5.TabIndex = 36;
            this.lbl6_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl6_5.Click += new System.EventHandler(this.map_click);
            // 
            // lbl6_4
            // 
            this.lbl6_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl6_4.Image = ((System.Drawing.Image)(resources.GetObject("lbl6_4.Image")));
            this.lbl6_4.Location = new System.Drawing.Point(120, 180);
            this.lbl6_4.Name = "lbl6_4";
            this.lbl6_4.Size = new System.Drawing.Size(31, 31);
            this.lbl6_4.TabIndex = 35;
            this.lbl6_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl6_4.Click += new System.EventHandler(this.map_click);
            // 
            // lbl6_3
            // 
            this.lbl6_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl6_3.Image = ((System.Drawing.Image)(resources.GetObject("lbl6_3.Image")));
            this.lbl6_3.Location = new System.Drawing.Point(90, 180);
            this.lbl6_3.Name = "lbl6_3";
            this.lbl6_3.Size = new System.Drawing.Size(31, 31);
            this.lbl6_3.TabIndex = 34;
            this.lbl6_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl6_3.Click += new System.EventHandler(this.map_click);
            // 
            // lbl6_2
            // 
            this.lbl6_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl6_2.Image = ((System.Drawing.Image)(resources.GetObject("lbl6_2.Image")));
            this.lbl6_2.Location = new System.Drawing.Point(60, 180);
            this.lbl6_2.Name = "lbl6_2";
            this.lbl6_2.Size = new System.Drawing.Size(31, 31);
            this.lbl6_2.TabIndex = 33;
            this.lbl6_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl6_2.Click += new System.EventHandler(this.map_click);
            // 
            // lbl6_1
            // 
            this.lbl6_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl6_1.Image = ((System.Drawing.Image)(resources.GetObject("lbl6_1.Image")));
            this.lbl6_1.Location = new System.Drawing.Point(30, 180);
            this.lbl6_1.Name = "lbl6_1";
            this.lbl6_1.Size = new System.Drawing.Size(31, 31);
            this.lbl6_1.TabIndex = 32;
            this.lbl6_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl6_1.Click += new System.EventHandler(this.map_click);
            // 
            // lbl5_16
            // 
            this.lbl5_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl5_16.Image = ((System.Drawing.Image)(resources.GetObject("lbl5_16.Image")));
            this.lbl5_16.Location = new System.Drawing.Point(480, 150);
            this.lbl5_16.Name = "lbl5_16";
            this.lbl5_16.Size = new System.Drawing.Size(31, 31);
            this.lbl5_16.TabIndex = 31;
            this.lbl5_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5_16.Click += new System.EventHandler(this.map_click);
            // 
            // lbl5_15
            // 
            this.lbl5_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl5_15.Image = ((System.Drawing.Image)(resources.GetObject("lbl5_15.Image")));
            this.lbl5_15.Location = new System.Drawing.Point(450, 150);
            this.lbl5_15.Name = "lbl5_15";
            this.lbl5_15.Size = new System.Drawing.Size(31, 31);
            this.lbl5_15.TabIndex = 30;
            this.lbl5_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5_15.Click += new System.EventHandler(this.map_click);
            // 
            // lbl5_14
            // 
            this.lbl5_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl5_14.Image = ((System.Drawing.Image)(resources.GetObject("lbl5_14.Image")));
            this.lbl5_14.Location = new System.Drawing.Point(420, 150);
            this.lbl5_14.Name = "lbl5_14";
            this.lbl5_14.Size = new System.Drawing.Size(31, 31);
            this.lbl5_14.TabIndex = 29;
            this.lbl5_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5_14.Click += new System.EventHandler(this.map_click);
            // 
            // lbl5_13
            // 
            this.lbl5_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl5_13.Image = ((System.Drawing.Image)(resources.GetObject("lbl5_13.Image")));
            this.lbl5_13.Location = new System.Drawing.Point(390, 150);
            this.lbl5_13.Name = "lbl5_13";
            this.lbl5_13.Size = new System.Drawing.Size(31, 31);
            this.lbl5_13.TabIndex = 28;
            this.lbl5_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5_13.Click += new System.EventHandler(this.map_click);
            // 
            // lbl5_12
            // 
            this.lbl5_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl5_12.Image = ((System.Drawing.Image)(resources.GetObject("lbl5_12.Image")));
            this.lbl5_12.Location = new System.Drawing.Point(360, 150);
            this.lbl5_12.Name = "lbl5_12";
            this.lbl5_12.Size = new System.Drawing.Size(31, 31);
            this.lbl5_12.TabIndex = 27;
            this.lbl5_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5_12.Click += new System.EventHandler(this.map_click);
            // 
            // lbl5_11
            // 
            this.lbl5_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl5_11.Image = ((System.Drawing.Image)(resources.GetObject("lbl5_11.Image")));
            this.lbl5_11.Location = new System.Drawing.Point(330, 150);
            this.lbl5_11.Name = "lbl5_11";
            this.lbl5_11.Size = new System.Drawing.Size(31, 31);
            this.lbl5_11.TabIndex = 26;
            this.lbl5_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5_11.Click += new System.EventHandler(this.map_click);
            // 
            // lbl5_10
            // 
            this.lbl5_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl5_10.Image = ((System.Drawing.Image)(resources.GetObject("lbl5_10.Image")));
            this.lbl5_10.Location = new System.Drawing.Point(300, 150);
            this.lbl5_10.Name = "lbl5_10";
            this.lbl5_10.Size = new System.Drawing.Size(31, 31);
            this.lbl5_10.TabIndex = 25;
            this.lbl5_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5_10.Click += new System.EventHandler(this.map_click);
            // 
            // lbl5_9
            // 
            this.lbl5_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl5_9.Image = ((System.Drawing.Image)(resources.GetObject("lbl5_9.Image")));
            this.lbl5_9.Location = new System.Drawing.Point(270, 150);
            this.lbl5_9.Name = "lbl5_9";
            this.lbl5_9.Size = new System.Drawing.Size(31, 31);
            this.lbl5_9.TabIndex = 24;
            this.lbl5_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5_9.Click += new System.EventHandler(this.map_click);
            // 
            // lbl5_8
            // 
            this.lbl5_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl5_8.Image = ((System.Drawing.Image)(resources.GetObject("lbl5_8.Image")));
            this.lbl5_8.Location = new System.Drawing.Point(240, 150);
            this.lbl5_8.Name = "lbl5_8";
            this.lbl5_8.Size = new System.Drawing.Size(31, 31);
            this.lbl5_8.TabIndex = 23;
            this.lbl5_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5_8.Click += new System.EventHandler(this.map_click);
            // 
            // lbl5_7
            // 
            this.lbl5_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl5_7.Image = ((System.Drawing.Image)(resources.GetObject("lbl5_7.Image")));
            this.lbl5_7.Location = new System.Drawing.Point(210, 150);
            this.lbl5_7.Name = "lbl5_7";
            this.lbl5_7.Size = new System.Drawing.Size(31, 31);
            this.lbl5_7.TabIndex = 22;
            this.lbl5_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5_7.Click += new System.EventHandler(this.map_click);
            // 
            // lbl5_6
            // 
            this.lbl5_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl5_6.Image = ((System.Drawing.Image)(resources.GetObject("lbl5_6.Image")));
            this.lbl5_6.Location = new System.Drawing.Point(180, 150);
            this.lbl5_6.Name = "lbl5_6";
            this.lbl5_6.Size = new System.Drawing.Size(31, 31);
            this.lbl5_6.TabIndex = 21;
            this.lbl5_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5_6.Click += new System.EventHandler(this.map_click);
            // 
            // lbl5_5
            // 
            this.lbl5_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl5_5.Image = ((System.Drawing.Image)(resources.GetObject("lbl5_5.Image")));
            this.lbl5_5.Location = new System.Drawing.Point(150, 150);
            this.lbl5_5.Name = "lbl5_5";
            this.lbl5_5.Size = new System.Drawing.Size(31, 31);
            this.lbl5_5.TabIndex = 20;
            this.lbl5_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5_5.Click += new System.EventHandler(this.map_click);
            // 
            // lbl5_4
            // 
            this.lbl5_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl5_4.Image = ((System.Drawing.Image)(resources.GetObject("lbl5_4.Image")));
            this.lbl5_4.Location = new System.Drawing.Point(120, 150);
            this.lbl5_4.Name = "lbl5_4";
            this.lbl5_4.Size = new System.Drawing.Size(31, 31);
            this.lbl5_4.TabIndex = 19;
            this.lbl5_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5_4.Click += new System.EventHandler(this.map_click);
            // 
            // lbl5_3
            // 
            this.lbl5_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl5_3.Image = ((System.Drawing.Image)(resources.GetObject("lbl5_3.Image")));
            this.lbl5_3.Location = new System.Drawing.Point(90, 150);
            this.lbl5_3.Name = "lbl5_3";
            this.lbl5_3.Size = new System.Drawing.Size(31, 31);
            this.lbl5_3.TabIndex = 18;
            this.lbl5_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5_3.Click += new System.EventHandler(this.map_click);
            // 
            // lbl5_2
            // 
            this.lbl5_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl5_2.Image = ((System.Drawing.Image)(resources.GetObject("lbl5_2.Image")));
            this.lbl5_2.Location = new System.Drawing.Point(60, 150);
            this.lbl5_2.Name = "lbl5_2";
            this.lbl5_2.Size = new System.Drawing.Size(31, 31);
            this.lbl5_2.TabIndex = 17;
            this.lbl5_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5_2.Click += new System.EventHandler(this.map_click);
            // 
            // lbl5_1
            // 
            this.lbl5_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl5_1.Image = ((System.Drawing.Image)(resources.GetObject("lbl5_1.Image")));
            this.lbl5_1.Location = new System.Drawing.Point(30, 150);
            this.lbl5_1.Name = "lbl5_1";
            this.lbl5_1.Size = new System.Drawing.Size(31, 31);
            this.lbl5_1.TabIndex = 16;
            this.lbl5_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5_1.Click += new System.EventHandler(this.map_click);
            // 
            // lbl4_16
            // 
            this.lbl4_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl4_16.Image = ((System.Drawing.Image)(resources.GetObject("lbl4_16.Image")));
            this.lbl4_16.Location = new System.Drawing.Point(480, 120);
            this.lbl4_16.Name = "lbl4_16";
            this.lbl4_16.Size = new System.Drawing.Size(31, 31);
            this.lbl4_16.TabIndex = 15;
            this.lbl4_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4_16.Click += new System.EventHandler(this.map_click);
            // 
            // lbl4_15
            // 
            this.lbl4_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl4_15.Image = ((System.Drawing.Image)(resources.GetObject("lbl4_15.Image")));
            this.lbl4_15.Location = new System.Drawing.Point(450, 120);
            this.lbl4_15.Name = "lbl4_15";
            this.lbl4_15.Size = new System.Drawing.Size(31, 31);
            this.lbl4_15.TabIndex = 14;
            this.lbl4_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4_15.Click += new System.EventHandler(this.map_click);
            // 
            // lbl4_14
            // 
            this.lbl4_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl4_14.Image = ((System.Drawing.Image)(resources.GetObject("lbl4_14.Image")));
            this.lbl4_14.Location = new System.Drawing.Point(420, 120);
            this.lbl4_14.Name = "lbl4_14";
            this.lbl4_14.Size = new System.Drawing.Size(31, 31);
            this.lbl4_14.TabIndex = 13;
            this.lbl4_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4_14.Click += new System.EventHandler(this.map_click);
            // 
            // lbl4_13
            // 
            this.lbl4_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl4_13.Image = ((System.Drawing.Image)(resources.GetObject("lbl4_13.Image")));
            this.lbl4_13.Location = new System.Drawing.Point(390, 120);
            this.lbl4_13.Name = "lbl4_13";
            this.lbl4_13.Size = new System.Drawing.Size(31, 31);
            this.lbl4_13.TabIndex = 12;
            this.lbl4_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4_13.Click += new System.EventHandler(this.map_click);
            // 
            // lbl4_12
            // 
            this.lbl4_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl4_12.Image = ((System.Drawing.Image)(resources.GetObject("lbl4_12.Image")));
            this.lbl4_12.Location = new System.Drawing.Point(360, 120);
            this.lbl4_12.Name = "lbl4_12";
            this.lbl4_12.Size = new System.Drawing.Size(31, 31);
            this.lbl4_12.TabIndex = 11;
            this.lbl4_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4_12.Click += new System.EventHandler(this.map_click);
            // 
            // lbl4_11
            // 
            this.lbl4_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl4_11.Image = ((System.Drawing.Image)(resources.GetObject("lbl4_11.Image")));
            this.lbl4_11.Location = new System.Drawing.Point(330, 120);
            this.lbl4_11.Name = "lbl4_11";
            this.lbl4_11.Size = new System.Drawing.Size(31, 31);
            this.lbl4_11.TabIndex = 10;
            this.lbl4_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4_11.Click += new System.EventHandler(this.map_click);
            // 
            // lbl4_10
            // 
            this.lbl4_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl4_10.Image = ((System.Drawing.Image)(resources.GetObject("lbl4_10.Image")));
            this.lbl4_10.Location = new System.Drawing.Point(300, 120);
            this.lbl4_10.Name = "lbl4_10";
            this.lbl4_10.Size = new System.Drawing.Size(31, 31);
            this.lbl4_10.TabIndex = 9;
            this.lbl4_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4_10.Click += new System.EventHandler(this.map_click);
            // 
            // lbl4_9
            // 
            this.lbl4_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl4_9.Image = ((System.Drawing.Image)(resources.GetObject("lbl4_9.Image")));
            this.lbl4_9.Location = new System.Drawing.Point(270, 120);
            this.lbl4_9.Name = "lbl4_9";
            this.lbl4_9.Size = new System.Drawing.Size(31, 31);
            this.lbl4_9.TabIndex = 8;
            this.lbl4_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4_9.Click += new System.EventHandler(this.map_click);
            // 
            // lbl4_8
            // 
            this.lbl4_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl4_8.Image = ((System.Drawing.Image)(resources.GetObject("lbl4_8.Image")));
            this.lbl4_8.Location = new System.Drawing.Point(240, 120);
            this.lbl4_8.Name = "lbl4_8";
            this.lbl4_8.Size = new System.Drawing.Size(31, 31);
            this.lbl4_8.TabIndex = 7;
            this.lbl4_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4_8.Click += new System.EventHandler(this.map_click);
            // 
            // lbl4_7
            // 
            this.lbl4_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl4_7.Image = ((System.Drawing.Image)(resources.GetObject("lbl4_7.Image")));
            this.lbl4_7.Location = new System.Drawing.Point(210, 120);
            this.lbl4_7.Name = "lbl4_7";
            this.lbl4_7.Size = new System.Drawing.Size(31, 31);
            this.lbl4_7.TabIndex = 6;
            this.lbl4_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4_7.Click += new System.EventHandler(this.map_click);
            // 
            // lbl4_6
            // 
            this.lbl4_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl4_6.Image = ((System.Drawing.Image)(resources.GetObject("lbl4_6.Image")));
            this.lbl4_6.Location = new System.Drawing.Point(180, 120);
            this.lbl4_6.Name = "lbl4_6";
            this.lbl4_6.Size = new System.Drawing.Size(31, 31);
            this.lbl4_6.TabIndex = 5;
            this.lbl4_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4_6.Click += new System.EventHandler(this.map_click);
            // 
            // lbl4_5
            // 
            this.lbl4_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl4_5.Image = ((System.Drawing.Image)(resources.GetObject("lbl4_5.Image")));
            this.lbl4_5.Location = new System.Drawing.Point(150, 120);
            this.lbl4_5.Name = "lbl4_5";
            this.lbl4_5.Size = new System.Drawing.Size(31, 31);
            this.lbl4_5.TabIndex = 4;
            this.lbl4_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4_5.Click += new System.EventHandler(this.map_click);
            // 
            // lbl4_4
            // 
            this.lbl4_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl4_4.Image = ((System.Drawing.Image)(resources.GetObject("lbl4_4.Image")));
            this.lbl4_4.Location = new System.Drawing.Point(120, 120);
            this.lbl4_4.Name = "lbl4_4";
            this.lbl4_4.Size = new System.Drawing.Size(31, 31);
            this.lbl4_4.TabIndex = 3;
            this.lbl4_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4_4.Click += new System.EventHandler(this.map_click);
            // 
            // lbl4_3
            // 
            this.lbl4_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl4_3.Image = ((System.Drawing.Image)(resources.GetObject("lbl4_3.Image")));
            this.lbl4_3.Location = new System.Drawing.Point(90, 120);
            this.lbl4_3.Name = "lbl4_3";
            this.lbl4_3.Size = new System.Drawing.Size(31, 31);
            this.lbl4_3.TabIndex = 2;
            this.lbl4_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4_3.Click += new System.EventHandler(this.map_click);
            // 
            // lbl4_2
            // 
            this.lbl4_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl4_2.Image = ((System.Drawing.Image)(resources.GetObject("lbl4_2.Image")));
            this.lbl4_2.Location = new System.Drawing.Point(60, 120);
            this.lbl4_2.Name = "lbl4_2";
            this.lbl4_2.Size = new System.Drawing.Size(31, 31);
            this.lbl4_2.TabIndex = 1;
            this.lbl4_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4_2.Click += new System.EventHandler(this.map_click);
            // 
            // lbl4_1
            // 
            this.lbl4_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl4_1.Image = ((System.Drawing.Image)(resources.GetObject("lbl4_1.Image")));
            this.lbl4_1.Location = new System.Drawing.Point(30, 120);
            this.lbl4_1.Name = "lbl4_1";
            this.lbl4_1.Size = new System.Drawing.Size(31, 31);
            this.lbl4_1.TabIndex = 0;
            this.lbl4_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4_1.Click += new System.EventHandler(this.map_click);
            // 
            // btnHelp
            // 
            this.btnHelp.Location = new System.Drawing.Point(480, 552);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(108, 35);
            this.btnHelp.TabIndex = 301;
            this.btnHelp.Text = "Help";
            this.btnHelp.UseVisualStyleBackColor = true;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // frmGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1124, 647);
            this.Controls.Add(this.btnHelp);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lblDay);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpBuild);
            this.Controls.Add(this.btnNextDay);
            this.Controls.Add(this.label74);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.label72);
            this.Controls.Add(this.label71);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.label64);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.label59);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl16_16);
            this.Controls.Add(this.lbl16_15);
            this.Controls.Add(this.lbl16_14);
            this.Controls.Add(this.lbl16_13);
            this.Controls.Add(this.lbl16_12);
            this.Controls.Add(this.lbl16_11);
            this.Controls.Add(this.lbl16_10);
            this.Controls.Add(this.lbl16_9);
            this.Controls.Add(this.lbl16_8);
            this.Controls.Add(this.lbl16_7);
            this.Controls.Add(this.lbl16_6);
            this.Controls.Add(this.lbl16_5);
            this.Controls.Add(this.lbl16_4);
            this.Controls.Add(this.lbl16_3);
            this.Controls.Add(this.lbl16_2);
            this.Controls.Add(this.lbl16_1);
            this.Controls.Add(this.lbl1_16);
            this.Controls.Add(this.lbl1_15);
            this.Controls.Add(this.lbl1_14);
            this.Controls.Add(this.lbl1_13);
            this.Controls.Add(this.lbl1_12);
            this.Controls.Add(this.lbl1_11);
            this.Controls.Add(this.lbl1_10);
            this.Controls.Add(this.lbl1_9);
            this.Controls.Add(this.lbl1_8);
            this.Controls.Add(this.lbl1_7);
            this.Controls.Add(this.lbl1_6);
            this.Controls.Add(this.lbl1_5);
            this.Controls.Add(this.lbl1_4);
            this.Controls.Add(this.lbl1_3);
            this.Controls.Add(this.lbl1_2);
            this.Controls.Add(this.lbl1_1);
            this.Controls.Add(this.lbl15_16);
            this.Controls.Add(this.lbl15_15);
            this.Controls.Add(this.lbl15_14);
            this.Controls.Add(this.lbl15_13);
            this.Controls.Add(this.lbl15_12);
            this.Controls.Add(this.lbl15_11);
            this.Controls.Add(this.lbl15_10);
            this.Controls.Add(this.lbl15_9);
            this.Controls.Add(this.lbl15_8);
            this.Controls.Add(this.lbl15_7);
            this.Controls.Add(this.lbl15_6);
            this.Controls.Add(this.lbl15_5);
            this.Controls.Add(this.lbl15_4);
            this.Controls.Add(this.lbl15_3);
            this.Controls.Add(this.lbl15_2);
            this.Controls.Add(this.lbl15_1);
            this.Controls.Add(this.lbl14_16);
            this.Controls.Add(this.lbl14_15);
            this.Controls.Add(this.lbl14_14);
            this.Controls.Add(this.lbl14_13);
            this.Controls.Add(this.lbl14_12);
            this.Controls.Add(this.lbl14_11);
            this.Controls.Add(this.lbl14_10);
            this.Controls.Add(this.lbl14_9);
            this.Controls.Add(this.lbl14_8);
            this.Controls.Add(this.lbl14_7);
            this.Controls.Add(this.lbl14_6);
            this.Controls.Add(this.lbl14_5);
            this.Controls.Add(this.lbl14_4);
            this.Controls.Add(this.lbl14_3);
            this.Controls.Add(this.lbl14_2);
            this.Controls.Add(this.lbl14_1);
            this.Controls.Add(this.lbl13_16);
            this.Controls.Add(this.lbl13_15);
            this.Controls.Add(this.lbl13_14);
            this.Controls.Add(this.lbl13_13);
            this.Controls.Add(this.lbl13_12);
            this.Controls.Add(this.lbl13_11);
            this.Controls.Add(this.lbl13_10);
            this.Controls.Add(this.lbl13_9);
            this.Controls.Add(this.lbl13_8);
            this.Controls.Add(this.lbl13_7);
            this.Controls.Add(this.lbl13_6);
            this.Controls.Add(this.lbl13_5);
            this.Controls.Add(this.lbl13_4);
            this.Controls.Add(this.lbl13_3);
            this.Controls.Add(this.lbl13_2);
            this.Controls.Add(this.lbl13_1);
            this.Controls.Add(this.lbl12_16);
            this.Controls.Add(this.lbl12_15);
            this.Controls.Add(this.lbl12_14);
            this.Controls.Add(this.lbl12_13);
            this.Controls.Add(this.lbl12_12);
            this.Controls.Add(this.lbl12_11);
            this.Controls.Add(this.lbl12_10);
            this.Controls.Add(this.lbl12_9);
            this.Controls.Add(this.lbl12_8);
            this.Controls.Add(this.lbl12_7);
            this.Controls.Add(this.lbl12_6);
            this.Controls.Add(this.lbl12_5);
            this.Controls.Add(this.lbl12_4);
            this.Controls.Add(this.lbl12_3);
            this.Controls.Add(this.lbl12_2);
            this.Controls.Add(this.lbl12_1);
            this.Controls.Add(this.lbl11_16);
            this.Controls.Add(this.lbl11_15);
            this.Controls.Add(this.lbl11_14);
            this.Controls.Add(this.lbl11_13);
            this.Controls.Add(this.lbl11_12);
            this.Controls.Add(this.lbl11_11);
            this.Controls.Add(this.lbl11_10);
            this.Controls.Add(this.lbl11_9);
            this.Controls.Add(this.lbl11_8);
            this.Controls.Add(this.lbl11_7);
            this.Controls.Add(this.lbl11_6);
            this.Controls.Add(this.lbl11_5);
            this.Controls.Add(this.lbl11_4);
            this.Controls.Add(this.lbl11_3);
            this.Controls.Add(this.lbl11_2);
            this.Controls.Add(this.lbl11_1);
            this.Controls.Add(this.lbl10_16);
            this.Controls.Add(this.lbl10_15);
            this.Controls.Add(this.lbl10_14);
            this.Controls.Add(this.lbl10_13);
            this.Controls.Add(this.lbl10_12);
            this.Controls.Add(this.lbl10_11);
            this.Controls.Add(this.lbl10_10);
            this.Controls.Add(this.lbl10_9);
            this.Controls.Add(this.lbl10_8);
            this.Controls.Add(this.lbl10_7);
            this.Controls.Add(this.lbl10_6);
            this.Controls.Add(this.lbl10_5);
            this.Controls.Add(this.lbl10_4);
            this.Controls.Add(this.lbl10_3);
            this.Controls.Add(this.lbl10_2);
            this.Controls.Add(this.lbl10_1);
            this.Controls.Add(this.lbl9_16);
            this.Controls.Add(this.lbl9_15);
            this.Controls.Add(this.lbl9_14);
            this.Controls.Add(this.lbl9_13);
            this.Controls.Add(this.lbl9_12);
            this.Controls.Add(this.lbl9_11);
            this.Controls.Add(this.lbl9_10);
            this.Controls.Add(this.lbl9_9);
            this.Controls.Add(this.lbl9_8);
            this.Controls.Add(this.lbl9_7);
            this.Controls.Add(this.lbl9_6);
            this.Controls.Add(this.lbl9_5);
            this.Controls.Add(this.lbl9_4);
            this.Controls.Add(this.lbl9_3);
            this.Controls.Add(this.lbl9_2);
            this.Controls.Add(this.lbl9_1);
            this.Controls.Add(this.lbl8_16);
            this.Controls.Add(this.lbl8_15);
            this.Controls.Add(this.lbl8_14);
            this.Controls.Add(this.lbl8_13);
            this.Controls.Add(this.lbl8_12);
            this.Controls.Add(this.lbl8_11);
            this.Controls.Add(this.lbl8_10);
            this.Controls.Add(this.lbl8_9);
            this.Controls.Add(this.lbl8_8);
            this.Controls.Add(this.lbl8_7);
            this.Controls.Add(this.lbl8_6);
            this.Controls.Add(this.lbl8_5);
            this.Controls.Add(this.lbl8_4);
            this.Controls.Add(this.lbl8_3);
            this.Controls.Add(this.lbl8_2);
            this.Controls.Add(this.lbl8_1);
            this.Controls.Add(this.lbl7_16);
            this.Controls.Add(this.lbl7_15);
            this.Controls.Add(this.lbl7_14);
            this.Controls.Add(this.lbl7_13);
            this.Controls.Add(this.lbl7_12);
            this.Controls.Add(this.lbl7_11);
            this.Controls.Add(this.lbl7_10);
            this.Controls.Add(this.lbl7_9);
            this.Controls.Add(this.lbl7_8);
            this.Controls.Add(this.lbl7_7);
            this.Controls.Add(this.lbl7_6);
            this.Controls.Add(this.lbl7_5);
            this.Controls.Add(this.lbl7_4);
            this.Controls.Add(this.lbl7_3);
            this.Controls.Add(this.lbl7_2);
            this.Controls.Add(this.lbl7_1);
            this.Controls.Add(this.lbl2_16);
            this.Controls.Add(this.lbl2_15);
            this.Controls.Add(this.lbl2_14);
            this.Controls.Add(this.lbl2_13);
            this.Controls.Add(this.lbl2_12);
            this.Controls.Add(this.lbl2_11);
            this.Controls.Add(this.lbl2_10);
            this.Controls.Add(this.lbl2_9);
            this.Controls.Add(this.lbl2_8);
            this.Controls.Add(this.lbl2_7);
            this.Controls.Add(this.lbl2_6);
            this.Controls.Add(this.lbl2_5);
            this.Controls.Add(this.lbl2_4);
            this.Controls.Add(this.lbl2_3);
            this.Controls.Add(this.lbl2_2);
            this.Controls.Add(this.lbl2_1);
            this.Controls.Add(this.lbl3_16);
            this.Controls.Add(this.lbl3_15);
            this.Controls.Add(this.lbl3_14);
            this.Controls.Add(this.lbl3_13);
            this.Controls.Add(this.lbl3_12);
            this.Controls.Add(this.lbl3_11);
            this.Controls.Add(this.lbl3_10);
            this.Controls.Add(this.lbl3_9);
            this.Controls.Add(this.lbl3_8);
            this.Controls.Add(this.lbl3_7);
            this.Controls.Add(this.lbl3_6);
            this.Controls.Add(this.lbl3_5);
            this.Controls.Add(this.lbl3_4);
            this.Controls.Add(this.lbl3_3);
            this.Controls.Add(this.lbl3_2);
            this.Controls.Add(this.lbl3_1);
            this.Controls.Add(this.lbl6_16);
            this.Controls.Add(this.lbl6_15);
            this.Controls.Add(this.lbl6_14);
            this.Controls.Add(this.lbl6_13);
            this.Controls.Add(this.lbl6_12);
            this.Controls.Add(this.lbl6_11);
            this.Controls.Add(this.lbl6_10);
            this.Controls.Add(this.lbl6_9);
            this.Controls.Add(this.lbl6_8);
            this.Controls.Add(this.lbl6_7);
            this.Controls.Add(this.lbl6_6);
            this.Controls.Add(this.lbl6_5);
            this.Controls.Add(this.lbl6_4);
            this.Controls.Add(this.lbl6_3);
            this.Controls.Add(this.lbl6_2);
            this.Controls.Add(this.lbl6_1);
            this.Controls.Add(this.lbl5_16);
            this.Controls.Add(this.lbl5_15);
            this.Controls.Add(this.lbl5_14);
            this.Controls.Add(this.lbl5_13);
            this.Controls.Add(this.lbl5_12);
            this.Controls.Add(this.lbl5_11);
            this.Controls.Add(this.lbl5_10);
            this.Controls.Add(this.lbl5_9);
            this.Controls.Add(this.lbl5_8);
            this.Controls.Add(this.lbl5_7);
            this.Controls.Add(this.lbl5_6);
            this.Controls.Add(this.lbl5_5);
            this.Controls.Add(this.lbl5_4);
            this.Controls.Add(this.lbl5_3);
            this.Controls.Add(this.lbl5_2);
            this.Controls.Add(this.lbl5_1);
            this.Controls.Add(this.lbl4_16);
            this.Controls.Add(this.lbl4_15);
            this.Controls.Add(this.lbl4_14);
            this.Controls.Add(this.lbl4_13);
            this.Controls.Add(this.lbl4_12);
            this.Controls.Add(this.lbl4_11);
            this.Controls.Add(this.lbl4_10);
            this.Controls.Add(this.lbl4_9);
            this.Controls.Add(this.lbl4_8);
            this.Controls.Add(this.lbl4_7);
            this.Controls.Add(this.lbl4_6);
            this.Controls.Add(this.lbl4_5);
            this.Controls.Add(this.lbl4_4);
            this.Controls.Add(this.lbl4_3);
            this.Controls.Add(this.lbl4_2);
            this.Controls.Add(this.lbl4_1);
            this.Name = "frmGame";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "City Builder";
            this.Load += new System.EventHandler(this.frmGame_Load);
            this.grpBuild.ResumeLayout(false);
            this.grpBuild.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl4_1;
        private System.Windows.Forms.Label lbl4_2;
        private System.Windows.Forms.Label lbl4_4;
        private System.Windows.Forms.Label lbl4_3;
        private System.Windows.Forms.Label lbl4_6;
        private System.Windows.Forms.Label lbl4_5;
        private System.Windows.Forms.Label lbl4_12;
        private System.Windows.Forms.Label lbl4_11;
        private System.Windows.Forms.Label lbl4_10;
        private System.Windows.Forms.Label lbl4_9;
        private System.Windows.Forms.Label lbl4_8;
        private System.Windows.Forms.Label lbl4_7;
        private System.Windows.Forms.Label lbl4_16;
        private System.Windows.Forms.Label lbl4_15;
        private System.Windows.Forms.Label lbl4_14;
        private System.Windows.Forms.Label lbl4_13;
        private System.Windows.Forms.Label lbl5_16;
        private System.Windows.Forms.Label lbl5_15;
        private System.Windows.Forms.Label lbl5_14;
        private System.Windows.Forms.Label lbl5_13;
        private System.Windows.Forms.Label lbl5_12;
        private System.Windows.Forms.Label lbl5_11;
        private System.Windows.Forms.Label lbl5_10;
        private System.Windows.Forms.Label lbl5_9;
        private System.Windows.Forms.Label lbl5_8;
        private System.Windows.Forms.Label lbl5_7;
        private System.Windows.Forms.Label lbl5_6;
        private System.Windows.Forms.Label lbl5_5;
        private System.Windows.Forms.Label lbl5_4;
        private System.Windows.Forms.Label lbl5_3;
        private System.Windows.Forms.Label lbl5_2;
        private System.Windows.Forms.Label lbl5_1;
        private System.Windows.Forms.Label lbl6_16;
        private System.Windows.Forms.Label lbl6_15;
        private System.Windows.Forms.Label lbl6_14;
        private System.Windows.Forms.Label lbl6_13;
        private System.Windows.Forms.Label lbl6_12;
        private System.Windows.Forms.Label lbl6_11;
        private System.Windows.Forms.Label lbl6_10;
        private System.Windows.Forms.Label lbl6_9;
        private System.Windows.Forms.Label lbl6_8;
        private System.Windows.Forms.Label lbl6_7;
        private System.Windows.Forms.Label lbl6_6;
        private System.Windows.Forms.Label lbl6_5;
        private System.Windows.Forms.Label lbl6_4;
        private System.Windows.Forms.Label lbl6_3;
        private System.Windows.Forms.Label lbl6_2;
        private System.Windows.Forms.Label lbl6_1;
        private System.Windows.Forms.Label lbl3_16;
        private System.Windows.Forms.Label lbl3_15;
        private System.Windows.Forms.Label lbl3_14;
        private System.Windows.Forms.Label lbl3_13;
        private System.Windows.Forms.Label lbl3_12;
        private System.Windows.Forms.Label lbl3_11;
        private System.Windows.Forms.Label lbl3_10;
        private System.Windows.Forms.Label lbl3_9;
        private System.Windows.Forms.Label lbl3_8;
        private System.Windows.Forms.Label lbl3_7;
        private System.Windows.Forms.Label lbl3_6;
        private System.Windows.Forms.Label lbl3_5;
        private System.Windows.Forms.Label lbl3_4;
        private System.Windows.Forms.Label lbl3_3;
        private System.Windows.Forms.Label lbl3_2;
        private System.Windows.Forms.Label lbl3_1;
        private System.Windows.Forms.Label lbl2_16;
        private System.Windows.Forms.Label lbl2_15;
        private System.Windows.Forms.Label lbl2_14;
        private System.Windows.Forms.Label lbl2_13;
        private System.Windows.Forms.Label lbl2_12;
        private System.Windows.Forms.Label lbl2_11;
        private System.Windows.Forms.Label lbl2_10;
        private System.Windows.Forms.Label lbl2_9;
        private System.Windows.Forms.Label lbl2_8;
        private System.Windows.Forms.Label lbl2_7;
        private System.Windows.Forms.Label lbl2_6;
        private System.Windows.Forms.Label lbl2_5;
        private System.Windows.Forms.Label lbl2_4;
        private System.Windows.Forms.Label lbl2_3;
        private System.Windows.Forms.Label lbl2_2;
        private System.Windows.Forms.Label lbl2_1;
        private System.Windows.Forms.Label lbl7_16;
        private System.Windows.Forms.Label lbl7_15;
        private System.Windows.Forms.Label lbl7_14;
        private System.Windows.Forms.Label lbl7_13;
        private System.Windows.Forms.Label lbl7_12;
        private System.Windows.Forms.Label lbl7_11;
        private System.Windows.Forms.Label lbl7_10;
        private System.Windows.Forms.Label lbl7_9;
        private System.Windows.Forms.Label lbl7_8;
        private System.Windows.Forms.Label lbl7_7;
        private System.Windows.Forms.Label lbl7_6;
        private System.Windows.Forms.Label lbl7_5;
        private System.Windows.Forms.Label lbl7_4;
        private System.Windows.Forms.Label lbl7_3;
        private System.Windows.Forms.Label lbl7_2;
        private System.Windows.Forms.Label lbl7_1;
        private System.Windows.Forms.Label lbl8_16;
        private System.Windows.Forms.Label lbl8_15;
        private System.Windows.Forms.Label lbl8_14;
        private System.Windows.Forms.Label lbl8_13;
        private System.Windows.Forms.Label lbl8_12;
        private System.Windows.Forms.Label lbl8_11;
        private System.Windows.Forms.Label lbl8_10;
        private System.Windows.Forms.Label lbl8_9;
        private System.Windows.Forms.Label lbl8_8;
        private System.Windows.Forms.Label lbl8_7;
        private System.Windows.Forms.Label lbl8_6;
        private System.Windows.Forms.Label lbl8_5;
        private System.Windows.Forms.Label lbl8_4;
        private System.Windows.Forms.Label lbl8_3;
        private System.Windows.Forms.Label lbl8_2;
        private System.Windows.Forms.Label lbl8_1;
        private System.Windows.Forms.Label lbl9_16;
        private System.Windows.Forms.Label lbl9_15;
        private System.Windows.Forms.Label lbl9_14;
        private System.Windows.Forms.Label lbl9_13;
        private System.Windows.Forms.Label lbl9_12;
        private System.Windows.Forms.Label lbl9_11;
        private System.Windows.Forms.Label lbl9_10;
        private System.Windows.Forms.Label lbl9_9;
        private System.Windows.Forms.Label lbl9_8;
        private System.Windows.Forms.Label lbl9_7;
        private System.Windows.Forms.Label lbl9_6;
        private System.Windows.Forms.Label lbl9_5;
        private System.Windows.Forms.Label lbl9_4;
        private System.Windows.Forms.Label lbl9_3;
        private System.Windows.Forms.Label lbl9_2;
        private System.Windows.Forms.Label lbl9_1;
        private System.Windows.Forms.Label lbl10_16;
        private System.Windows.Forms.Label lbl10_15;
        private System.Windows.Forms.Label lbl10_14;
        private System.Windows.Forms.Label lbl10_13;
        private System.Windows.Forms.Label lbl10_12;
        private System.Windows.Forms.Label lbl10_11;
        private System.Windows.Forms.Label lbl10_10;
        private System.Windows.Forms.Label lbl10_9;
        private System.Windows.Forms.Label lbl10_8;
        private System.Windows.Forms.Label lbl10_7;
        private System.Windows.Forms.Label lbl10_6;
        private System.Windows.Forms.Label lbl10_5;
        private System.Windows.Forms.Label lbl10_4;
        private System.Windows.Forms.Label lbl10_3;
        private System.Windows.Forms.Label lbl10_2;
        private System.Windows.Forms.Label lbl10_1;
        private System.Windows.Forms.Label lbl11_16;
        private System.Windows.Forms.Label lbl11_15;
        private System.Windows.Forms.Label lbl11_14;
        private System.Windows.Forms.Label lbl11_13;
        private System.Windows.Forms.Label lbl11_12;
        private System.Windows.Forms.Label lbl11_11;
        private System.Windows.Forms.Label lbl11_10;
        private System.Windows.Forms.Label lbl11_9;
        private System.Windows.Forms.Label lbl11_8;
        private System.Windows.Forms.Label lbl11_7;
        private System.Windows.Forms.Label lbl11_6;
        private System.Windows.Forms.Label lbl11_5;
        private System.Windows.Forms.Label lbl11_4;
        private System.Windows.Forms.Label lbl11_3;
        private System.Windows.Forms.Label lbl11_2;
        private System.Windows.Forms.Label lbl11_1;
        private System.Windows.Forms.Label lbl12_16;
        private System.Windows.Forms.Label lbl12_15;
        private System.Windows.Forms.Label lbl12_14;
        private System.Windows.Forms.Label lbl12_13;
        private System.Windows.Forms.Label lbl12_12;
        private System.Windows.Forms.Label lbl12_11;
        private System.Windows.Forms.Label lbl12_10;
        private System.Windows.Forms.Label lbl12_9;
        private System.Windows.Forms.Label lbl12_8;
        private System.Windows.Forms.Label lbl12_7;
        private System.Windows.Forms.Label lbl12_6;
        private System.Windows.Forms.Label lbl12_5;
        private System.Windows.Forms.Label lbl12_4;
        private System.Windows.Forms.Label lbl12_3;
        private System.Windows.Forms.Label lbl12_2;
        private System.Windows.Forms.Label lbl12_1;
        private System.Windows.Forms.Label lbl13_16;
        private System.Windows.Forms.Label lbl13_15;
        private System.Windows.Forms.Label lbl13_14;
        private System.Windows.Forms.Label lbl13_13;
        private System.Windows.Forms.Label lbl13_12;
        private System.Windows.Forms.Label lbl13_11;
        private System.Windows.Forms.Label lbl13_10;
        private System.Windows.Forms.Label lbl13_9;
        private System.Windows.Forms.Label lbl13_8;
        private System.Windows.Forms.Label lbl13_7;
        private System.Windows.Forms.Label lbl13_6;
        private System.Windows.Forms.Label lbl13_5;
        private System.Windows.Forms.Label lbl13_4;
        private System.Windows.Forms.Label lbl13_3;
        private System.Windows.Forms.Label lbl13_2;
        private System.Windows.Forms.Label lbl13_1;
        private System.Windows.Forms.Label lbl14_16;
        private System.Windows.Forms.Label lbl14_15;
        private System.Windows.Forms.Label lbl14_14;
        private System.Windows.Forms.Label lbl14_13;
        private System.Windows.Forms.Label lbl14_12;
        private System.Windows.Forms.Label lbl14_11;
        private System.Windows.Forms.Label lbl14_10;
        private System.Windows.Forms.Label lbl14_9;
        private System.Windows.Forms.Label lbl14_8;
        private System.Windows.Forms.Label lbl14_7;
        private System.Windows.Forms.Label lbl14_6;
        private System.Windows.Forms.Label lbl14_5;
        private System.Windows.Forms.Label lbl14_4;
        private System.Windows.Forms.Label lbl14_3;
        private System.Windows.Forms.Label lbl14_2;
        private System.Windows.Forms.Label lbl14_1;
        private System.Windows.Forms.Label lbl15_16;
        private System.Windows.Forms.Label lbl15_15;
        private System.Windows.Forms.Label lbl15_14;
        private System.Windows.Forms.Label lbl15_13;
        private System.Windows.Forms.Label lbl15_12;
        private System.Windows.Forms.Label lbl15_11;
        private System.Windows.Forms.Label lbl15_10;
        private System.Windows.Forms.Label lbl15_9;
        private System.Windows.Forms.Label lbl15_8;
        private System.Windows.Forms.Label lbl15_7;
        private System.Windows.Forms.Label lbl15_6;
        private System.Windows.Forms.Label lbl15_5;
        private System.Windows.Forms.Label lbl15_4;
        private System.Windows.Forms.Label lbl15_3;
        private System.Windows.Forms.Label lbl15_2;
        private System.Windows.Forms.Label lbl15_1;
        private System.Windows.Forms.Label lbl1_16;
        private System.Windows.Forms.Label lbl1_15;
        private System.Windows.Forms.Label lbl1_14;
        private System.Windows.Forms.Label lbl1_13;
        private System.Windows.Forms.Label lbl1_12;
        private System.Windows.Forms.Label lbl1_11;
        private System.Windows.Forms.Label lbl1_10;
        private System.Windows.Forms.Label lbl1_9;
        private System.Windows.Forms.Label lbl1_8;
        private System.Windows.Forms.Label lbl1_7;
        private System.Windows.Forms.Label lbl1_6;
        private System.Windows.Forms.Label lbl1_5;
        private System.Windows.Forms.Label lbl1_4;
        private System.Windows.Forms.Label lbl1_3;
        private System.Windows.Forms.Label lbl1_2;
        private System.Windows.Forms.Label lbl1_1;
        private System.Windows.Forms.Label lbl16_16;
        private System.Windows.Forms.Label lbl16_15;
        private System.Windows.Forms.Label lbl16_14;
        private System.Windows.Forms.Label lbl16_13;
        private System.Windows.Forms.Label lbl16_12;
        private System.Windows.Forms.Label lbl16_11;
        private System.Windows.Forms.Label lbl16_10;
        private System.Windows.Forms.Label lbl16_9;
        private System.Windows.Forms.Label lbl16_8;
        private System.Windows.Forms.Label lbl16_7;
        private System.Windows.Forms.Label lbl16_6;
        private System.Windows.Forms.Label lbl16_5;
        private System.Windows.Forms.Label lbl16_4;
        private System.Windows.Forms.Label lbl16_3;
        private System.Windows.Forms.Label lbl16_2;
        private System.Windows.Forms.Label lbl16_1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label labelWhatever;
        private System.Windows.Forms.Label lblMoney;
        private System.Windows.Forms.Label lblPopulation;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Button btnNextDay;
        private System.Windows.Forms.GroupBox grpBuild;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblTavernCost;
        private System.Windows.Forms.Label lblWellCost;
        private System.Windows.Forms.Label lblFarmCost;
        private System.Windows.Forms.Label lblRoadCost;
        private System.Windows.Forms.Label lblHouseCost;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblFood;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblDay;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblFoodIncome;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lblIncome;
        private System.Windows.Forms.Label lblWorkers;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnHelp;
    }
}