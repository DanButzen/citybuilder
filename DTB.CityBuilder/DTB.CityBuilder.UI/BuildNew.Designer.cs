﻿namespace DTB.CityBuilder.UI
{
    partial class BuildNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBuildRoad = new System.Windows.Forms.Button();
            this.btnBuildHouse = new System.Windows.Forms.Button();
            this.btnBuildWell = new System.Windows.Forms.Button();
            this.btnBuildTavern = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnBuildFarm = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnBuildRoad
            // 
            this.btnBuildRoad.Location = new System.Drawing.Point(61, 49);
            this.btnBuildRoad.Name = "btnBuildRoad";
            this.btnBuildRoad.Size = new System.Drawing.Size(97, 54);
            this.btnBuildRoad.TabIndex = 0;
            this.btnBuildRoad.Text = "Build Road";
            this.btnBuildRoad.UseVisualStyleBackColor = true;
            this.btnBuildRoad.Click += new System.EventHandler(this.btnBuildRoad_Click);
            // 
            // btnBuildHouse
            // 
            this.btnBuildHouse.Location = new System.Drawing.Point(190, 49);
            this.btnBuildHouse.Name = "btnBuildHouse";
            this.btnBuildHouse.Size = new System.Drawing.Size(97, 54);
            this.btnBuildHouse.TabIndex = 1;
            this.btnBuildHouse.Text = "Build House";
            this.btnBuildHouse.UseVisualStyleBackColor = true;
            this.btnBuildHouse.Click += new System.EventHandler(this.btnBuildHouse_Click);
            // 
            // btnBuildWell
            // 
            this.btnBuildWell.Location = new System.Drawing.Point(61, 123);
            this.btnBuildWell.Name = "btnBuildWell";
            this.btnBuildWell.Size = new System.Drawing.Size(97, 54);
            this.btnBuildWell.TabIndex = 2;
            this.btnBuildWell.Text = "Build Well";
            this.btnBuildWell.UseVisualStyleBackColor = true;
            this.btnBuildWell.Click += new System.EventHandler(this.btnBuildWell_Click);
            // 
            // btnBuildTavern
            // 
            this.btnBuildTavern.Location = new System.Drawing.Point(190, 123);
            this.btnBuildTavern.Name = "btnBuildTavern";
            this.btnBuildTavern.Size = new System.Drawing.Size(97, 54);
            this.btnBuildTavern.TabIndex = 3;
            this.btnBuildTavern.Text = "Build Tavern";
            this.btnBuildTavern.UseVisualStyleBackColor = true;
            this.btnBuildTavern.Click += new System.EventHandler(this.btnBuildTavern_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(190, 197);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(97, 54);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnBuildFarm
            // 
            this.btnBuildFarm.Location = new System.Drawing.Point(61, 197);
            this.btnBuildFarm.Name = "btnBuildFarm";
            this.btnBuildFarm.Size = new System.Drawing.Size(97, 54);
            this.btnBuildFarm.TabIndex = 5;
            this.btnBuildFarm.Text = "Build Farm";
            this.btnBuildFarm.UseVisualStyleBackColor = true;
            this.btnBuildFarm.Click += new System.EventHandler(this.btnBuildFarm_Click);
            // 
            // BuildNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 293);
            this.Controls.Add(this.btnBuildFarm);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnBuildTavern);
            this.Controls.Add(this.btnBuildWell);
            this.Controls.Add(this.btnBuildHouse);
            this.Controls.Add(this.btnBuildRoad);
            this.Name = "BuildNew";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Build";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnBuildRoad;
        private System.Windows.Forms.Button btnBuildHouse;
        private System.Windows.Forms.Button btnBuildWell;
        private System.Windows.Forms.Button btnBuildTavern;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnBuildFarm;
    }
}